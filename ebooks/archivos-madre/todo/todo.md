<section epub:type="chapter" role="doc-chapter">

# Pistilo

# 1. Pistilo @ignore

## [Daniela Tarazona](997a-semblanzas.xhtml#a10) {.centrado}

Me enfermé de lo mismo que ellas. Ahora que acudimos al hospital nos
vuelven a atender por turnos. Nos saludamos formadas en la fila para
donar nuestra sangre. La pureza existe en nosotras. Nada igual se ha
visto antes. Iluminamos los senderos entre los escombros. La más alta
enseña sus pómulos en los que han nacido brotes verdes. Yo muestro mis
senos cubiertos de musgo cuando me lo piden. La menor ya se arrastra por
el suelo, reverdecida y fosforescente por completo. La paciente
primigenia ahora yace a un lado del campo de futbol, es una superficie
de pasto. No sentimos hambre ni dolor. Estamos siendo herbáceas y
perfectas, oxigenadas y lubricadas por el agua sucia que llueve.
Perdimos la articulación de los pulgares, pero ganamos a cambio los
pistilos carnosos que nos salen de la garganta, con ellos respiramos el
aire espeso de la calle y lo devolvemos pleno de esporas. Hemos decidido
conquistar los pulmones de cualquiera porque es hora de ser vegetales.
Sabemos que se han terminado las diferencias. Enfermaremos al mundo para
ser idénticas. Los hombres son una amenaza. La contundencia de este
acontecimiento biodegradable es grave y a la vez benéfico. Echamos
raíces en nuestro interior: los alveolos son ahora extensiones de
nuestros cuerpos ecológicos, abrevamos el agua de nuestros pulmones,
somos también composta. No permitiremos que existan hombres que nos
tomen de la mano nunca más. Me enfermé de lo mismo que ellas. En las
palmas de las manos tengo espinas. Dicen que nos arderá el moho que
crece en los párpados de las animalas que resisten. Ignoro si la
purificación que experimento me lleva hacia la libertad. Creo que no,
pero mi piel es vegetal. Huelo a naftalina, sin embargo, porque la
salubridad es cosa vieja, un objeto guardado en el armario. Los jugos
verdes que preparamos nos dan la fuerza precisa para evitar cualquier
contaminación. La enfermera me saca sangre y mira mi escote y se
contagia de inmediato.

</section>
<section epub:type="chapter" role="doc-chapter">

# Pistil

# 1. Pistil @ignore

## By [Daniela Tarazona](997b-semblances.xhtml#a10) \ 
Translated by [Toshiya Kamei](997b-semblances.xhtml#a53) {.centrado}

The same thing that got them got me. Now that we’re in the hospital,
they attend to us in turn. We greet each other while waiting in line to
give blood. There’s purity in us. It’s something that has never been
seen before. We illuminate the paths through rubble. The tallest one
shows her cheekbones where green shoots have sprouted. I show my
moss-covered breasts when they ask me. The youngest one already crawls
on the ground, completely green and phosphorescent. The primal patient
now lies on one side of the soccer field that has a grass surface. We
don’t feel hunger or pain. We’re just herbaceous and perfect, oxygenated
and lubricated by the dirty rainwater. We lost the articulation of our
thumbs, but we gained in return the fleshy pistils that come out of our
throats with which we breathe the thick air of the street and fill it
with spores. We’ve decided to conquer anyone’s lungs because it’s time
to be vegetables. We know the differences are over. We’ll make the world
sick in order to be identical. Men are a threat. The strength of this
biodegradable event is serious and at the same time beneficial. We take
root in our interior: the alveoli are now extensions of our ecological
bodies. We drink the water from our lungs. We’re also compost. We’ll not
allow men to take us by the hand anymore. The same thing that got them
got me. Thorns grow in the palms of my hands. They say the mold that
grows on the eyelids of she-animals that resist will burn us. I don’t
know if the purification I go through leads me toward freedom. I don’t
think so, but my skin is vegetable. I smell naphthalene, however,
because healthiness is a thing of the past, an object stored in the
closet. The green juices we prepare give us the strength necessary to
repel any contaminants. When the nurse draws my blood and looks at my
cleavage, the virus spreads like wildfire.

</section>
<section epub:type="chapter" role="doc-chapter">

# Virus 

# 2. Virus @ignore

## [José Luis Zárate Herrera](997a-semblanzas.xhtml#a23) {.centrado}

Cartomancia {.espacios .sin-sangria}

---Y, bien… ¿Qué hay en mi futuro? {.sin-sangria}

La adivina miraba con horror su Tarot. No estaban la Muerte, la Luna, el
Loco, los Amantes. Se desplegaban ante ella cartas que nunca había
visto: el Kaiju, el Asteroide, el Virus, la Supernova… {.sangria}

Reporte {.espacios .sin-sangria}

Virus aparentemente no patógeno, pero armado. {.sin-sangria}

Origen {.espacios .sin-sangria}

“El planeta Tierra ---señaló el guía---, el planeta originario de los
conquistadores del universo. En ese insignificante mundo nacieron los
virus”. {.sin-sangria}

Influjo {.espacios .sin-sangria}

Pocos saben que los virus se convierten en algo peor bajo la luna llena. {.sin-sangria}

Pandemia {.espacios .sin-sangria}

El virus de la combustión espontánea hizo brillar, por última vez, las
innumerables ventanas de la ciudad. {.sin-sangria}

_…We have a problem_ {.espacios .sin-sangria}

“El ecosistema orgánico que somos también se ha visto influenciado por
la radiación, la gravedad cero, las condiciones extremas del espacio.
Algo trasmite desde nuestro interior, Houston, y nos dicen que van a
tomar el control, que es el último día de la opresión tiránica y la
población bacterial al fin será libre de nuestro horrible cuerpo”. {.sin-sangria}

Enfermedad autoimnune {.espacios .sin-sangria}

Sólo se curó cuando logró extirparse por completo de sí mismo. {.sin-sangria}

Grados {.espacios .sin-sangria}

Investigaciones recientes descubrieron que el denominado virus Z,
responsable de la infestación zombie de la última década, muere arriba
de los 50 grados. {.sin-sangria}

Si desea seguir utilizando a sus muertos vivientes (como mascotas, manos
de obra, diversión) sin temor a infectarse[]{#anchor}, basta con
hervirlos. {.sangria}

Misterio médico {.espacios .sin-sangria}

El virus solo mata a quienes tienen un segundo nombre que empieza con G. {.sin-sangria}

Da Vinci {.espacios .sin-sangria}

Miles de científicos se han maravillado de la minuciosidad de los
esbozos médicos y anatómicos de Leonardo Da Vinci, tan exactos que no es
de extrañar que nadie haya descubierto sus dibujos de virus a escala
uno-a-uno. {.sin-sangria}

La ventana {.espacios .sin-sangria}

Los resultados del microscopio electrónico temblaron en su mano. {.sin-sangria}

Su rostro era una luna blanca cuando me dijo: {.sangria}

---Las sombras, el virus se trasmite por medio de las sombras. {.sangria}

Miramos por la ventana, afuera del laboratorio, la noche cubriendo el
mundo. {.sangria}

</section>
<section epub:type="chapter" role="doc-chapter">

# Virus

# 2. Virus @ignore

## By [José Luis Zárate Herrera](997b-semblances.xhtml#a23) \
Translated by [Toshiya Kamei](997b-semblances.xhtml#a53) {.centrado}

Cartomancy {.espacios .sin-sangria}

“Well… What’s in my future?” {.sin-sangria}

The fortune-teller looked at the cards in horror. There was no Death,
the Moon, the Fool, or the Lovers. She had never seen the ones spread
out before her: the Kaiju, the Asteroid, the Virus, the Supernova… {.sangria}

Report {.espacios .sin-sangria}

Virus apparently not pathogenic, but armed. {.sin-sangria}

Origin {.espacios .sin-sangria}

“Planet Earth,” the guide pointed out, “the original home of those who
conquered the universe. The viruses were born in that insignificant
world.” {.sin-sangria}

Influence {.espacios .sin-sangria}

Hardly anyone knows that viruses become worse under the full moon. {.sin-sangria}

Pandemic {.espacios .sin-sangria}

The spontaneous combustion virus made, for the last time, the countless
windows in the city shine. {.sin-sangria}

…We Have a Problem {.espacios .sin-sangria}

“The organic ecosystem we are part of has also been influenced by
radiation, zero gravity, and the extreme conditions of outer space.
Something transmits from within us, Houston, and they tell us they’re
going to seize control, tyrannical oppression is over, and the bacterial
population will finally be free from our horrible bodies.” {.sin-sangria}

Autoimmune Disease {.espacios .sin-sangria}

It healed only when it managed to completely remove itself. {.sin-sangria}

Degrees {.espacios .sin-sangria}

Recent research has found that the so-called Z virus, responsible for
the zombie infestation during the last decade, dies above fifty degrees
(Celsius). {.sin-sangria}

If you want to continue using your living undead (as pets, labor, fun)
without risking infection, just boil them. {.sangria}

Medical Mystery {.espacios .sin-sangria}

The virus kills only those whose middle name begins with G. {.sin-sangria}

Da Vinci {.espacios .sin-sangria}

Thousands of scientists have marveled at Leonardo Da Vinci’s detailed
anatomical sketches. They are so accurate that, not surprisingly, no one
has discovered his one-to-one scale drawings of viruses. {.sin-sangria}

Window {.espacios .sin-sangria}

The electron microscope results trembled in his hand. {.sin-sangria}

His face was a white moon when he told me, “The shadows---the virus is transmitted through the shadows.” {.sangria}

As we looked out the window, the night engulfed the world outside the
laboratory. {.sangria}

</section>
<section epub:type="chapter" role="doc-chapter">

# Cuarto 33

# 3. Cuarto 33 @ignore

## [Karen Chacek](997a-semblanzas.xhtml#a25) {.centrado}

Al anciano sin dientes, atornillado al banquillo de la recepción, le
aclaro: “En circunstancias normales, yo nunca me hospedaría con cuatro
palmas bambú y dos hiedras”, en una habitación sin ventanas como la que
le urgí que me asignara. Siempre me han parecido deprimentes las
recámaras con cortinas de _jacquard_ que simulan ventanales donde hay
paredes de ladrillo o que esconden viejos calefactores que el esplendor
desconoció al partir. Pero hoy no es un día de circunstancias normales:
quien mire hacia la calle enfermará de pura sugestión. En la tele
aconsejaron encerrarse en un cuarto con al menos seis plantas para
purificar el aire y clausurar las ventanas: “Volvernos invisibles, en lo
que el peligro pasa”. Yo solo venía a un simposio sobre lenguas
extintas; ahora estoy varada en el núcleo de una contingencia de
magnitud incalculable, bañada en sudor amarillo de pies a cabeza.

La voz del locutor de noticias me provoca náuseas; traduce al momento el
comunicado que alguien más lee en un inglés golpeado; repite que los
potenciales beneficios para la salud pública siempre han sido mayores
que los riesgos, que haber levantado en 2017 la prohibición de crear
virus letales en laboratorios fue un acto responsable: “La naturaleza es
la mayor bioterrorista y tenemos que hacer todo lo posible por estar un
paso por delante”; cita íntegra de la declaración del presidente de la
Junta Nacional de Asesoramiento Científico de Bioseguridad del mismo
país que construyera una trinidad de deslumbrantes laboratorios, en esta
pequeña ciudad latinoamericana olvidada por el dios de los sistemas
satelitales, con una temperatura promedio de 37 grados centígrados y
variedad de artefactos de aire acondicionado en desuso repartidos por
sus viejos edificios.

El anciano de la recepción sonríe como si morir le diera igual. Me
entrega la llave del cuarto 33.

Traslado las plantas en un mohoso carro portaequipaje; al final del
pasillo, la mano me tiembla frente a la puerta: “Tranquila, cariño ---me
digo---. A la edad de seis años podías desaparecer del mundo con solo
enrollarte en una cortina. ¿Por qué no habría de funcionar eso ahora?”.

</section>
<section epub:type="chapter" role="doc-chapter">

# Room 33

# 3. Room 33 @ignore

## By [Karen Chacek](997b-semblances.xhtml#a25) \
Translated by [Toshiya Kamei](997b-semblances.xhtml#a53) {.centrado}

I tell the toothless old man glued to the reception desk, “Under normal
circumstances, I’d never stay with four bamboo palms and two ivy trees,”
in a windowless room like the one I urged him to assign me. I’ve always
found those rooms depressing---the ones with jacquard curtains
simulating windows covering brick walls or hiding old heaters _the
splendor_ didn’t recognize when it left. But today isn’t a normal 
day---whoever looks toward the street will become ill by pure suggestion.
The TV announcement had advised locking yourself in a room with
at least six air-purification plants and closing the windows
to “become invisible as the danger passes by.” I only came for a
symposium on extinct languages. Now I’m stranded at the core of a
contingency of incalculable magnitude, bathed in yellow sweat from head
to toe.

The newscaster’s voice makes me nauseous. He translates the statement
someone else reads in staccato English. He repeats that the potential
benefits for public health have always been greater than the risks, that
having lifted the ban to create lethal viruses in labs in 2017 was a
responsible act. “Nature is the biggest bioterrorist, and we have to do
everything possible to stay one step ahead.” A full statement from the
president of the National Science Advisory Board for Biosecurity in the
same country that built a trinity of dazzling laboratories in this small
Latin American city forgotten by the god of satellite systems, with an
average temperature of thirty-seven degrees Celsius and a variety of
disused air-conditioning devices scattered throughout its old buildings.

The old man at the reception smiles as if he doesn’t care. He hands me
the key to room 33.

I move the plants using the moldy luggage trolley. At the end of the
corridor, my hand trembles in front of the door. “Quiet, honey,” I tell
myself. “When you were six, you were able to disappear from the world by
simply winding yourself up in a curtain. Why wouldn’t that work now?”

</section>
<section epub:type="chapter" role="doc-chapter">

# Escribe aquí_

# 4. Escribe aquí_ @ignore

## [Pepe Rojo](997a-semblanzas.xhtml#a34) {.centrado}

<script type="text/javascript" src="../js/poetry.js"></script>

Rechazamos ser tratados como humanos. No es un trato digno. No lo
merecemos. {.poesia}

No queremos ser humanos. No nos interesa. {.poesia}

No queremos morir. Ni pensamos hacerlo. {.poesia}

No estamos enfermos. {.poesia}

La infección no nos mató porque una infección nunca mata. Solo permite
que otra cosa viva. {.poesia}

Aprende sus placeres. Coger sin sexo. Todo tu cuerpo una incubadora.
Todo tu cuerpo un orificio. Ábrete. Escápate de ti mismo. Goza como
nosotros. {.poesia}

virusojepse. viruspalabra. viruscerouno. {.poesia}

Soy. Es. El/la/lo. Si. Sí. Output. Input. Fuera. Adentro. Encendido. {.poesia}

Si entiendes, es nosotros. {.poesia}

Sométete. Ríndete. Acepta. {.poesia}

Desdóblate. {.poesia} 

Estamos enamorados de nuestra enfermedad. Siempre. {.poesia}

Lo somos. Lo hacemos. {.poesia}

Ya. {.poesia}

[Escúchame](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/pepe-rojo/) {.centrado}

</section>
<section epub:type="chapter" role="doc-chapter">

# Write Here_

# 4. Write Here_ @ignore

## By [Pepe Rojo](997b-semblances.xhtml#a34) {.centrado}

<script type="text/javascript" src="../js/poetry.js"></script>

We refuse to be treated as humans. No dignity there. Not worthy. {.poesia}

We don’t want to be humans. Not anymore. {.poesia}

We don’t want to die. We won’t. {.poesia}

We are not sick. The disease didn’t kill us, because diseases never do.
They allow something else to live. {.poesia}

Learn its kinks. Asexual fuck. Your body an incubator. All your body an
orifice. {.poesia}

Open up. Escape yourself. Enjoy like us. {.poesia}

rorrimvirus. wordvirus. oh-onevirus. {.poesia}

Am. Is. The. If. Yes. Output. Input. Out. In. On. {.poesia}

If you understand, then it’s us. {.poesia}

Submit. Accept. Surrender. Unfold. {.poesia}

We are in love with our disease. We always have. We are our disease. {.poesia}

We will disease. {.poesia}

Now. {.poesia}

[Listen to me](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/pepe-rojo/) {.centrado}

</section>
<section epub:type="chapter" role="doc-chapter">

# Hambre

# 5. Hambre @ignore

## [Andrea Ciria](997a-semblanzas.xhtml#a06) {.centrado}

La última rama verde del arbusto comenzó a secarse y adoptar el tono
amarillo del resto del árbol. El silencio era total. Las hormigas
muertas no podían devorar los cadáveres de las aves carroñeras caídas,
que a su vez no podían comerse los cuerpos de otros animales que
formaban un tapete infinito de cuerpos inertes. Por primera vez en la
historia, todas las especies de la Tierra, incluido el ser humano,
parecían estar juntas, en una armonía estática y sin vida.

El temerario virus Borax-1 modificó fácilmente la información genética
de las células de los seres vivos para multiplicarse a sí mismo y
engulló hasta la última partícula con vida del planeta. Cuando aquello
comenzó, los científicos del mundo trataron en vano de combatir la
pandemia. Pero no había forma de evitar que las células se convirtieran
en virus, ni vivos, ni muertos, siempre a la espera de un nuevo huésped.
Avezados investigadores aseguraron, con razón, que el Borax-1 era
superior a cualquier otro virus conocido, debido a que podía viajar por
aire y bajo el agua, y convertiría a la Tierra en un sitio yermo. Otros
expertos dudaban de la superioridad del virus porque no solo mutaba con
facilidad para hospedarse en cualquier tipo de célula, sino que era
incapaz de detener su mutación y pronto no encontraría hospederos. En lo
que todos estaban de acuerdo era en que el Borax-1 provenía del espacio.

Seguros de la muerte de la última cucaracha, sin duda las más
resistentes, los creadores del Borax-1 salieron de las profundidades del
mar. Estaban hambrientos. Poco a poco se instalaron en las casas de los
difuntos. Pronto se avistaron pequeñas fogatas por doquier, en las que
quemaban a los cadáveres. Llegó la hora de comer. Los platos de porcelana 
estaban llenos de ceniza. 

</section>
<section epub:type="chapter" role="doc-chapter">

# Hunger

# 5. Hunger @ignore

## By [Andrea Ciria](997b-semblances.xhtml#a06) {.centrado}

The last green branch of the shrub began to dry up and then adopted the
yellow tonality of the rest of the trees. Silence was everywhere. Dead
ants could not devour the corpses of fallen carrion eating birds, which
in turn could not eat the bodies of other animals that made up an
infinite mat of inert bodies. For the first time in history, all species
on Earth, including human beings, seemed to be together, in a static and
lifeless harmony.

The reckless Borax-1 virus easily modified the genetic information of
the cells of living beings to multiply itself and swallowed up all the
living particles of the planet. When all began, the scientists around
the world tried in vain to fight the pandemic. But there was no way to
prevent the cells from becoming viruses, neither alive nor dead, which
were always ready for a new host. Experienced researchers assured, and
they were not far from the truth, that the Borax-1 was superior to any
other known virus because it could travel by air and under water, and it
would turn the Earth into a barren place. Other experts doubted about
the superiority of the virus because besides that it could easily mutate
to stay in any type of cell, it was also unable to stop mutating, and it
would soon ran out of hosts. However, what everyone agreed on was that
the Borax-1 came from space.

When they were certain of the death of the last cockroach,
the most resistant without doubt, the creators of the Borax-1 came out
of the depths of the sea. They were hungry. Little by little they
settled in the houses of the deceased. Soon after, small fires could be
sighted everywhere, in which they burned the corpses. It was time to
eat. The porcelain plates were full of ash.

</section>
<section epub:type="chapter" role="doc-chapter">

![](../img/cody-jimenez_countdown-cuentatras01.jpg)

![](../img/cody-jimenez_countdown-cuentatras02.jpg)

# _Cuenta atrás_ de [Cody Jiménez](997a-semblanzas.xhtml#a44) {.centrado}

Cuando me dieron los diferentes temas para elegir, el tema “Virus / bacterias” me llamó la atención al instante. Hice una pintura titulada _Counting Down_ en 2015, en la que hay un paisaje brumoso, con varias garzas y luces brillantes, misteriosas y en forma de serpiente, que envuelven las garzas. En esa pintura, las serpientes brillantes se veían como un virus, como algo que se iba extendiendo de garza en garza. Hice otra pintura y múltiples bocetos y dibujos explorando esas imágenes para mí. Esta colaboración se convirtió en la manera perfecta de explorar el tema en un medio que siempre me ha encantado, el cómic. Quería ver si podía expresar el movimiento de este intrusivo objeto brillante que imaginé para el mundo ficticio de mis obras.

# [Cody Jiménez](997b-semblances.xhtml#a44), _Countdown_ {.centrado}

# 6. Cuenta atrás / Countdown @ignore

When I was given the different themes to choose from, the “Virus/bacteria” theme instantly caught my eye. I had made a painting entitled _Counting Down_ in 2015, in which there is a foggy landscape, multiple herons, and glowy, mysterious, snake-like lights wrapping around the herons. In that painting, the glowy snakes felt like a virus. Like something that was spreading from heron to heron. I’ve done another painting and multiple sketches and drawings exploring that imagery for myself. This collaboration became the perfect way to explore this in a medium I have always loved, comics. I wanted to see if I could get across the motion of this intrusive glowy object I imagined for the world I created in my painting.

</section>
<section epub:type="chapter" role="doc-chapter">

# La solución más parsimoniosa

# 7. La solución más parsimoniosa @ignore

## [Maia F. Miret](997a-semblanzas.xhtml#a26) {.centrado}

Gracias por quedarse hasta el final del Congreso; sabemos que afuera los
espera un muy buen vino de Na… Sí, pensamos en hacer un póster, pero
los organizadores tuvieron a bien… Por la importancia del autor
principal. Justamente. Entonces, bien. Nos pareció adecuado cerrar con
este tema, de interés meramente teórico, por no decir filosófi… Claro,
nos apuramos entonces. Nadie aquí es ajeno al problema de la taxonomía
viral y la demarcación entre los seres vivos y el ámbito de lo inerte.
Sí, es un problema ya casi superado, claro, aunque no deja de tener su
interés. La cosa es que desarrollamos un método matemático para unificar
conceptualmente el reino de las moléculas orgánicas autorreplican… Sí.
Claro, Mimiviridae principalmente. Regresiones lineales, nada muy
sofisticado, pero creemos que con conclusiones altamente originales. No
hemos publicado, no. En efecto, citamos a Forterre. Sí, la distinción
entre virus y viriones, mire, pero principalmente… Como sea.
Querríamos cerrar con esto y luego pueden ir a disfrutar el coctel. Me
dicen que jamón de bellota, estamos de suerte. Bien. Pues, para resumir,
resolvimos el problema con este modelo, que como decimos es puramente
teórico. Así que la solución más parsimoniosa, y en esto hay que tener
presente que aplica para todos los niveles de organización de la vida,
esto… El continuo vida-no vida. Lo resolvimos. Listo, son libres de
irse. Lo resolvimos. Nada. Nada está vivo. Un enfoque puramente teórico,
como mencionábamos. Gracias por estos minutos.

</section>
<section epub:type="chapter" role="doc-chapter">

# The Most Parsimonious Solution

# 7. The Most Parsimonious Solution @ignore

## By [Maia F. Miret](997b-semblances.xhtml#a26) \
Translated by [Toshiya Kamei](997b-semblances.xhtml#a53) {.centrado}

Thank you for staying until the end of the conference. We know there’s a
great wine from Napa waitying outs… Yes, we thought about making a
poster, but the organizers had… For the main author’s importance.
Rightly so. Well, then. It seemed appropriate to close with this topic,
of merely theoretical interest, if not philosophi… Sure, we hurry
then. No one here is oblivious to the problem of viral taxonomy and
demarcation between living beings and the realm of the inert. Yes, it’s
a problem already almost overcome, of course, although it’s
not uninteresting. The thing is, we developed a mathematical method to
conceptually unify the kingdom of self-replicating organic molecules…
Yes. Sure, Mimiviridae mainly. Linear regressions. Nothing very
sophisticated, but we believe with highly original conclusions. We
haven’t published, no. In fact, we quote Forterre. Yes, the distinction
between viruses and virions, look, but mainly… Whatever. We’d like to
close with this and then you can go enjoy cocktails. I’m told there’s
acorn ham, so we’re in luck. All right. Well, to sum it up, we solved
the problem with this model, which as we say is purely theoretical. So
the most parsimonious solution, and in this we must bear in mind that
applies to all levels of life organization, this… Continuum life-no
life. We solved it. Okay, you’re free to leave. We solved it. Nothing.
Nothing is alive. A purely theoretical approach, as we mentioned. Thank
you for these minutes.

</section>
<section epub:type="chapter" role="doc-chapter">

# Chicle bomba

# 8. Chicle bomba @ignore

## [Gerardo Sifuentes](997a-semblanzas.xhtml#a18) {.centrado}

Un beso para recordar todo. Dos, para asegurarse de que saliera bien.
Eso le dijo. En el primero le pasó el chicle con la lengua y, al
mascarlo, ocurrió lo que creía imposible. La carga se le subió a la
cabeza en apenas segundos. Al principio creyó que todo seguía igual,
pero observó la pantalla de la _tablet_, se puso a contestar el examen y
entonces se dio cuenta. Las fórmulas adquirieron lógica, las ecuaciones
dejaron de ser jeroglíficos. Empezó a comprender aquel lenguaje que le
estaba prohibido. Por primera vez en el año escolar sintió que había
valido la pena aprender todo aquello. Analizó cada pregunta, fue hasta
el último de los rincones de su cerebro y echó a andar la maquinaria
hasta dar con las respuestas. Siempre supo cómo hacerlo, solo faltaba
quien le trazara la ruta. Los virus modificados y en hibernación dentro
del acetato de polivinilo fueron chispas en el polvorín de sus neuronas.
Mascó hasta que la goma perdió su sabor. En la tarde el efecto fue
desvaneciéndose y vino la fiebre. Pero en cuestión de horas y tras una
siesta su organismo quedó como nuevo. Después se enteró de que los besos
no eran necesarios para suministrar la dosis.


</section>
<section epub:type="chapter" role="doc-chapter">

# Bubble Gum

# 8. Bubble Gum @ignore

## By [Gerardo Sifuentes](997b-semblances.xhtml#a18) \
Translated by [Toshiya Kamei](997b-semblances.xhtml#a53) {.centrado}

A kiss to remember everything. Two, to make sure it goes well. You told
me that. With the first kiss, you passed me the gum with your tongue.
When I chewed it, what I thought was impossible happened. The load went
to my head in just seconds. At first, I thought everything was the same,
but I looked at the tablet screen, began to answer the exam, and then
realized. As the formulas made sense, the equations stopped being
hieroglyphic. I began to understand that language off-limits to me. For
the first time in the school year, I felt it was worth learning all
that. I analyzed each question, went to the last corner of my brain, and
turned on the machinery until I found answers. I always knew how to do
it, just missing who would trace the route. The modified and hibernating
viruses within polyvinyl acetate sparked the powder keg of my neurons. I
chewed until the gum lost its flavor. In the afternoon, the effect was
fading and the fever came on. But in a matter of hours and after a nap,
I was like new. Later I learned the kisses weren’t necessary to deliver
the dose.

</section>
<section epub:type="chapter" role="doc-chapter">

# Infección

# 9. Infección @ignore

## [Héctor Octavio González Hernández](997a-semblanzas.xhtml#a19) {.centrado}

---Tenemos que destruir el planeta.

---Wow ---dijo &amp;∗∗&amp;, tratando de no molestar a |}}|, su maestro y
guía---. ¿Es necesario destruirlo solo porque se infectó?

---Esto no es una simple infección ---contestó |}}|, mientras se
acercaba a su alumno---. Ellos ya desarrollaron el intercambio de
servicios o bienes mediante el dinero. Es una potente infección viral
que puede llevar a efectos devastadores. ¡Ocho de las diez razas que
hemos plantado y analizado terminaron destruyéndose! Solo estoy
limpiando todo antes de que se convierta en un desastre más grande.

---Pero… ---dijo &amp;∗∗&amp; tímidamente.

---¿¡Qué!? ---respondió |}}|, visiblemente enojado.

---Es la primera vez que este virus se manifiesta con esta raza. Sí, el
virus del monetarismo ha devastado otros seres. Sin embargo, esta es una
oportunidad única para ver si el planeta !=52 puede resistirlo. Tal vez
podamos analizarlos y aprender cómo pueden subsistir.

---Tu optimismo es bastante peculiar ---se burló |}}|---. Si mal no
recuerdo, el planeta !=52 está bastante aislado, ¿correcto?

---Es un tercer planeta en un sector separado, bastante joven y, si las
cosas se ponen feas, se puede cercar fácilmente con un cinturón de
asteroides.

---&amp;∗∗&amp;, aunque la idea me parece ridícula, tu historial es admirable.
Dejaremos que esta infección se prolongue durante 5 ciclos y después de
eso, los reevaluaremos.

---¡Gracias! ---dijo &amp;∗∗&amp;--- Me siento bastante optimista al respecto.

---Te recomiendo que seas cautamente optimista. La esperanza es fácil de
plantar pero difícil de cosechar.

∗∗∗ {.centrado .espacio-arriba1}

---¿Puedes darme una actualización en el planeta !=52? Dijo |}}|,
bastante satisfecho. {.espacio-arriba1 .sin-sangria}

---Bueno. La situación no es óptima. Las especies dominantes lograron
resistir los efectos del virus del monetarismo y prosperaron bajo este
durante aproximadamente 2.47 ciclos. Sin embargo, el virus mutó y
amenaza con volverse un peligro inminente para todo el planeta.
Realmente tenía grandes esperanzas para esta especie de sangre fría.

---Continúa con la limpieza del sistema, utiliza el asteroide grado
Diez. Eso será suficiente para destruir el planeta.

El mentor dejó a su alumno para que terminara la orden. Sin embargo, 
&amp;∗∗&amp; cambió el grado del asteroide a Siete. Esto no destruiría 
planeta !=52 pero tenía el poder suficiente para darle una segunda oportunidad. 
&amp;∗∗&amp; sintió la esperanza de una cosecha tardía.

</section>
<section epub:type="chapter" role="doc-chapter">

# Infection

# 9. Infection @ignore

## By [Héctor Octavio González Hernández](997b-semblances.xhtml#a19) {.centrado}

“We have to destroy the planet.”

“Wow,” &amp;∗∗&amp; said, cautious to not anger |}}|, his teacher and
mentor. “Destroying the whole planet just because it got infected?”

“This isn’t a simple infection.” |}}| said, while approaching the
student “They have developed the exchange of services or goods by using
_currency_. It’s a potent viral infection that can lead to devastating
effects. Eight out of the ten races we have planted and analyzed ended
destroying themselves! I’m just cleaning up before it becomes a bigger
mess!”

“But…” timidly interjected &amp;∗∗&amp;

“WHAT?!” replied |}}|, visibly angry.

“This is the first time this virus has manifested with this genus. Yes,
the _currency_ has devastated other beings. However, this is a unique
opportunity to see if planet !=52 can resist this virus. Maybe we can
analyze them and learn how they can subsist.”

“Your optimism is quite amusing.” scoffed |}}|. “If I remember
correctly, planet !=52 is quite isolated, correct?”

“It’s a third planet on a separate sector, quite young, and can be
easily fenced with an asteroid belt if things go sour.”

“&amp;∗∗&amp;, although the idea is ridiculous, your track record is
admirable. We’ll let this infection go for 5 cycles and after that,
reevaluate.”

“Thank you!” said &amp;∗∗&amp;, “I’m quite optimistic about it.”

“I recommend you to be cautiously optimistic. Hope is easy to plant but
hard to harvest.”

∗∗∗ {.centrado .espacio-arriba1}

“Can you give me an update on planet !=52?” A slight sense of
satisfaction crossed |}}|’s mind. {.espacio-arriba1 .sin-sangria}

“Well. The situation is not optimal. The dominant species managed to
resist the _currency_ virus effects and thrived under it for about 2.47
cycles. However, it mutated and has led to a downward spiral for the
whole planet. I truly had high hopes for this cold blooded species.”

“Proceed with cleansing the system using Asteroid 10 event. That should
be enough to destroy it.”

The mentor left the pupil to finish the task. However, &amp;∗∗&amp; changed
the asteroid level to 7. Not enough to obliterate planet !=52 but with
enough power to give it a second chance. &amp;∗∗&amp; had a lot of hope for a
late harvest.

</section>
<section epub:type="chapter" role="doc-chapter">

# Transmisible

# 10. Transmisible @ignore

## [Julia Rios](997a-semblanzas.xhtml#a24) \
Traducido por [Andrea Chapela](997a-semblanzas.xhtml#a05) {.centrado}

Cuando llegan, no los oímos ni los vemos. Los buscadores de OVNIs hablan
de señales de radio y luces en el cielo. Los científicos analizan por
mucho tiempo los datos hasta que se percatan de que son solo una
distorsión causada por un horno de microondas en el laboratorio de las
antenas parabólicas. Nunca nos molestamos en buscar qué pasó realmente.

El año en que llegan, la temporada de gripa es particularmente mala. Las
vacunas no nos inmunizan porque no puede cubrir todas las cepas. Muchas
personas mueren. Esto nos deja desolados. Asustados. Romantizamos los
hechos. Se ruedan tres dramas históricos sobre la pandemia de influenza
de 1918, que son aclamados tanto por la crítica como el público. “My
Dick Can Heal Ya Girl” de Lil Jabby llega al primer lugar de los
_rankings_. Habla de cómo, gracias a su milagrosa potencia sexual, sus
espermas son capaces de curar la enfermedad. Lil Jabby muere de gripa,
contagiado por una de sus _fans_.

Algunos sobrevivimos, pero el virus permanece en nuestro sistema. Justo
cuando pensamos que ha desaparecido, regresa con más fuerza. No
entendemos que este sufrimiento es un crecimiento. Que el virus es
información. Que nuestros bebés y los bebés de nuestros bebés lo
recordarán.

No nos daremos cuenta de lo que sucedió hasta generaciones más tarde,
cuando hayamos evolucionado. Cuando nuestros científicos descubran que
este fue el primer contacto. Que nos colonizaron. Que nos asimilaron.
Que no podemos distinguir un _nosotros_ de un _ellos_ y la idea de
intentarlo nos horroriza. Celebramos la vida de Lil Jabby y de otros
como él. Nos lamentamos porque nunca se convirtieron en nosotros, aunque
siempre están con nosotros. Recordamos a nuestros padres y a nuestros
abuelos e incluso las vidas de todas las criaturas que vivieron hace
tanto tiempo que apenas se parecen a nosotros, excepto, nos percatamos
ahora, de que ellos son nosotros. Nosotros somos ellos. Compartir la
Tierra nos hace un macroorganismo. Pero también estamos conectados a
otros lugares, a otros planetas. Somos la distorsión de horno de
microondas en la antena parabólica. La llamada desde el interior de la
casa.

</section>
<section epub:type="chapter" role="doc-chapter">

# Communicable

# 10. Communicable @ignore

## By [Julia Rios](997b-semblances.xhtml#a24) {.centrado}

When they come, we don’t hear or see them. UFO enthusiasts talk about
radio signals and lights in the sky. Scientists analyze a strange set of
data for ages before they work out it’s just a distortion from a
microwave oven at the facility where the satellite dishes are listening.
We never think to look for what actually happens.

The year they come, flu season is particularly bad. The vaccine doesn’t
inoculate us against it because it can’t account for every strain. Many
of us die. We are devastated. We are afraid. We romanticize it. Three
separate period dramas about the 1918 influenza pandemic come out to
critical and popular acclaim. “My Dick Can Heal Ya Girl” by Lil Jabby
tops the pop music charts. It’s about how, through the miraculous powers
of sexual potency, his sperm will cure disease. Lil Jabby, dies of the
flu, which he catches from a groupie.

Some of us survive, but the virus hangs on in our systems. Just when we
think it will go away, it comes back again, stronger. We do not
understand that this suffering is growth. That the virus is information.
That our babies and our babies’ babies will remember.

We do not realize what has happened until generations later, when we
have evolved. When our scientists work out that this was first contact.
That we are colonized. That we are assimilated. That we cannot untangle
_us_ from _them_, that the idea of trying is horrifying. We celebrate
Lil Jabby and the others like him. We mourn them because they never
became us, yet they are always with us. We remember our parents and our
grandparents and the lives of creatures who came so far before us that
they hardly seem like us at all, except, we realize now, they _are_ us.
We are them. By sharing the Earth we are all one macroorganism. And,
too, we are connected to other places, other planets. _We_ are the
microwave oven distortion in the satellite dish. _We_ are the call
coming from inside the house.

</section>
<section epub:type="chapter" role="doc-chapter">

![](../img/richard-zela_vidaotro-lifeanother.jpg)

# _La vida en otro universo_ de [Richard Zela](997a-semblanzas.xhtml#a50) {.centrado}

Me inspiré en el articulo de los [virus](https://www.sciencealert.com/cholera-bacteria-using-pili-to-harpoon-dna-horizontal-gene-transfer-antibiotic-resistance) y el video de los [multiuversos](https://www.youtube.com/watch?v=kpJ51h7bi8g), que me hicieron imaginar un universo donde lo microscópico es gigante.

# [Richard Zela](997b-semblances.xhtml#a50), _Life In Another Universe_  {.centrado}

# 11. La vida en otro universo / Life In Another Universe @ignore

I find inspiration both in the [virus’s article](https://www.sciencealert.com/cholera-bacteria-using-pili-to-harpoon-dna-horizontal-gene-transfer-antibiotic-resistance) and the [video about multiverse](https://www.youtube.com/watch?v=kpJ51h7bi8g), it made me imagine a universe where the microscopic is gigantic. 

</section>
<section epub:type="chapter" role="doc-chapter">

# Lengua parasita

# 12. Lengua parasita @ignore

## [Mónica Nepote](997a-semblanzas.xhtml#a31) {.centrado}

<script type="text/javascript" src="../js/poetry.js"></script>

[No](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[es](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[una,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[soy](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[ellas](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
{.poesia}

[se](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[reproduce,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[de](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[sí](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[misma,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[se](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[aísla,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[vuelve](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[toma](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[otra](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[forma](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
{.poesia}

[sequedad](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[dentro,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[ese](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[fuego,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[esa](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[piedra](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
{.poesia}

[No](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[es](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[una,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[soy](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[ellas](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
{.poesia}

[tengo](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[que](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[pensar](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[en](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[el](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[hilo,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[tengo](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[pensar](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[que](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[esa](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[piedra](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[de](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[fuego](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[tiene](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[una](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[fisura,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[una](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[gota](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
{.poesia}

[se](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[divide](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[se](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[multiplica](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[una](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[idéntica](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[pronto](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[toma](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[otra](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[forma](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
{.poesia}

[y](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[luego](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[otra](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[se](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[divide](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[multiplica](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[otra](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
{.poesia}

[con](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[otra](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[con](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[otra](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[con](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[otra](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[soy](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[cada](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[vez](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[menos](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
{.poesia}

[soy](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[mi](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[cuerpo](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[soy](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[su](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[cuerpo,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[su](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[planeta](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
{.poesia}

[colonias,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[territorio](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[renombrado](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
{.poesia}

[en](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[una](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[lengua](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[de](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[ritmos,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[de](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[iridiscencia,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[de](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[multiplicidad](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
{.poesia}

[soy](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[era](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[aire,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[soy](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[turba,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[soy](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[invasión,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[desconozco](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[me](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[desconozco](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
{.poesia}

[soy](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[su](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[plaza,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[tomada,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[anida,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[se](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[escurren,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[no](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[ventilo](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
{.poesia}

[soy](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[su](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[lengua](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[soy](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[su](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[familia](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
{.poesia}

[es](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[nuestra,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[territorio,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[nutriendo,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[condición,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[atmósfera](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
{.poesia}

[brincamos,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[duele](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[separarse,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[eres](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[tú,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[te](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[reconozco](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
{.poesia}

[no](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[eres](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[tú](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[es](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[otra](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[eres](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[yo](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[soy](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[somos](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
{.poesia}

[latigueo,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[reproduzco,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[invasión,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[detención,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[carcoma](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
{.poesia}

[eres](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[tú](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[te](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[pareces](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[soy](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[tú](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[padres](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[madres](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[hermanas](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[animales](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[nutrientes](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
{.poesia}

[raíces](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[escándalos,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[ritmos,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[reproducción,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[cada](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[vez](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[más](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[otra](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[idéntica](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[no](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[únicas](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
{.poesia}

[decenas,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[cientos,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[miles](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[pequeñas](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[conductos](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[fluidos,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[traslado](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[velocidad](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
{.poesia}

[detención](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[áreas](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[azules,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[verdes,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[rojas,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[alfabetos,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[lenguajes,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[signos](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
{.poesia}

[no](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[eres](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[tú](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[es](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[el](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[látigo](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[colonias](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[colonias](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[seres](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[diminutos](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[comunitarios](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
{.poesia}

[contacto](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[comunicación](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[expansión](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[reflejo,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[aprendizaje](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
{.poesia}

[condición](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[de](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[vida](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[de](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[muerte](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[desaparece](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
{.poesia}

[ella](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[no](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[es](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[ella](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[es](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[el](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[medio](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
{.poesia}

[somos](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[ella](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[somos](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[vive](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[te](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[digo](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[vive](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[te](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[decimos](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
{.poesia}

[sostén](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[esa](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[respiración](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[no](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[luches](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[somos](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[tú](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[eres](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[nuestra.](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
{.poesia}

</section>
<section epub:type="chapter" role="doc-chapter">

# Parasitic Language

# 12. Parasitic Language @ignore

## By [Mónica Nepote](997b-semblances.xhtml#a31) \
Translated by [Toshiya Kamei](997b-semblances.xhtml#a53) {.centrado}

<script type="text/javascript" src="../js/poetry.js"></script>

[It’s](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[not](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[one,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[I’m](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[them](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
{.poesia}


[It](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[reproduces](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[itself,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[isolates](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[itself,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[returns,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[takes](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[another](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[form](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
{.poesia}


[dryness](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[inside,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[that](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[fire,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[that](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[stone](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
{.poesia}

[It’s](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[not](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[one,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[I’m](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[them](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
{.poesia}


[I](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[have](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[to](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[think](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[about](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[the](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[thread,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[I](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[have](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[to](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[think](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[the](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[stone](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[of](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[fire](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[has](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[a](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[fissure,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[a](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[drop](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
{.poesia}


[is](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[divided](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[multiplies](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[an](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[identical](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[one](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[soon](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[takes](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[another](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[form](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
{.poesia}


[and](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[then](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[another](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[one](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[divides](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[multiplies](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[another](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
{.poesia}


[with](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[another](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[with](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[another](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[with](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[another](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[I’m](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[less](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[and](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[less](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
{.poesia}


[I’m](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[my](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[body,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[I’m](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[its](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[body,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[its](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[planet](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
{.poesia}

</section>
<section epub:type="chapter" role="doc-chapter">

# Sueño bacterial

# 13. Sueño bacterial @ignore

## [Cecilia Eudave](997a-semblanzas.xhtml#a09) {.centrado}

Soñé que me soñaban, me han dicho que es el principio del fin. Desperté
sudando, envuelta en una nebulosa caliente, la respiración agitada, la
vista aturdida aún con restos de mis sueños confundidos en los suyos.
Estoy segura, no es una infección cualquiera, las bacterias van ganando
terreno. Cuando me enteré de que hacían transferencias horizontales a mi
ADN para confundirlo y resistir, entré en paranoia. ¿Cómo lo hacen?, es
difícil de creer: ¡son peludas! Arponean mi pobre ácido
desoxirribonucleico con vellos tan delicados, tan resistentes como el
hilo de Ariadna. Imaginé mi cuerpo entrecruzado por pelos formando una
red intrincada de conexiones increíblemente sensibles alrededor de mi
cerebro, creando un enorme capullo en donde anidar sus pesadillas y
controlarme. Terrible. Por eso me inscribí en un grupo de apoyo que sabe
cómo frustrar bacterias. Algunos de sus miembros llevan años probando
todo tipo de antibióticos y tratamientos experimentales. Les conté mi
caso, no podían dar crédito a lo que las ingratas estaban consumiendo.
“¿Quién eres sin tus sueños? Son unas perversas, ve sin piedad tras
ellas”. Me recomendaron la fagoterapia: bacterias come bacterias.

¿Funcionará? Las que me habitan son muy fieras, están decididas a
infectarme con sus recuerdos milenarios hasta asimilarme. Parecen un
pueblo repudiado, maldito, reproducen sus malignos modos, sus conductas
nefandas hasta el infinito para seguir persistiendo. Saben refugiarse en
el dormir o en el delirio, de los seres vivos su más primitiva y
poderosa esencia vital, así van de cuerpo en cuerpo, se hacen eternas.
No sé qué vieron en mí y no en otro, posiblemente les gusta cómo sueño,
o les gusta cómo me sueñan. Resulté un buen caldo de cultivo.

Tal vez no sea tan malo si pierdo esta guerra, quizá perviva algo de mi
existencia en una pequeña escena onírica recluida y repartida entre
todas ellas. O tal vez, nos quieren recordar que nosotros somos a su vez
bacterias dentro de otro organismo agonizante, siempre con hambre
egocéntrica, siempre soñando que alguien más nos sueña.

</section>
<section epub:type="chapter" role="doc-chapter">

# Bacterial Dream

# 13. Bacterial Dream @ignore

## By [Cecilia Eudave](997b-semblances.xhtml#a09) \
Translated by [Toshiya Kamei](997b-semblances.xhtml#a53) {.centrado}

I dreamed I was in someone else’s dream, which is supposed to 
be the beginning of the end. I awoke in a sweat, wrapped in a hot 
nebula and out of breath, my gaze still dazed with remnants of my 
dreams mixed with theirs. Surely this is no ordinary infection. 
In fact, the bacteria are gaining ground. When I found out they 
were making horizontal transfers to my DNA to confuse it and resist, 
I became paranoid. How do they do it? It’s hard
to believe, but they’re hairy! They spear my poor deoxyribonucleic acid
with fine hairs as strong as Ariadne’s thread. I imagined hairs that
formed an intricate network of incredibly sensitive connections around
my brain crisscrossing my body, creating a huge cocoon to nest their
nightmares in and control me. Horrible. That’s why I signed up for a
support group that knows how to thwart bacteria. Some of its members
have been trying all kinds of antibiotics and experimental treatments
for years. I told them about my case, but they didn’t believe what the
wretched bacteria were devouring: “Who are you without your dreams? The
bacteria are evil. Go get them, show no mercy.” They recommended
phagotherapy: bacteria-eating bacteria.

Will it work? The bacteria that inhabit me are very fierce, determined
to infect me with their millennial memories until I become assimilated.
They seem like a condemned, cursed tribe, reproducing their evil ways
and their nefarious behaviors until infinity in order to linger on. They
know how to take refuge in sleep or delirium. Sucking their most
primitive and powerful vital essence from living beings, they go from
body to body and last forever. I have no idea what they saw in me rather
than another person. It’s possible they like how I dream or how they
dream about me. I’ve turned out to be a great breeding ground.

Maybe it’s not quite so bad even if I lose this battle. Maybe something
of my existence survives in a small dream scene, secluded and
distributed among all of them. Or maybe they want to remind us that
we’re, in turn, bacteria within another dying organism, always with
self-serving hunger, always dreaming that someone else dreams about us.

</section>
<section epub:type="chapter" role="doc-chapter">

# Vacuna núm. 5

# 14. Vacuna núm. 5 @ignore

## [Mario Acevedo](997a-semblanzas.xhtml#a28) {.centrado}

Cuando el virus T196 golpeó, la plaga mortal corrió sin obstáculos de
aeropuerto en aeropuerto, de ciudad en ciudad, de hogar en hogar. En
pocas semanas murieron millones. Entonces se detuvo misteriosamente.

Mi equipo trabajó hasta desentrañar los secretos de la enfermedad y por
qué no había acabado con todos. Fui convocado al Centro para la
Sostenibilidad del Planeta.

Adele Haddon, jefa del comité de recursos estratégicos, me preguntó:

---Doctor Mendoza, ¿qué detuvo la plaga?

---Nuestro sistema inmunológico se ajusta y limita la mortalidad al
quince por ciento.

---¿Sería posible mutar el virus T196 para superar el sistema
inmunológico de una persona? Yo sabía a qué se refería:

---Ciertamente, pero entonces nuestro sistema inmunológico compensará
una vez más. Mi investigación muestra que la mortalidad se mantendría en
un quince por ciento por cada cepa viral.

---Ya leíste nuestro resumen. En todo el mundo estamos casi en el punto
de no retorno. El control extremo de la población es la única opción
práctica para revertir una larga lista de males: violencia social,
agotamiento de recursos y destrucción del medio ambiente.

Yo había hecho mi tarea:

---Podríamos administrar la plaga mutada como una vacuna. Cuatro
iteraciones de cepas sucesivas en intervalos de seis meses nos llevara a
más del cincuenta y dos por ciento.

---Nuestra meta es cincuenta por ciento.

Lo consideré.

---Para alcanzar el objetivo se podría requerir una quinta vacuna
especial. En realidad, un sustituto fatal administrado selectivamente
hasta alcanzar ese cincuenta por ciento.

∗∗∗ {.centrado .espacio-arriba1}

Mi participación en esa conspiración no me quitó el sueño. Después de
todo, estábamos salvando al planeta. Dos años después, me llamaron para
que informara a Haddon sobre nuestro éxito. Pero a mi llegada, los
guardias me arrastraron a un laboratorio. Haddon y un técnico médico
estaban esperando. {.espacio-arriba1 .sin-sangria}

Los guardias me sujetaron.

---Cometiste un asesinato monstruoso ---dijo Haddon.

---¿Yo? ---grité--- ¡Fue idea tuya!

---Sí ---respondió ella.

El técnico preparó una jeringa con la etiqueta _Vacuna núm. 5_.

---Y la primera regla de cualquier asesinato ---continuó--- es hacer
justicia a los asesinos. Además, todavía no estamos al cincuenta por
ciento.

</section>
<section epub:type="chapter" role="doc-chapter">

# Vaccine No. 5

# 14. Vaccine No. 5 @ignore

## By [Mario Acevedo](997b-semblances.xhtml#a28) {.centrado}

When the T196 virus hit, the deadly plague raced unimpeded from airport
to airport, city to city, home to home. Within weeks, millions died.
Then the plague mysteriously halted.

My team labored to unravel the secrets of this disease, and why it
stopped just before wiping us all out. Shortly afterwards I was summoned
to the Center for Planet Sustainability.

Adele Haddon, chief of the strategic resources committee asked me, “Dr.
Mendoza, what stopped the plague?”

“Our immune systems adjust and cap mortality at fifteen percent.”

“Would it be possible to mutate the T196 virus to overcome a person’s
immune system?”

I knew what she was getting at. “Certainly, but then our immune systems
will compensate once again. My research shows that mortality would
remain at fifteen percent for each viral strain.”

Haddon said, “You’ve read our summary. Globally, we’re almost at the
point of no return. Extreme population control is our only practical
option for reversing a long list of ills: social violence, resource
depletion, and destruction of the environment.”

I’d done my homework. “We could administer the mutated plague as a
vaccine. Four iterations of successive strains at six month intervals
would get us to above 52 percent.”

“Our goal is fifty percent.”

I considered this. “To achieve the target level might require a special
fifth ’vaccine’ ---actually a fatal substitute. Selectively administered
until you achieve fifty percent.”

∗∗∗ {.centrado .espacio-arriba1}

I lost no sleep over my part in this conspiracy. After all, we were
saving the planet. Twenty-four months later I was invited to brief
Haddon about our ongoing success. But upon my arrival, guards dragged me
to a laboratory. Haddon and a medical tech were waiting. {.espacio-arriba1 .sin-sangria}

The guards held me down.

“It was a monstrous assassination you committed,” Haddon said.

“Me?” I shrieked. “It was your idea!”

“True,” she replied.

The tech readied a syringe labeled _Vaccine No. 5_.

“And the first rule of any assassination,” Haddon said, “is to do
justice to the assassins. Besides, we’re not yet at fifty percent.”

</section>
<section epub:type="chapter" role="doc-chapter">

# El origen de las sinapsis

# 15. El origen de las sinapsis @ignore

## [Andrea Chapela](997a-semblanzas.xhtml#a05) {.centrado}

Te propongo la siguiente escena. Dos personas. Un concierto. Una copa
que se derrama y a raíz de eso una conversación. Una cascada de hormonas
que hace que una de estas personas le diga a la otra: “¿Te vienes a mi
casa?”. Ya allí hay muchas casualidades. Las probabilidades siempre
están en nuestra contra. Si nos detenemos a pensarlo nos damos cuenta de
que debe haber muchísimos más momentos que no ocurren, más mutaciones
que no se dan, más encuentros que no suceden. Por ejemplo,
¿sabías que el 8% de nuestro ADN es material genético que viene de un
virus? No del mismo virus. Muchos. Que infectaron a alguno de nuestros
antepasados antes de que fuéramos humanos, cuando éramos un
protomamífero. Es verdad, lo leí en un artículo científico. Decía que no
sabemos qué hace todo ese material genético, solo algunas partes. Creen
que uno de esos genes es muy importante para las sinapsis. Podría ser
que generemos consciencia por una casualidad tan pequeña como que
nuestro antecesor se enfermara hace cien millones de años. A eso me
refiero cuando hablo de casualidad. Y es que es importante reconocer el
papel que juega a todos los niveles. Como ahora mismo, cuando por algún
azar este texto llegó a tus manos y te propuse una escena, te conté
algo. Algo pasó entre tú y yo hace un momento, ¿no crees? Está pasando.
Ahora mismo. Aquí también hay un encuentro, no tan obvio como el de las
dos personas en el concierto que se irán juntas a casa, se conocerán, se
contarán sus vidas, pasarán una, dos, tres noches juntas o tal vez
muchos años. No como el virus que se mete en el cuerpo de un mamífero,
deja un par de genes y lo cambia todo. Pero esto es un encuentro que
también puede cambiarnos. Abres un libro, una página al azar y ¡pum!:
una conexión. Tan frágil, tan casual este encuentro. Dos personas que
comparten un texto.

</section>
<section epub:type="chapter" role="doc-chapter">

# When Synapsis Began

# 15. When Synapsis Began @ignore

## By [Andrea Chapela](997b-semblances.xhtml#a05) {.centrado}

I offer you the following scene. Two people. A concert. A spilled drink
that starts a conversation. A cascade of hormones that makes one of them
say, _Want to come back to my place?_ There, just in that one moment we
already have so many coincidences. Probability is always against us. If
we stop and think about it, we realize that the number of moments that
didn’t happen, mutations that didn’t take place, encounters that didn’t
come to be, is far greater than those that did. For example, did you
know that 8% of our DNA comes from the genetic material of viruses? And
not just one virus. Many. They infected one of our ancestors before we
were human, when we were some kind of proto-mammal. It’s true. I read it
in a scientific article. It said that no one knows what all that genetic
material is for. But they think some of those genes are vital to
synapsis. Maybe our consciousness exists by chance, only because one of
our ancestors got sick with the right virus a hundred million years ago.
That’s what I mean by coincidence. And it’s important to recognize the
role it plays on every level of our lives. Like right now, when you
stumbled upon this piece and I offered you a scene. I shared something
with you. Something happened between us a moment ago, right? It’s still
happening. Right now. Just a chance encounter, and we met. It isn’t an
obvious example, like the one where two people bump into each other at a
concert, go home together, talk about their lives. They spend one, two,
three nights together, maybe even years. It isn’t like the
virus that infects a mammal, leaves behind a couple of genes, and
changes everything. But this meeting---our meeting---could change us,
too. You open a book, find a page randomly, and _bam!_, connection
happens. It’s so fragile, so unexpected. Miraculous. Just two people
sharing a text.

</section>
<section epub:type="chapter" role="doc-chapter">

# Van a llamarme Dios

# 16. Van a llamarme Dios @ignore

## [Mariana Palova](997a-semblanzas.xhtml#a27) {.centrado}

<script type="text/javascript" src="../js/poetry.js"></script>

Pero por ahora solo soy diminuto. Insignificante. Parásito. {.poesia}

Mi hambre los enferma, los retuerce y los convulsiona. {.poesia}

Los saca a rastras de los mares, los arrastra por la arena helada. {.poesia}

Despacio, poco a poco, hueso a hueso. {.poesia}

Miro, quieto en mi inexistencia, cómo el mundo nace. {.poesia .espacio-arriba1}

Los veo sufrir con la punzada de mis dientes. {.poesia}

Con el calor de cientos de soles. {.poesia}

Y el dolor los cambia. Los adapta. {.poesia}

Los vuelve monstruos de dos patas. {.poesia}

Y ellos ahora solo contemplan, perplejos, cómo tuerzo sus células {poesia .espacio-arriba1}

y sus filamentos y los convierto en entrañas. {.poesia}

En cómo tejo sus membranas y escamas y las convierto en pieles. {.poesia}

En cómo rompo sus aletas y tentáculos, y los convierto en piernas. {.poesia}

Y mi pequeñez me volverá omnisciente, absoluto. E indudable. {.poesia .espacio-arriba1}

Me llamarán Gran Espíritu, me llamarán Coatlicue. {.poesia .espacio-arriba1}

Me llamarán Creador. Me llamarán Mercenario. {.poesia}

Me suplicarán sobre altares, me clamarán desde las sinagogas. {.poesia}

¿Quién nos ha creado? Gritarán los incrédulos {.poesia}

y cuando me descubran, desataré su rabia los unos contra los otros. {.poesia}

Pero en algo siempre tendrán certeza, {.poesia .espacio-arriba1}

y es que siempre fui yo su creador. {.poesia}

Después, me llamarán Ciencia. Me llamarán Evolución. {.poesia .espacio-arriba1}

Pero, durante milenios, van a llamarme Dios. {.poesia}

</section>
<section epub:type="chapter" role="doc-chapter">

# They Will Call Me God

# 16. They Will Call Me God @ignore

## By [Mariana Palova](997b-semblances.xhtml#a27) {.centrado}

<script type="text/javascript" src="../js/poetry.js"></script>

But now, I’m just tiny. Insignificant. A parasite. {.poesia}

My hunger ill’s them, wring them and convulses them. {.poesia}

It dragges them out of the seas, dragges them along the icy sand. {.poesia}

Slowly, little by little, bone by bone. {.poesia}

I look, still in my nonexistence, how the world is born. {.poesia .espacio-arriba1}

I see them suffering with the twinge of my teeth. {.poesia}

With the heat of hundreds of suns. {.poesia}

And the pain changes them. And adapts them. {.poesia}

It makes them two-legged monsters. {.poesia}

And now, they only contemplate, perplexed, how I thigh their cells {.poesia .espacio-arriba1}

and their filaments, and then I turn them into bowels. {.poesia}

In how I weave their membranes and scales and turn them into skins. {.poesia}

In how I break their fins and tentacles, and turn them into legs. {.poesia}

And my littleness will make me omniscient, absolute. And undoubted. {.poesia .espacio-arriba1}

They will call me Great Spirit, they will call me Coatlicue. {.poesia .espacio-arriba1}

They will call me Creator. They will call me Mercenary. {.poesia}

They will beg me in their altars, they will cry out for me from the 
synagogues. {.poesia}

Who created us? The unbelievers will scream {.poesia}

and when they discover me, I will unleash their rage against each other. {.poesia}

But they will always have certainty about something {.poesia .espacio-arriba1}

and it’s that I have always been their creator. {.poesia}

Then, they will call me Science. They will call me Evolution. {.poesia .espacio-arriba1}

But, for millenia, they will call me God. {.poesia}

</section>
<section epub:type="chapter" role="doc-chapter">

![](../img/mariana-palova_vientreinfin-infinitewomb.jpg)

# _Vientre infinito_ de [Mariana Palova](997a-semblanzas.xhtml#a27) {.centrado}

Como es la primera vez que trato con temas de esta índole, fue muy complicado para mí inclinarme hacia lo científico, a pesar de ser el tema central, por lo que tanto mi texto como mi obra terminaron dándole una personificación a algo tan intangible como la adaptación al medio natural; algo que, a pesar de su complejidad científica, siempre parece tener mucho misticismo. El bestiario que conforma la propia naturaleza parece infinito y fantástico, aún siendo real y tangible, lo que ha despertado mi imaginación desde que tengo memoria. Encontrar este lazo entre mi obra y algo totalmente desconocido en el ámbito artístico para mí: la creación de ideas a partir de hechos científicos, fue tan sorpresivo, que la creación final del texto (de la cual había descartado montones de ideas antes) nació de forma espontánea y prácticamente en los últimos minutos de entrega.

# [Mariana Palova](997b-semblances.xhtml#a27), _Infinite womb_ {.centrado}

# 17. Vientre infinito / Infinite womb @ignore

As it was the first time I’ve dealt with science fiction in my art, it was very complicated for me to get my bearings and work with the central theme. Consequently, both my text and my art ended up personifying something as intangible as natural adaptation, a process that, despite its scientific complexity, always seems filled with mysticism. The living bestiary of nature itself seems infinite and fantastic, even though it is real and tangible, awakening my imagination since I can remember. Finding this link between my work and something totally unknown in the artistic field for me---the creation of ideas from scientific facts--- was so surprising that the final creation of the text (from which I had discarded many ideas) was born spontaneously and practically in the last minutes before the deadline.

</section>
<section epub:type="chapter" role="doc-chapter">

# KIC 5520878 con variaciones y fuga

# 18. KIC 5520878 con variaciones y fuga @ignore

## [Martha Riva Palacio Obón](997a-semblanzas.xhtml#a29) [intercambiable por autora 1] {.centrado}

El movimiento de un péndulo [intercambiable por memoria de X] tiende a
seguir un patrón que incluye el conjunto de todos sus comportamientos
posibles. Los desplazamientos ---bucles infinitos--- de un sistema
caótico a través de sus atractores se conocen como fractales. No podemos
predecir cómo es que el aleteo de una mariposa [intercambiable por
recuerdo 1] se transformará en huracán [intercambiable por recuerdo
2], porque desconocemos las condiciones iniciales. Dentro del pulso de
una estrella puede ocultarse una segunda variación de origen
desconocido. Puede ser que el radio de dos frecuencias sea de 1.618 y
puede ser que en el momento preciso en el que me rompo los brazos, un
físico en Hawaii [intercambiable por John Learned] conecte el ritmo de
KIC 5520878 con la proporción áurea. Crecimiento Laplaciano, fractura
radial bilateral: las condiciones iniciales que desembocan en la
construcción de la historia de mi vida, comienzan mucho antes de que yo
nazca [intercambiable por variable extraña]. Proporción áurea,
anatomía cósmica. Me gusta más el San Juan Bautista de Leonardo que la
Mona Lisa [ambos cuadros intercambiables por las chanclas azules que
traía puestas el día que los vi en el Louvre]. Andar en bicicleta, la
espiral de un nautilo [intercambiable por amonita]. Mis fracturas son
también la fractura de muñeca de mi mamá cuando yo era niña y pasaba el
día [intercambiable por todos los veranos de mi infancia] en la
alberca. El calcio que vuelve al mar y la cadera de mi abuela
[intercambiable por (vacío)]. Dentro del _Contrapunto No. 17b:
Inverso_ de Bach, están contenidas todas las fugas que compondrá la
última versión del algoritmo que ahora me toma dictado [intercambiable
por autora 2]. Mis brazos rotos se mueven distinto, tienen un desfase
con el resto de mi cuerpo. Balanceo pendular de extremidades superiores,
retorno a un pasado homínido [intercambiable por proyección a un futuro
post-humano]. El movimiento de un péndulo [intercambiable por memoria
inversa de X] tiende a seguir un patrón que incluye el conjunto de
todos sus comportamientos posibles.

</section>
<section epub:type="chapter" role="doc-chapter">

# KIC 5520878 with variations and fugue

# 18. KIC 5520878 with variations and fugue @ignore

## By [Martha Riva Palacio Obón](997b-semblances.xhtml#a29) [interchangeable with author 1] {.centrado}

The movement of a pendulum [interchangeable with X’s memory] tends to
follow a pattern that includes the subset of all its possible behaviors.
The displacements ---infinite loops--- of a chaotic system through its
attractors are known as fractals. We can’t predict how the flap of the
butterfly’s wings [interchangeable with reminiscence 1] becomes a
hurricane [interchangeable with reminiscence 2], because we ignore the
initial conditions. The pulse of a star may conceal a second variation
of unknown origin. It’s possible that the ratio of two frequencies is
1.618 and it’s possible that in the precise moment that I break my arms,
a physicist in Hawaii [interchangeable with John Learned] connects the
rhythm of KIC 5520878 with the golden mean. Laplacian growth, bilateral
radial head fracture: the initial conditions that result into the story
of my life begin long before I’m born [interchangeable with strange
variable]. Golden mean, cosmic anatomy. I prefer Leonardo’s Saint John
the Baptist than the Mona Lisa [both paintings are interchangeable with
the blue flip-flops I wore the day I saw them at the Louvre]. To ride a
bike, the spiral of a nautilus [interchangeable with ammonite]. My
fractures are also my mother’s broken wrist and spending the day
[interchangeable with all the summers of my childhood] at the pool.
The calcium that flows back to the sea and my grandmother’s hip
[interchangeable with (void)]. Inside _Bach’s Contrapunctus No. 17b:
Inversus_ are all the fugues that will compose the last version of the
algorithm that now takes my dictation [interchangeable with author 2].
My broken arms move differently, they are desynchronized with the rest
of my body. Pendular swinging of higher extremities, return to a hominid
past [interchangeable with projection into a post-human future]. The
movement of a pendulum [interchangeable with X’s inverse memory] tends
to follow a pattern that includes the subset of all its possible
behaviors.

</section>
<section epub:type="chapter" role="doc-chapter">

# Océano viral

# 19. Océano viral @ignore

## [Cisteil X. Pérez](997a-semblanzas.xhtml#a39) {.centrado}

_An inefficient virus kills its host. A clever virus stays with it_. \
James Lovelock {.epigrafe}

Parece extraño que, siendo seres vivos, no podamos distinguir fácilmente
lo vivo de la materia inerte. Pero sucede que, por décadas, el debate
acerca del estatus ‘vivo/no vivo’ de los virus y su no-clasificación
como ‘organismos’ ha permanecido abierto. Lo que pasa es que los virus
son, digamos, bastante especiales. Por ejemplo, se les considera
entidades ‘no vivas’ porque tienen fases en las que son completamente
inertes; son minúsculos ---cerca de 50 veces más pequeños que muchas
bacterias--- y, además, no son capaces de reproducirse por sí solos,
requieren forzosamente de una célula hospedera para poder multiplicarse
y heredar sus genes. Por otro lado, sea como sea sí se reproducen,
tienen capacidad para producir sus propios genes y evolucionan, lo que
los ubica como entidades vivas. Son estas características las que
continúan desafiando los conceptos típicos que usamos para definir a los
organismos y lo que mantiene abierto el debate sobre el concepto mismo
de _vida_.

Recientemente, nuevos hallazgos parecen haber inclinado la balanza en
favor del estado vivo de los virus: se encontró que las partículas
virales en los ecosistemas naturales son mucho más diversas y abundantes
de lo que se creía, quizá aún más que las células; también se determinó
que los virus que infectan especies de las tres principales ramas que
conforman el árbol de la vida ---dominios Bacteria (bacterias
verdaderas), Eukarya (protistas, algas, plantas, animales y hongos) y
Archaea (similares a las bacterias, pero extremófilos)--- tienen una
relación evolutiva muy cercana entre ellos, establecida hace miles de
millones de años; y se han detectado virus gigantes, es decir, con
tamaño y contenido genético comparable al de muchas células bacterianas.
Entonces, en caso de ser entidades vivas, ¿en qué parte del árbol de la
vida tendríamos que añadir a los virus?

Uno de los principales problemas para resolver este conflicto es que,
cuando añadimos los distintos grupos de virus al árbol de la vida, estos
quedan repartidos en todas las ramas que derivan de los tres dominios,
debido a que hay muchos genes compartidos entre virus y organismos
celulares. Esto se explica parcialmente por el modo en que los virus se
multiplican: infectan una célula, secuestran su maquinaria genética y la
reprograman para que haga copias de su genoma; y en el proceso, pueden
robar genes de su célula huésped y mezclarlos con los suyos para
adquirir una mayor capacidad de infección celular que heredarán a su
progenie. Esta estrategia es tan exitosa que sus huéspedes han
desarrollado muchas tácticas de defensa contra virus y, en muchos casos,
ha resultado mejor establecer una relación mutualista con el virus antes
que luchar contra él.

Algunas de esas relaciones han estado tan íntimamente ligadas durante
tanto tiempo que el material genético de los participantes está muy
entrelazado y muchos virus mutualistas ya son esenciales para la
supervivencia de sus huéspedes. Es el caso de la interacción que ocurre
entre virus y avispas parasitoides de las familias Brachonidae e
Ichneumonidae. Las avispas parasitoides ponen sus huevos dentro de sus
huéspedes (orugas, arañas, pulgones, entre muchos otros) para que cuando
las larvas emerjan, puedan alimentarse poco a poco de su huésped,
mientras este sigue vivo, hasta alcanzar la madurez suficiente para
salir al exterior, al mejor estilo del _Octavo pasajero_. Sin embargo,
hay un pequeño detalle y es que esto no podría suceder sin la ayuda del
virus que acompaña a los huevos cuando su madre los deposita. Pues sin
él, el huevo interpretaría su nuevo ambiente como una amenaza y
detendría su desarrollo (ahora que lo pienso, ¿sería posible que los
xenomorfos utilizaran virus para parasitar a sus hospederos?). Una
situación similar ocurre desde hace millones de años entre virus y
mamíferos, en los cuales un virus es totalmente responsable del
desarrollo de una capa protectora que está embebida en la placenta; esa
capa es esencial para evitar que el cuerpo de la madre identifique al
feto como un intruso y que trate de eliminarlo.

Los virus también pueden otorgar a sus huéspedes una mayor capacidad
competitiva en sus hábitats o hacer frente a sus enemigos. Por ejemplo,
los virus que acompañan a algunas bacterias y levaduras pueden eliminar
por infección o debilitar mediante la liberación de toxinas a los
organismos que podrían competir o amenazar a sus compañeros. Organismos
macroscópicos como las plantas invasoras llevan consigo virus que
debilitan a las especies nativas; y en la historia de los humanos hay
distintos episodios marcados por invasiones que fueron más fáciles
después del contagio de enfermedades virales como la influenza, el
sarampión y la viruela. En los últimos años, se ha
comenzado a utilizar virus que atacan bacterias resistentes a
antibióticos y virus que atacan a otros virus, como el de la hepatitis G
que suprime las superinfecciones con +++VIH+++. En otros casos, los virus
confieren a sus hospederos una mayor resistencia a los cambios en el
ambiente mediante la transferencia de genes que les permiten a sus
huéspedes una rápida adaptación y el desarrollo de nuevas funciones,
como aquellas que los dotan de una mayor tolerancia a las sequías y al
calor. Obviamente, ya estamos tratando de aprovechar al máximo esta
capacidad en el cultivo de tomates y otros alimentos en ambientes
extremos.

Son este tipo de relaciones entre virus y sus huéspedes, junto con otras
asociadas al desarrollo de las estrategias de defensa contra virus, por
las que se considera que éstos tuvieron un papel muy importante en la
evolución de la vida en la Tierra y que constituyen un factor que
influyó notablemente en el cambio evolutivo de los organismos con los
que esas entidades interactúan. De modo que, si descartamos a los virus
cuando intentamos reconstruir el árbol de la vida, nos quedaremos con
algunos huecos y ramas incompletas porque falta la información que ellos
mismos aportan.

Todas estas interacciones entre los virus y los tres dominios de la vida
se han ido macerando poco a poco a lo largo de cientos de millones de
años y han formado redes tan intricadas y complejas que no estamos ni
cerca de terminar de entenderlas. Sin embargo, podemos empezar por
aceptar que los virus sí son entidades biológicas, un poco diferentes
del resto, pero partículas vivas a fin de cuentas. Hay quienes incluso
sugieren que, como primer paso, tendríamos que asumir que los virus no
son una rama del árbol de la vida, sino que en realidad ese árbol
entero, desde sus raíces, vive y crece inmerso en un océano viral.

</section>
<section epub:type="chapter" role="doc-chapter">

# Viral ocean

# 19. Viral ocean @ignore

## By [Cisteil X. Pérez](997b-semblances.xhtml#a39) \
Translated by [Valeria Becerril](997b-semblances.xhtml#a54) {.centrado}

_An inefficient virus kills its host. A clever virus stays with it_.\
James Lovelock {.epigrafe}

It seems strange, that while being living beings, we cannot easily
differentiate between what’s alive and what’s inert material. But so it
happens, for decades, that the debate about the status between
“living/nonliving” of viruses and their non-classification as
“organisms” has remained an open one. What happens is that viruses are,
let’s say, pretty special. For example, they are considered as
“nonliving” because they have phases in which they are completely inert;
they are minuscule ---nearly 50 times smaller than many bacteria--- plus
they are not able to reproduce on their own, they require a host cell to
be able to multiply and inherit their genes. On the other side, whether
they reproduced, they have the capacity to produce their own genes and
to evolve, which marks them as living entities. These characteristics
are what continue to challenge the typical concepts that we use to
define the organisms and what keeps the debate open about the concept of
_life_ itself.

Recently, new findings seem to have tilted the scales in favor of the
living status of the viruses: it was discovered that the viral particles
in the natural ecosystem are far more diverse and abundant than it was
first thought, maybe even more than cells; it was also determined that
the virus that infect species of the main three branches that conform
the tree of life ---domains Bacteria (true bacteria), Eukarya
(protists, algae, plants, animals and fungi) and Archaea (similar to
bacteria, but extremophiles)--- have a close evolutionary relationship
among them, established thousands of millions years ago; and also giant
viruses have been detected, in other word, with size and genetic content
comparable to many bacteria cells. Then, in case of being living
entities, in which part of the tree of life would we have to add
viruses?

One of the main issues to solving this problem is that when we add the
different groups of viruses to the tree of life, these are scattered
among the branches that derive from the three domains, since there are
many shared genes between the viruses and cellular organisms. This is
partially explained through the way in which the viruses are multiplied:
they infect a cell, kidnap their genetic machinery and reprogram it to
make copies of their genome; in the process, they can steal genes of
their host cell and mixed them with their own in order to acquire a
greater cell infection capacity that their progeny will inherit. This
strategy is so successful that their hosts have developed many defense
tactics against viruses and, in many cases, it has been better to
stablish a mutualistic relationship with the virus rather than fighting
it.

Some of these relationships have been intimately linked during such a
lengthy period of time that the genetic material of the participant is
too intertwined and many mutualistic relationship viruses are essential
for the survival of the hosts. Such is the case of the interaction that
happens between viruses and parasitic wasps of the Brachonidae and
Ichneumonidae families. The parasitic wasps lay their eggs inside their
hosts (caterpillar, spiders, aphids, among others) so when the larvae
emerge, they are able to feed little by little of their host, while it
is still alive, until reaching the proper maturity to go outside,
reminiscing of ALIEN. However, the small detail is that this could not
happen without the aid of the virus that accompanies the eggs when their
mother inserts them. Since without it, the eggs would interpret their
new environment as a threat and would stop its development (Now that I
think about it, would it be possible that the xenomorph carried a virus
to infect their hosts?). A similar situation has occurred for millions
of years between viruses and mammals, in which a virus is totally
responsible for the development of a protective barrier that is embedded
in the placenta; that barrier is essential to avoid the body of the
mother identifying the fetus as an intruder and attempting to eliminate
it.

Viruses can also proportionate the host with a greater competitive
capacity in their habitats or to face their enemies. For example, the
virus that accompany some bacteria and yeasts are able to eliminate the
organisms that could compete or threat their teammates, via infection or
by weakening them through the liberation of toxins. Macroscopic
organisms such as invasive plants carry with them a virus that weakens
native species; and in the history of humanity there are different
episodes marked for invasions that were easier after the infection with
viral diseases such as influenza, measles and chickenpox. In the last
few years, viruses that attack bacteria resistant to antibiotics and
viruses that attack other viruses, such as the hepatitis G virus that
suppress the superinfection with +++HIV+++, has begun to be used. In other
cases, viruses confer their guests a greater resistance to changes in
the environment through the transferring of genes that allows their
hosts a quick adaptation and the development of new functions, such as
the ones that give them a greater tolerance to droughts and heat.
Obviously, we are already trying to exploit this capacity to the fullest
extent on the harvest of tomatoes and other foods in extreme
environments.

This kind of relationships between viruses and their hosts, along with
other associated to the development of defense strategies against
viruses, why it is considered that these had an important role in the
evolution of life on Earth and that they constituted a factor that
notably influenced the evolutionary changes of the organisms with which
these entities interact. So, if we discarded viruses when we tried to
rebuild the tree of life, we would be left with some gaps and incomplete
branches due to the lack of information that they themselves provide.

All these interactions between viruses and the three domains of life
have merged little by little through hundreds of millions of years and
have created networks so intricate and complex that we are not even
close to fully understand them. However, we can start by accepting that
the viruses are not a branch of the tree of life, they actually are the
whole tree, it lives and grows, from its roots, immersed in a viral
ocean.

</section>
<section epub:type="chapter" role="doc-chapter">

# Extinción Artificial

# 20. Extinción Artificial @ignore

## [Smok](997a-semblanzas.xhtml#a36) {.centrado}

<script type="text/javascript" src="../js/poetry.js"></script>

No me culpes por esto, {.poesia}

en tus aguas negras me he limpiado las manos. {.poesia}

Es cierto, los ahorqué mientras dormían, {.poesia}

los dejé sin cielo, sin sustento, {.poesia}

permití que los abrazara el fuego. {.poesia}

Pero también fingí no verlos al pasar a su lado, {.poesia}

cuando se escondían de mis cenizas, {.poesia}

de mi beso eterno. {.poesia}

Y solo los fuertes crearon su propio camino. {.poesia}

Con lentitud, cambiando de caras, {.poesia}

de plumas, de escamas. {.poesia}

Entonces abriste lo ojos, {.poesia}

inventaste la piedra, {.poesia}

probaste la sangre. {.poesia}

Me robaste la hoz {.poesia}

y rompiste el reloj de arena. {.poesia}

Tenías más prisa que la evolución, {.poesia}

tenías más hambre que el devorador. {.poesia}

Adelantaste el día del juicio para los inocentes, {.poesia}

a los que no tenían voz para defenderse. {.poesia}

Así entraron a lista de los que se perdieron, {.poesia}

preparaste con orgullo su pedestal en los museos. {.poesia}

Pero no te lamentes, cuando no puedas ver tu {.poesia}

belleza reflejada en las aguas. {.poesia}

Usa esas lágrimas, {.poesia}

para que la tierra florezca. {.poesia}

Puedes ser el juez, pero no la víctima. {.poesia}

Si quieres jugar a ser dios, {.poesia}

tienes que aprender a dar vida. {.poesia}

</section>
<section epub:type="chapter" role="doc-chapter">

# Artificial Extinction

# 20. Artificial Extinction @ignore

## By [Smok](997b-semblances.xhtml#a36) {.centrado}

<script type="text/javascript" src="../js/poetry.js"></script>

Don’t blame me for this, {.poesia}

in your black waters I have washed my hands. {.poesia}

It’s true, I hung them while they were sleeping, {.poesia}

I left them without heaven, without sustenance, {.poesia}

I allowed the fire to embrace them. {.poesia}

But I also pretended not to see them when I passed by, {.poesia}

when they were hiding from my ashes, {.poesia}

from my eternal kiss. {.poesia}

And only the strong ones created their own path. {.poesia}

Slowly, changing faces, {.poesia}

feathers and scales. {.poesia}

Then you opened your eyes, {.poesia}

you invented the stone, {.poesia}

you tested the blood. {.poesia}

You stole my sickle {.poesia}

and you broke the clock. {.poesia}

You were faster than evolution, {.poesia}

you were hungrier than the devourer. {.poesia}

You advanced the day of judgment for the innocent, {.poesia}

for those who had no voice to defend themselves. {.poesia}

So they entered the list of the lost ones, {.poesia}

and you prepared their pedestal with pride in the museums. {.poesia}

But don’t be sorry when you no longer see your {.poesia}

beauty reflected in the waters. {.poesia}

Use those tears for the earth to flourish. {.poesia}

You can be the judge, {.poesia}

but not the victim. {.poesia}

If you want to play God, {.poesia}

You have to learn to give life. {.poesia}

</section>
<section epub:type="chapter" role="doc-chapter">

# Nunca subestimes a una mujer dormida

# 21. Nunca subestimes a una mujer dormida @ignore

## [Gabriela Damián Miravete](997a-semblanzas.xhtml#a16) {.centrado}

La población estaba preparada para la erupción del volcán Popocatépetl,
el guerrero, que llevaba más de cien años adornado por su columna de
vapor, exhalando su mal humor por lo bajo; se hacía el importante con la
ocasional pirotecnia, que no eran sino malabares en el aire con unas
cuantas rocas fundidas y un flujo de lava más bien ralito, expulsado con
el placentero propósito de asustar. Y funcionaba: asustaba a la gente de
cerca y de lejos.

Los preparativos llevaban décadas: las viviendas a las faldas del volcán
ya eran cáscaras vacías de fruta, los pueblos se mudaron creando una
nueva periferia, como las cuentas de un collar. El parque nacional hizo
un plan de contingencia para la fauna; estudiaron adónde volarían los
búhos, colibríes, pájaros carpinteros y zopilotes, por qué rutas huirían
los cacomixtles y tlacuaches y mapaches y linces y coyotes y armadillos
y venaditos, a qué ramas o tallos se aferrarían las vanessas y a dónde
querrían treparse los lagartos y esconderse las culebras y cascabeles.
No podíamos arriesgarnos a perderlos. Se construyeron aquí y allá
sofisticadas madrigueras, sótanos-estuche, refugios pequeños, postes
altísimos de iridio desde donde observarían, como el resto de la gente,
el paso del fuego y el aliento temible de los lahares.

Estábamos listos. Después de terremotos zarandeadores, fumarolas
encanijadas y magma escupido, la gente se guardó en sus nuevas cajitas
antisísmicas, y esperó, mirando desde sus pantallas y azoteas.

Pero nadie había pensado en que el volcán Iztaccíhuatl, la Mujer Dormida
que velaba el guerrero, pudiera despertar. Sus chimeneas se habían ido
llenando de magma, poco a poco. Tanto esperaban del Popo que la habían
ignorado a ella. Iztaccíhuatl madrugó: aún brillaban las estrellas
cuando la nieve se hizo a un lado como si, con una patada, hubiera hecho
a un lado las sábanas para descubrirse los pies. Las explosiones
refulgieron sobre el cielo púrpura, y los ríos de viscosa lava
rojanaranja reptaron despacio por las laderas cual dragones de puro
fuego. Las nubes de ceniza tejieron telarañas de relámpagos y
estruendos.

Por alguna razón, nuestro miedo cambió. Vimos la erupción no como un
infierno, sino como un milagro futuro. Iztaccíhuatl no quería asustar a
nadie, simplemente necesitaba crear nueva tierra. Y como todas las
mujeres que despiertan, transformó nuestro paisaje.

</section>
<section epub:type="chapter" role="doc-chapter">

# Never Underestimate a Sleeping Woman

# 21. Never Underestimate a Sleeping Woman @ignore

## By [Gabriela Damián Miravete](997b-semblances.xhtml#a16) \
Translated by [Toshiya Kamei](997b-semblances.xhtml#a53) {.centrado}

The village was ready for the eruption of the Popocatépetl volcano, the
warrior, which had been adorned for more than one hundred years with its
steam plume, exhaling its bad humor under its breath; it made a
statement with occasional pyrotechnics, juggling in the air a few molten
rocks and rather thin spurts of lava, expelled with the pleasant purpose
of frightening. And it worked: it scared people from near and far.

Preparations took decades: the houses along the slopes of the volcano
became empty fruit peels, and the towns moved to create a new periphery,
like beads on a necklace. The national park made a contingency plan for
the fauna; they studied where owls, hummingbirds, woodpeckers and
vultures would fly, which routes cacomistles, opossums, raccoons,
lynxes, coyotes, armadillos, and fawns would flee to, what branches or
stems painted lady butterflies would cling to, where lizards would
climb, and where serpents and rattlesnakes would hide. We couldn’t
afford to lose them. Here and there sophisticated burrows,
case-basements, small refuges, and high iridium posts were built so that
they could observe, like everyone else, the passage of fire, and the
fearsome breath of lahars.

We were ready. After shaking the ground, belching volcanic gas, and
spitting magma, the volcano held its breath, and people took shelter in
their new anti-seismic little sheds and waited, watching from their
screens and rooftops.

But no one had thought Iztaccíhuatl volcano, the Sleeping Woman watched
by the warrior, could wake up. Its chimneys had been filling of magma
little by little. So much was expected of the warrior that she has been
forgotten. Iztaccíhuatl got up early: the stars were still shining when
the snow was cast aside as if, with a kick, the sheets were thrown to
reveal her feet. The explosions gleamed on the purple sky, and the
rivers of viscous red-orange lava crawled down the slopes like dragons
of pure fire. Volcanic ash clouds wove spidery cobwebs of lightning and
thunder.

For some reason, our fear changed. We saw the eruption not as hell, but
as a future miracle. Iztaccíhuatl didn’t want to scare anyone. She
simply needed to create new land. And like all women who wake up, she
transformed the landscape.

</section>
<section epub:type="chapter" role="doc-chapter">

# Quitzacuia Ahuácatl (El último aguacate)

# 22. Quitzacuia Ahuácatl (El último aguacate) @ignore

## [Rick Canfield](997a-semblanzas.xhtml#a35) {.centrado}

Trillones de estrellas inundan la atmósfera, nacen a miles de millones
de años de distancia por los restos de una supernova explosiva que
alcanza su cenit cósmico. La tormenta cuántica se acerca.

Sin saberlo, envejecido por su antimateria, Jaime Rodríguez se sienta
entre las sombras fantasmales de las torres que se proyectan en Tribeca
sobre una cafetería que mira hacia el río Hudson, que se acerca.

Alguna vez el paisaje sonoro de dos millones de historias y emociones
sucediendo a la vez, Manhattan es ahora un lugar solemne.

Mientras la lluvia golpea la fachada, un camarero prepara su última
comida, mientras los pensamientos de Jaime están en otra parte, añorando
productos frescos.

A pesar de su rústico aspecto español, bebe sorbos de chocolate caliente
importado como una oda a los reales ancestros nahuas, ya que sus padres
son agricultores migrantes; el padre de México y la madre de Filipinas,
que se reunieron en el calor de los campos de California.

El concreto en mal estado separa el alma vieja de las capas de suelo
empañado, cedido hace medio siglo a la Compañía de las Indias
Occidentales, la primera de muchas compañías que traicionaron acuerdos
de paz mediante la masacre de personas de la tribu Lenape.

Surgió Nueva Ámsterdam, separando a los habitantes de Manhattan
originales con una palizada ahora conocida como Wall Street,
convirtiéndose en el principal mercado de la trata de esclavos humanos
africanos y amerindios.

De manera similar, sus colegas acumularon vastas fortunas ---del trabajo
de otros--- en bóvedas digitales, ahora inútilmente selladas sin
electricidad ni internet. Sus inversiones fueron ecológicas.

Un viejo amor, una bella genetista brasileña llamada Carolina, se une a
él como un bálsamo; trae su propio brillo al café iluminado por la luz
de las velas.

Ella quería casarse. Si combinaran su herencia genética, serían la
culminación de mil tribus de la Tierra, muchas ya olvidadas como
estrellas moribundas.

Se escuchan las olas rompiendo sobre Broadway. Él contempla ciertos
errores que frenan la marea, pero nada más importa, excepto este
momento.

Los socios dividen los gastos por su inestimable reliquia tomada por
corsarios, mercenarios contratados por antiguos rivales.

Finalmente, el momento llega, mirándola a los ojos con tristeza, Jaime
entrega las llaves de su reino junto con él mismo.

Un reflejo de los agujeros negros en colisión y su danza eterna, lo
llaman _Quitzacuia Ahuácatl_, el hijo y heredero de su colonia de
estrellas regresa.

</section>
<section epub:type="chapter" role="doc-chapter">

# Quitzacuia Ahuácatl (The Last Avocado)

# 22. Quitzacuia Ahuácatl (The Last Avocado) @ignore

## By [Rick Canfield](997b-semblances.xhtml#a35) {.centrado}

Trillions of stars awash the atmosphere, birthed billions of years away
by the remnants of an explosive supernova reaching its cosmic zenith.
The quantum storm approaches.

Unknowingly aged by its antimatter, Jaime Rodriguez sits amongst the
ghostly shadows of towers above a Tribeca cafe; he looks out over the
encroaching Hudson River.

Once the soundscape of two million stories and emotions happening all at
once, Manhattan is now a solemn place.

As rain taps the facade a waiter prepares his last meal, while Jaime’s
thoughts are elsewhere, nostalgic for fresh produce.

Despite his rustic Spanish looks, he sips on imported hot chocolate as
an ode to royal Nahua ancestors, his parents being migrant farmers; a
father from Mexico and mother of the Philippines whom met in the heat of
California’s fields.

Dilapidated concrete separates the old soul from layers of tarnished
soil, relinquished half a century ago to the West India Company, the
first of many companies who betrayed peace deals by massacring people of
the Lenape tribe.

New Amsterdam sprang forth, separating the original Manhattanites by a
pallisade known now as Wall Street, becoming the primary market of the
African and Amerindian human slave trade.

His colleagues similarly hoarded their vast fortunes off the labor of
others into digital vaults, now meaninglessly sealed without electricity
nor the internet. His investments were ecological.

An old Brazilian flame, a beautiful geneticist named Carolina joins him
in solace, bringing her own shine into the cafe illuminated by
candlelight.

She wanted marriage. Between their genetics, they are the culmination of
a thousand tribes of the Earth, many forgotten like dying stars.

Waves can be heard crashing down Broadway. He contemplates misdeeds
holding back the tide, yet nothing else matters but this moment.

The partners "go Dutch" for their priceless relic taken by privateers,
mercenaries hired by old rivals.

Finally the moment arrives, looking into her eyes drearily, Jaime hands
over the keys to his kingdom along with himself.

A reflection of colliding black holes and their eternal dance, they call
him _Quitzacuia Ahuácatl_, the son and heir to their star colony is
returned.

</section>
<section epub:type="chapter" role="doc-chapter">

![](../img/gonzalo-alvarez_skyfell-cielocayo01.jpg)

![](../img/gonzalo-alvarez_skyfell-cielocayo02.jpg)

# _Cuando se cayó el cielo_ de [Gonzalo Álvarez](997a-semblanzas.xhtml#a47) {.centrado}

El tema que dio lugar a esta secuencia fue el Cretáceo-Paleógeno, particularmente la extinción. Un artículo que leí mencionaba que los dinosaurios podrían haberse extinguido no por un meteorito, sino por una erupción volcánica masiva, 250 000 años antes de la fecha que fija la teoría actual del meteorito. Las trampas Deccan son depósitos de lava enfriada del tamaño de una montaña en la India, y algunos creen fue la causa de la extinción, por lo que quise crear una imagen basada en esa teoría. Además, cuando estaba haciendo el trabajo, traté de ponerme en el lugar de esas criaturas que vivían en este planeta mucho antes que yo y pensar en cómo debieron experimentar este evento. Seguramente miedo y dolor fue lo que sintieron y traté de ilustrar esa angustia en la segunda imagen, mientras que la primera imagen se enfoca más en establecer el evento.

# [Gonzalo Álvarez](997b-semblances.xhtml#a47), _When the Sky Fell_ {.centrado}

# 23. Cuando se cayó el cielo / When the Sky Fell @ignore

The theme that moved me to this sequence was the Cretaceous-Paleogene and particularly the extinction. An article I read mentioned that the dinosaurs could have in fact been killed not by a meteor, but by a massive volcanic eruption 250,000 years before the current theory of the meteor. The Deccan Traps are mountain-sized cooled lava in India that is said to have been what caused the extinction, and so I wanted to create an image based on that theory. Also as I was creating the work I tried to put myself in the shoes of these creatures that lived on this planet way before me and think of how they must of experienced this event. Surely fear and pain was something they felt and I tried to illustrate that with the second image while having the first image focusing more on establishing the event. 

</section>
<section epub:type="chapter" role="doc-chapter">

# Remembranzas

# 24. Remembranzas @ignore

## [Ana Delia Carrillo](997a-semblanzas.xhtml#a04) {.centrado}

Cuando ella cierra los ojos, aún puede recordar el cielo pintado de
ocres, naranjas y morados de los ocasos. Aún recuerda el olor fresco de
la tierra mojada después de la lluvia. Y los bosques, y la playa, y su
pequeño huerto al fondo del jardín. Pero no habla de eso. Nadie lo hace.
Ellos lo sabían, y se prepararon justo para ese día. Los tildaron de
locos, de fanáticos, y los ridiculizaron hasta el cansancio. Familiares
y amigos se negaron a evacuar a pesar de que agencias como la NASA
anunciaban la inminente catástrofe. Exageraciones, dijeron. Ustedes y
sus cuentos de destrucción, proclamaron. Su pequeño grupo de
sobrevivientes, poco más de una docena de personas, se resguardó en el
refugio antibombas. ¿De qué les sirve ahora jactarse de que tuvieron
razón? De qué, si el meteorito, aún siendo nueve veces más pequeño que
el de Chicxulub, acabó con la agricultura mundial, ocasionó el incendio
de la mayoría de los bosques del planeta y los tsunamis que devastaron
gran parte de las costas, causando la extinción de miles de especies, y
la muerte de millones de seres humanos. Ella no sabe qué le depara el
futuro. No sabe si hay futuro posible. Solo sabe que si cierra los ojos,
aún recuerda las puestas de sol, el olor a tierra mojada, y su huerto al
fondo del jardín.

</section>
<section epub:type="chapter" role="doc-chapter">

# Remembrances

# 24. Remembrances @ignore

## By [Ana Delia Carrillo](997b-semblances.xhtml#a04) {.centrado}

When she closes her eyes, she can still remember the sunsets with their
ocre, orange and purple painted skies. She still remembers the smell of
wet soil after the rain. And the forest, and the beach, and her small
orchard at the back of her garden. But she doesn’t talk about it. Nobody
does. They knew, and prepared themselves for the day. They were called
crazy, fanatics, and were ridiculed beyond belief. Family and friends
refused to evacuate, even though agencies like NASA announced the
imminent catastrophe. Exaggerations, they said. You and your stories of
destruction, they laughed. Her small group of survivalists, a little
more than a dozen people, took refuge in the shelter. What’s the point
in bragging they were right? What for, if the meteorite, nine times
smaller than Chicxulub, ended world agriculture, set on fire the
planet’s forests and created giant tsunamis that tore up the coastlines,
causing the extinction of thousands of species and the death of millions
of humans. She doesn’t know what the future holds for her. She doesn’t
even know if there’s a future. She only knows that if she closes her
eyes, she can still remember the sunsets, the smell of wet soil, and her
orchard at the back of her garden.

</section>
<section epub:type="chapter" role="doc-chapter">

# El Hegelosaurio

# 25. El Hegelosaurio @ignore

## [Yuri Herrera](997a-semblanzas.xhtml#a37) {.centrado}

Esto que usted ve es la prueba, única pero incontrovertible, de la
racionalidad dinosáurica y, por desgracia, también de sus últimos
momentos sobre la tierra. Al contrario de lo que se ha repetido desde
siempre, los dinosaurios eran sensibles, juiciosos, y reflexionaban. Lo
cual no debería sorprendernos, considerando que no tuvieron mucho qué
hacer durante millones de años. Nomás no tenían prisa. Aunque deberían
haberla tenido, porque para cuando llegó la gran extinción los pobres ya
eran demasiado grandes y la comida de la que disponían ya no abundaba y
poco a poco habían comenzado a declinar. Bueno, pues había dos maneras
en las que estos animalitos procesaban sus ideas: una, dejando que el
mundo les interesara el cuerpo poco a poco, que la lluvia les macerara
la piel gruesa y rugosa, que el viento les indicara algo sobre las
constantes de la naturaleza, que los bichos más pequeños anidaran en su
epidermis. Así iban entendiendo cosas. No como las enseñamos ahora, en
párrafos, sino como una especie de fluido entre su organismo y los otros
organismos. La segunda era, cómo decir, su ciencia dura, la trituración
del mundo hasta hacerlo digerible, es decir, masticando. Los dientes
eran los engranes de su racionalidad. Gracias a estos dos mecanismos los
dinosaurios desarrollaron una filosofía que probablemente hubieran
llegado a poner por escrito de no ser por el meteorito que terminó de
destruir lo que los volcanes y la evolución ya habían comenzado. Pero no
lo hicieron… ¿O lo hicieron?… _Lo hicieron_. En particular un
herbívoro. Recientes investigaciones han mostrado que esto que ve es un
vestigio de lo que uno de los herbívoros, a quien he llamado
Hegelosaurio (disculpe, no pude resistirme), emprendió cuando se dio
cuenta de que esa luz que crecía en el cielo era la catástrofe. No puedo
reconstruir la cosmovisión entera que este individuo plasmó en esas
¿horas? que tuvo antes de que todo se fuera al carajo; es como si
contáramos solo con una página de la _Fenomenología del espíritu_. Pero
sí le puedo decir que es una página hermosa, que la masticación ahí
eternizada plasmó una manera de entender el paso del tiempo y el temor
ante el cataclismo. Sigo descifrándola, pero ya puede verse ahí, vea,
ahí y ahí, esos no son accidentes, son matices. Qué. No. No. No. Se
equivoca. Esto no es una piedra.

</section>
<section epub:type="chapter" role="doc-chapter">

# Hegelosaur

# 25. Hegelosaur @ignore

## By [Yuri Herrera](997b-semblances.xhtml#a37) \
Translated by [Toshiya Kamei](997b-semblances.xhtml#a53) {.centrado}

What you see here is the unique but incontrovertible evidence of
dinosaurs’ rationality and, unfortunately, also of their last moments on
Earth. Contrary to what has been repeated ever since, dinosaurs were
sensitive, sensible, and _thoughtful_. That shouldn’t surprise us,
considering they had nothing much to do for millions of years. They were
never in a hurry. They should have been, however, because, by the time
the great extinction occurred, the poor creatures were already too big,
food had become scarce, and they had begun to decline little by little.
There were two ways in which these animals processed their ideas. First,
they let the world concern their bodies little by little, the rain
soften their thick rough skin, the wind indicate something about
nature’s constants, and the tiniest bugs nest in their epidermis. That
was how they understood things. Not as we show them now, in paragraphs,
but as a sort of fluidity between them and other organisms. Secondly,
how should I put it? They employed hard science, crushing the world to
make it digestible, namely, chewing. Their teeth were their
rationality’s gears. Thanks to these two mechanisms, the dinosaurs
developed a philosophy they would have put in writing if it hadn’t been
for the asteroid that finished off what volcanoes and evolution had
started. But they didn’t get round to it. Or did they? _They did it_. An
herbivore in particular. Recent research has shown that what you see
here is a vestige of what one herbivore, which I’ve dubbed Hegelosaur
(sorry, I couldn’t resist), undertook when it realized the glaring light
in the sky was an impending catastrophe. I can’t reconstruct the entire
worldview this individual embodied in those hours before everything went
to hell; it’s as if we had only one page of _The Phenomenology of
Spirit_. But I can tell you it’s a beautiful page, that eternalized
chewing there embodied a way to understand the passage of time and the
fear of cataclysm. I still have to decipher it, but you can see it
there, see: there and there. Those aren’t accidents but nuances. What?
No. No. No. You’re wrong. This isn’t a stone.

</section>
<section epub:type="chapter" role="doc-chapter">

# Prospección lejana

# 26. Prospección lejana @ignore

## [Amílcar Amaya](997a-semblanzas.xhtml#a03) {.centrado}

Ese calor. Era una cosa horrorosa. Si hubiera una superficie lisa donde
echar una gota de agua, esta habría siseado como aceite en una sartén.
Pero no había ninguna superficie lisa y el agua era demasiado preciada
para desperdiciarla en un experimento fútil. La sombra del risco a su
espalda se achicaba con la llegada del medio día. El paleontólogo no
estaba tan desamparado como podría parecer, solo que el sol inclemente
era de las pocas cosas que no le gustaban de su trabajo.

Lo que realmente le interesaba dormía bajo sus pies, lejos de las raíces
de las biznagas y las choyas, de las yucas y los madroños. Capas y capas
de estratos que entre sus páginas de sílice ---porque sí, desde la
perspectiva adecuada esas láminas sin edad parecerían un libro
gigantesco--- conservaban las huellas de la vida en la Tierra en forma
de huesos, huellas, impresiones… Como separadores de
lectura improvisados.

Esa zona en particular era rica en afloramientos cretácicos. Es decir,
una ventana que permitía asomarse al mundo como era por lo menos 65
millones de años antes. Una época extraña en la que había más oxígeno en
la atmósfera, animales que podían alcanzar tallas gigantescas y
temperaturas globales en promedio más altas que en la actualidad. El
calor: esa chispa de vida.

Si el principio de superposición no sufría ningún requiebro, entre más
profundo estaban los restos prensados, más antiguos eran. Más abajo de
los dinosaurios y las primeras flores habría reptiles con velas en la
espalda, peces que aprendían a caminar y minúsculos gusanos con cierta
fuerza interior. Y si se bajaba lo suficiente, muchos, muchos
kilómetros, la consistencia misma de la tierra se transformaría del
confiable sólido sobre el cual el paleontólogo apoyaba sus botas, en un
no tan confiable líquido hirviente donde todo sería efímero.

Pensando en esos calores plutónicos, al paleontólogo ya no parecían
molestarle los cuarenta grados de la superficie. Podía seguir trabajando
un poco más.

</section>
<section epub:type="chapter" role="doc-chapter">

# Distant Prospecting

# 26. Distant Prospecting @ignore

## By [Amílcar Amaya](997b-semblances.xhtml#a03) \
Translated by [Toshiya Kamei](997b-semblances.xhtml#a53) {.centrado}

That heat. It was horrible. If there were a smooth surface where a drop
of water would fall, it would have hissed like oil in a pan. But there
was no smooth surface and water was too precious to be wasted in a
futile experiment. The shadow of the cliff behind him was shrinking with
the arrival of noon. The paleontologist wasn’t as helpless as he might
seem. The harsh sun was one of the few things he didn’t like about his
job.

What really interested him was asleep under his feet, away from the
roots of biznagas, choyas, yuccas, and strawberry trees. Multiple layers
of sediment between their pages of silica---because, yes, viewed from
a proper perspective, those ageless plates would seem like a gigantic
book---kept the traces of life on Earth in the form of bones, prints,
impressions. As makeshift bookmarks.

That particular area was rich in Cretaceous outcrops that is, a
window that allowed the world to be seen as it was at least sixty-five
million years earlier. A strange time when there was more oxygen in the
atmosphere, animals that could reach gigantic sizes and average global
temperatures higher than at present. The heat was that spark of life.

If the superposition principle didn’t suffer from any break, the deeper
the pressed remains were, the older they were. Below the dinosaurs and
the first flowers there would be reptiles with candles on their backs,
fish that learned to walk, and tiny worms with some inner strength. And
if it went down enough, many, many kilometers, the very consistency of
the earth would be transformed from the reliable solid on which the
paleontologist supported his boots into a not-so-reliable boiling liquid
where everything would be ephemeral.

Thinking about those plutonic heats, the paleontologist was no longer
bothered by forty degrees on the surface. He could keep working a little
longer.

</section>
<section epub:type="chapter" role="doc-chapter">

# La caída

# 27. La caída @ignore

## [David Bowles](997a-semblanzas.xhtml#a12) {.centrado}

Qhirtlahik observó cómo la estrella se desplomaba hacia su aldea.

El sacerdote había explicado que las estrellas eran los Guardianes de la
Oscuridad, donde la Madre Sol ocultaba su rostro cada noche, avergonzada
ante los pecados de la Gente. Las visiones divinas proclamaban que, de
entre todos sus hijos emplumados, solo a la Gente le había dado alma.

Pero la gente era ingrata.

Qhirtlahik dudaba. La duda era su pecado. Observaba los cielos de noche,
marcando con sus garras los caminos de las estrellas en una amplia
sección de corteza.

Era una danza. Más regular que las mareas, que el pasar de las
estaciones, que el ir y venir de la lluvia y el calor y el frío.

Hace una semana, había notado una nueva luz, ensanchándose más cada
noche, desafiando el compás de la danza.

Ahora, incluso cuando la Madre Sol ensangrentaba el horizonte con sus
garras de amanecer, la estrella no se desvanecía. Era casi del tamaño de
la Hermana Luna.

Qhirtlahik juraba ver llamas en aquel rostro.

Mirando colina abajo hacia su aldea, sintió que sus plumas se agitaban
de miedo. Su lengua palpó el aire, un impulso nervioso.

Como uno de los Conocedores, Qhirtlahik había viajado lejos del conjunto
de pueblos.

Ningún otro ser pensante, ningún otro hablante parecía existir más allá
de esta pequeña península que sobresalía cual colmillo del Mar
Iridiscente.

“Tal vez ---pensó--- debería llevarme a mis amigos y huir.

”Esta estrella nos puede consumir a todos. No quedaría Gente alguna, ni
señal de que hayamos existido. Nuestras casas de bajareque destrozadas.
Reducidas a cenizas las armas de madera y herramientas de hueso. Las
muescas que hacemos en corteza mojada se borrarían para siempre.

”¿Y más allá? ¿Qué sobreviviría? ¿Aquellos monstruos pesados ​​cuyo
plumaje se mofa del nuestro? ¿Los corredores, desesperados por volar?
¿Los pequeños cavadores, tímidos, de sangre caliente?”.

Madre Sol levantó su rostro hacia el cielo.

La estrella voraz la deslumbraba.

</section>
<section epub:type="chapter" role="doc-chapter">

# Starfall

# 27. Starfall @ignore

## By [David Bowles](997b-semblances.xhtml#a12) {.centrado}

Qhirtlahik watched the star stream toward his village.

The priest claimed stars were the Guardians of the Dark, where Mother
Sun hid her face in shame for the People’s sins each night. Alone of all
her feathered children she had given them souls, yet they were
ungrateful. So the divine visions proclaimed.

Qhirtlahik doubted. It was his sin. He watched the skies at night,
scratching the stars’ paths into a wide sheet of bark with his talon.

They did a dance. More regular than the tides, than the passing of the
seasons, than the coming and going of rain and heat and cool.

A week ago, he had noticed a new one, growing larger each night, defying
the dance.

Now, even with the dawn claws of Mother Sun bloodying the horizon, the
star would not fade. It was nearly as big as Sister Moon.

Qhirtlahik could have sworn he saw flames flickering across its face.

Looking down the hill at his village, he felt his feathers ripple with
fear. His tongue flicked at the air, a nervous impulse.

As one of the Knowers, Qhirtlahik had traveled far from the People’s
cluster of villages.

No other thinkers, no other speakers existed beyond their little spar of
land, jutting into the Iridescent Sea.

_Perhaps_ (he thought) _I should take my friends and flee_.

This star might consume us all. There would be no People left. No sign
we were ever here. Our wattle and daub homes shattered. Reduced to ash
our wooden weapons, bone tools. The sigils we score into flexible bark
effaced forever.

And beyond? What survives? The lumbering monsters whose feathers mimic
our own? The smaller ones, desperate to fly? The burrowers, warm-blooded
and timid?

Mother Sun thrust her face into the sky.

The falling star outshone her.

</section>
<section epub:type="chapter" role="doc-chapter">

![](../img/emmanuel-valtierra_dinos-murierondied.jpg)

# _El día que los dinosaurios murieron_ de [Emmanuel Valtierra](997a-semblanzas.xhtml#a46) {.centrado}

Los dos personajes principales hacen alusión a los dioses Miclanteuctli y Quetzalcoatl en el _Códice Borgia_, siendo representados aquí por dos dinosaurios, uno vivo y uno muerto. Esto crea una separación entre el antes y después de que el meteorito se estrellara y la actividad volcánica acabara con ellos. Estos dos elementos se pueden ver en el fondo de la ilustración.

# [Emmanuel Valtierra](997b-semblances.xhtml#a46), _The day the dinosaurs died_ {.centrado}

# 28. El día que los dinosaurios murieron / The day the dinosaurs died @ignore

The two main characters of the image allude to the gods Miclanteuctli and Quetzalcoatl in the _Borgia Codex_, represented here by two dinosaurs, one alive and one dead. This symbolism creates a separation between the time before and after the meteorite crashed and volcanic activity extinguished them. These two events are depicted in the background of the illustration.

</section>
<section epub:type="chapter" role="doc-chapter">

# Tiempos propicios

# 29. Tiempos propicios @ignore

## [Iliana Vargas](997a-semblanzas.xhtml#a20) {.centrado}

El tan temido fenómeno ocurrió en pleno día, sin señales o sucesos
previos, cuando ya nadie se acordaba de la época en la que se había
vuelto lugar común especular sobre el fin del mundo. Intensísimas ondas
solares desecaron en siete días cada uno de los oceanos, originando
tormentas de sal que herrumbraron poco a poco todo ser orgánico que
encontraron a su paso. En la mayoría de los continentes solo se salvaron
los organismos subterráneos y quienes llenaban el carrito de despensa en
algún supermercado construido varios pisos bajo tierra; sin embargo, la
comida duró solo algunos meses, y los sobrevivientes debieron
arriesgarse a explorar la superficie. Deambulaban hacia todas
direcciones entre inmensos terrenos irreconocibles que semejaban un
jardín de restos disecados. El cielo se había convertido en una capa de
destellos purpúreos, vacío de astros; solo el calor permanecía como
huella indeleble de la estrella que antes regía los días. Entonces
comenzaron a alimentarse de cualquier cosa masticable con que
tropezaban: cuerpos secos cuya anatomía empezaba a volverse un trozo
informe de carne. Fue cuando iniciaron la migración hacia el Sur,
creyendo que la Antártida, al descongelarse, había revelado la _Terra
Australis_ donde seguramente les esperaban los verdaderos dioses y un
portal hacia otra dimensión cósmica. Sin embargo, pese a sus esfuerzos y
a la pésima sobrevivencia que lograron desarrollar en comparación con
otras especies, solo alcanzaron a avanzar lo suficiente para descubrir
una mancha verde que, desde la tierra a la que anhelaban llegar, se
arremolinaba hacia ellos: hordas de listrosaurios que en efecto se
habían liberado de los mausoleos glaciales donde habían sido
criopreservados a la espera de tiempos más propicios para vivir
plenamente.

</section>
<section epub:type="chapter" role="doc-chapter">

# Auspicious Times

# 29. Auspicious Times @ignore

## By [Iliana Vargas](997b-semblances.xhtml#a20) \
Translated by [Ana Ximena Jiménez Nava](997b-semblances.xhtml#a52) {.centrado}

The long-feared phenomenon occurred in broad daylight, there were no
previous signs or events. Nobody recalled the moment when speculation
about the end of the world had become such commonplace. Heavy solar
waves dried up each of the oceans in seven days, creating storms of salt
that rusted, little by little, every organic being in their way. In most
of the continents, only the underground-living organisms, and those who
filled their shopping carts in a supermarket built many floors
underground, survived; however, the food only lasted a few months, and
the survivors risked to explore the surface. They wandered in every
direction through unknown big lands that looked like a garden full of
dried remains. The sky had become a dome of purple flashes empty of
stars; just the heat remained as a permanent mark of the one that once
ruled the days. Then, they started to feed on every edible thing that
they encountered: dried corpses whose anatomy was turning into shapeless
flesh. That was the time when they started their migration to the South,
believing that the Antarctic, unfrozen now, had revealed the _Terra
Australis_, the place where the true gods and the doorway to another
cosmic dimension awaited them. Despite their efforts and their terrible
survival skills (unequaled to those of other species) they got far
enough to discover a green spot that swirled towards them coming from
the land they longed to reach: hordes of _lystrosaurus_ that had been
liberated from their glacial tombs where they used to lie cryopreserved,
waiting for auspicious times to live fully.

</section>
<section epub:type="chapter" role="doc-chapter">

# Varias hipótesis para explicar la ausencia de restos de dinosaurios inmediatamente debajo de la capa de residuos geológicos conocida como Límite K/Pg, que se tiene como evidencia del impacto en la Tierra del asteroide de Chicxulub y la extinción masiva de formas de vida terrestre al final del periodo Cretácico, pero plantea aún este curioso enigma, que podría expresarse como la ausencia aparente de dinosaurios durante años antes del momento fatal, y entonces uno dice ¿dónde estaban los dinosaurios?, ¿qué estaban haciendo los dinosaurios?, ¿tenían algo mejor que hacer que esperar su fin entre llamas y sufrimientos espantosos?, porque yo no los culparía si lo hubiesen tenido, claro

# 30. Varias hipótesis para explicar la ausencia de restos de dinosaurios inmediatamente debajo de la capa de residuos geológicos conocida como Límite K/Pg, que se tiene como evidencia del impacto en la Tierra del asteroide de Chicxulub y la extinción masiva de formas de vida terrestre al final del periodo Cretácico, pero plantea aún este curioso enigma, que podría expresarse como la ausencia aparente de dinosaurios durante años antes del momento fatal, y entonces uno dice ¿dónde estaban los dinosaurios?, ¿qué estaban haciendo los dinosaurios?, ¿tenían algo mejor que hacer que esperar su fin entre llamas y sufrimientos espantosos?, porque yo no los culparía si lo hubiesen tenido, claro @ignore

## [Alberto Chimal](997a-semblanzas.xhtml#a02) {.centrado}

1. El impacto del asteroide ocultó la evidencia de otra extinción: la
que provocaron las misteriosas bacterias carnívoras a las que los
científicos del siglo +++XXVIII+++, si es que no nos extinguimos antes,
darán el nombre popular de _espirilos salvajes_. Siglos antes de que
llegara el asteroide, los espirilos eran ya una amenaza: no solo
mataban de fiebre a las criaturas de células eucariotas (en especial
a los dinosaurios), sino que se comían sus cuerpos, de los que no
quedaban ni los huesos. El asteroide mató a aquellos asesinos
invisibles, que se quemaron en las olas de fuego, y a casi la
totalidad de las criaturas que aún no se convertían en sus víctimas.
Después, mucho tiempo después, llegamos nosotros a excavar en la
tierra.
2. El impacto del asteroide ocultó la evidencia de otra extinción: la
que provocaron las criaturas diamantinas que ni en el siglo +++XXVIII+++
tendrán nombre (y si nos extinguimos antes, menos todavía). Anidaban
en el fondo de las más oscuras cavernas y de los abismos marinos,
pues preferían la oscuridad y las altas presiones. Vastas,
brillantes, de piel durísima y de fuerza descomunal, no más
inteligentes que los tiburones, salían volando hasta grandes
alturas, se dejaban caer y arrebataban por el aire a los
dinosaurios, pobres lagartos atribulados, para luego comérselos en
sus guaridas remotas. (Cuando cayó el asteroide en Chicxulub, el
estallido mató a casi todas las criaturas diamantinas, pero algunas
salieron expulsadas de la atmósfera terrestre ---sus cuerpos eran
enormemente, resistentes y además podían entrar en estado de
latencia, envolverse en un capullo hermético y resistir años sin
aire ni alimento--- y fueron a dar, tras un largo viaje, al interior
de la atmósfera de Júpiter, aún más oscuro y oprimente que su
antiguo hogar, donde son felices hasta hoy). {.espacio-arriba1}
3. El impacto del asteroide fue previsto por los videntes de la cultura
dinosáurica, de grandes cerebros y colmillos, y brazos pequeñitos
porque su ámbito no era el del mundo material (hoy los llamamos
tiranosaurios). Lo vieron venir y, como aquella comunidad de
especies inteligentes tenía mucho de precognición pero muy poco de
telequinesis ---o de herramientas de cualquier tipo para desviar
asteroides---, decidieron cometer suicidio colectivo. Y lo hicieron
en el que, para ellos, fue el día Flor de las Humildades del
Lenguaje del año 8 576 234 del Calendario Faminófeno, y las colinas
y ciudades de su mundo se quedaron en silencio por siglos, hasta que
les llegó la hora de la ruina y del fuego. {.espacio-arriba1}
4. El impacto del asteroide fue previsto por un científico iguanodonte,
Ssssumorssr del Cailano Yarepsíl, que de inmediato avisó al Consejo
General de Ancianos, el cuerpo rector de los más esclarecidos
dinosaurios. Con la sensatez que era habitual entre ellos,
discutieron las posibilidades al alcance de aquella civilización
racional, tecnológica e inmensamente antigua; concluyeron que el
asteroide no podía ser detenido, pero sí era posible, en cambio,
protegerse de las peores consecuencias de la catástrofe. Por lo
tanto, en el día Plenitud Imaginada de las Criaturas del año 82dce9
del Calendario Crivistorno, comenzó el magno proyecto: la excavación
de los grandes túneles y la construcción de las ciudades
subterráneas, en las que en apenas otro año pudieron refugiarse de
la ruina y el fuego todos los dinosaurios inteligentes, y donde son
felices hasta hoy porque ustedes no se pueden imaginar la belleza de
los jardines, las paredes pintadas, los soles artificiales que los
alumbran, ni el refinamiento y bondad de sus máquinas, que los
mantienen vivos e iguales sin arrebatarles la dignidad ni uncirlos a
labor injusta. {.espacio-arriba1}
5. El impacto del asteroide no fue previsto por nadie. No había quien
pudiera preverlo porque los dinosaurios no eran inteligentes. ¡Qué
tontería hablar de especies de dinosaurios inteligentes! En cambio,
siglos antes, hubo un resplandor en Chicxulub, en un llano que hoy
está sumergido: un círculo de luz que hubiera recordado los
“portales” que se ven las películas de Hollywood a cualquiera que
hubiese visto una, lo cual significa que no inquietó a ninguna
criatura. Por el portal (sí era un portal) entraron poco a poco los
camiones no tripulados, los robots y los drones voladores de la
empresa de cárnicos, todos con las armas en ristre, todos
obedientes. Y comenzó la masacre centenaria, violenta de todas las
criaturas que tuvieron a su alcance, y cuyos cuerpos muertos eran
cortados allí mismo, convertidos en paquetes, puestos en
refrigeración para ser llevados al siglo +++XXVIII+++ (¡de hecho era una
máquina del tiempo!), en el que ya es historia antigua la
destrucción permitida o propiciada por demagogos y oligarcas, pero
éstos ya se aburrieron de carne de esclavo, y por fin han terminado
de exterminar al resto de la raza humana, y quieren platillos
nuevos. {.espacio-arriba1}
6. El impacto del asteroide no fue previsto por nadie. En cambio,
siglos antes, hubo un resplandor en Chicxulub, en un llano que hoy
está sumergido: un círculo de luz que hubiera recordado los
“portales” que se ven las películas de Hollywood a cualquiera que
hubiese visto una, y por el cual entraron los pastores entrenados,
los cazadores y entrenadores que se llevaron a millones de
criaturas. Solo querían grabarlas en video para la escena culminante
de _Parque Jurásico 143: la Venganza del Regreso - Conclusión -
Segunda Vuelta_, que debía ser más impresionante que la de _Nuevos
Nuevos Vengadores - Fin de Partida 4 - Capítulo Por Fin Final_,
porque ya se había acabado el presupuesto asignado a la animación
digital… Pero una vez en el presente humano, los dinosaurios
resultaron ser notoriamente difíciles de controlar, se apoderaron de
las instalaciones que mantenían funcionando la máquina del tiempo y
se dedicaron, a partir de entonces, a cazar y comerse a aquellas
criaturas pequeñas y frágiles. {.espacio-arriba1}
7. El impacto del asteroide fue impedido, de hecho, por las enormes
máquinas de guerra espacial de los dinosaurios, que ya habían
vencido a las arañas de Marte y a las bestias diamantinas del
corazón de Júpiter. Así, las culturas de los dinosaurios perduraron
hasta el día de hoy, en el que sus demagogos y oligarcas las dominan
mediante el engaño y el odio, los dsungarípteros cometen genocidio
contra los pteranodontes, la catástrofe ecológica compite con la
nuclear a ver cuál ocurre primero y todos se están volviendo locos,
despacio, mientras sus comunidades se disuelven en odios ínfimos y
discusiones inútiles. Una científica aragosauria, llamada
Rrrromusrrs de la Clíspera Yonalia, ha inventado una máquina del
tiempo: una nave espaciotemporal que puede atravesar sin esfuerzo
las cuatro dimensiones, y tiene un plan desesperado: viajar al
tiempo previo al impacto e _impedir_ que las máquinas de guerra
espacial hagan su labor. Liberará una nube de espirilos salvajes en
la atmósfera de cada una. No la detendrán esas muertes. Ni las
otras. Que choque el asteroide. Que todo se acabe, porque la muerte
y el olvido son mejores que esto. (Tal vez, cumplida su labor, pueda
rescatar a algunos cuantos de sus remotos antepasados y llevárselos
en su nave, a la que ha llamado _Nada_, hacia otra parte, otro
futuro distinto). {.espacio-arriba1}

</section>
<section epub:type="chapter" role="doc-chapter">

# Various Hypotheses Explain the Absence of Dinosaur Remains Immediately Beneath the Geological Signature Known as the K-Pg Boundary, Which Attests to the Chicxulub Asteroid Impact on Earth and the Mass Extinction of Terrestrial Lifeforms at the End of the Cretaceous Period. But It Still Poses This Curious Enigma, Which Could Be Described as the Apparent Absence of Dinosaurs for Years Before the Fatal Moment. Then One Wonders, “Where Were the Dinosaurs? What Were They Doing? Didn’t They Have Anything Better to Do Than Wait for Their Own Demise in the Midst of Flames and Horrific Suffering?” I Surely Wouldn’t Blame Them If They Had Something Else in Mind

# 30. Various Hypotheses Explain the Absence of Dinosaur Remains Immediately Beneath the Geological Signature Known as the K-Pg Boundary, Which Attests to the Chicxulub Asteroid Impact on Earth and the Mass Extinction of Terrestrial Lifeforms at the End of the Cretaceous Period. But It Still Poses This Curious Enigma, Which Could Be Described as the Apparent Absence of Dinosaurs for Years Before the Fatal Moment. Then One Wonders, “Where Were the Dinosaurs? What Were They Doing? Didn’t They Have Anything Better to Do Than Wait for Their Own Demise in the Midst of Flames and Horrific Suffering?” I Surely Wouldn’t Blame Them If They Had Something Else in Mind @ignore

## By [Alberto Chimal](997b-semblances.xhtml#a02) \
Translated by [Toshiya Kamei](997b-semblances.xhtml#a53) {.centrado}

1. The asteroid’s impact hid the evidence of another extinction: the
one caused by mysterious carnivorous bacteria to which
twenty-eighth-century scientists, if we don’t become extinct before,
will give the popular name “wild spirilla.” Centuries before the
asteroid struck, the spirilla were already a threat: not only did
they kill creatures with eukaryotic cells (especially the
dinosaurs), they ate their bodies, leaving nothing, not even the
bones. The asteroid killed those invisible killers, which were
burned in the waves of fire, along with almost all of the creatures
that hadn’t yet become their victims. Then, a long time later, we
came to dig the earth.
2. The asteroid’s impact hid the evidence of another extinction: the
one caused by the diamond creatures that won’t have a name in the
twenty-eight century (and if we become extinct before, less likely
still). They nested at the bottom of the darkest caverns and the
deep sea, preferring darkness and high pressures. Vast, sparkling,
hard-skinned, and with enormous strength, no smarter than sharks,
they flew up to great heights, dropped, and snatched the dinosaurs,
poor distressed lizards, and then ate them in their remote lairs.
(When the asteroid hit Chicxulub, the explosion killed almost all
the diamond creatures, but some were expelled from Earth’s
atmosphere ---their bodies were enormously strong, and they could
also enter the dormant state, wrap themselves in a hermetic cocoon,
and last for years without air or food--- and after a long trip,
they reached the interior of Jupiter’s atmosphere, even darker and
oppressive than their old home, where they are happy to this day.)
3. The asteroid’s impact was foreseen by dinosaur culture’s seers, with
large brains and fangs, and tiny arms because their scope wasn’t
that of the material world (today we call them tyrannosaurs). They
saw it coming and, as that community of intelligent species had a
lot of precognition but very little telekinesis ---or tools of any
kind to divert asteroids--- they decided to commit mass suicide.
And they did it in what, for them, was the Flower of Language
Humility day of the year 8,576,234 of the Phaminophene Calendar, and
their world’s hills and cities remained silent for centuries until
the time of ruin and fire came. {.espacio-arriba1}
4. The asteroid’s impact was predicted by an Iguanodon specialist,
Ssssumorssr del Cailano Yarepsíl, who immediately notified the
General Council of Elders, the most enlightened dinosaurs’ governing
body. With the common sense that was recurrent among them, they
discussed the possibilities available to that rational,
technological and immensely ancient civilization. They concluded the
asteroid couldn’t be stopped, but it was possible, instead, to
protect themselves from the worst consequences of the catastrophe.
Therefore, on the day Imagined Plenitude of the Creatures of the
year 82dce9 of the Crivistorno Calendar, the great project began:
the excavation of the great tunnels and the construction of
subterranean cities, where all intelligent dinosaurs could take
refuge from the ruin and fire in just another year, and where they
are happy until today because you can’t imagine the beauty of the
gardens, the painted walls, the artificial suns that illuminate
them, or the refinement and goodness of their machines, which keep
them alive and equal without taking away their dignity or uniting
them to unjust work. {.espacio-arriba1}
5. The asteroid’s impact wasn’t foreseen by anyone. No one saw it
coming because the dinosaurs weren’t smart. What nonsense to talk
about intelligent dinosaur species! Rather, centuries earlier, there
was a glow in Chicxulub, on a plain that is now submerged: a circle
of light that would have resembled the “portals” that look like ones
from Hollywood movies to anyone who has seen one, which means that
it didn’t disturb any creature. Through the portal (it was a
portal), unmanned trucks, robots, and flying drones of the meat
company entered, little by little, all with weapons at their
disposal, all obedient. And thus began a century-old, violent
massacre of all the creatures within reach, and their carcasses were
cut right there, turned into packages, placed in refrigeration to be
taken to the twenty-eighth century (in fact, it was a time
machine!), where the destruction allowed or made possible by
demagogues and oligarchs is already ancient history, but they were
already bored with slave meat. Finally having killed off the
remaining humans, they want new dishes. {.espacio-arriba1}
6. The asteroid’s impact wasn’t foreseen by anyone. Rather, centuries
earlier, there was a glow in Chicxulub, on a plain that is now
submerged: a circle of light that would have resembled the “portals”
that look like ones from Hollywood movies to anyone who has seen one
and through which entered trained shepherds, hunters, and trainers
who took away millions of creatures. They only wanted to videotape
them for the climactic scene of _Jurassic Park 143: Revenge of the
Return - Conclusion - Second Return_, which had to be more
impressive than that of _The Newest Avengers: Endgame 4 - Final
Chapter_, because it had already used up the budget assigned to
digital animation. But once in the human present, the dinosaurs
turned out to be notoriously difficult to control. They seized the
facilities that kept the time machine running and, from then on,
they devoted themselves to hunting and gobbling up those small,
fragile creatures. {.espacio-arriba1}
7. The asteroid’s impact was prevented, in fact, by the dinosaurs’
large space warfare machines, which had already defeated spiders
from Mars and the diamond beasts from Jupiter’s heart. Thus,
dinosaur cultures have survived to this day, in which their
demagogues and oligarchs dominate them through deception and hatred,
the dsungarípteros commit genocide against the pteranodontes, the
ecological catastrophe competes with the nuclear to see which occurs
first and everyone is going crazy, slowly, while their communities
dissolve into petty hatreds and useless arguments. An aragosauria
scientist called Rrrromusrrs de la Clíspera Yonalia has invented a
time machine: a space-time ship that can effortlessly navigate four
dimensions and has a desperate plan: to travel to the time before
the impact and _prevent_ the space warfare machines from doing their
work. It’ll release a cloud of wild spirilla in each one’s
atmosphere. They won’t stop those deaths. Nor the others. Let the
asteroid collide. May everything be over, because death and oblivion
are better than this. (Perhaps, when his work is done, he can rescue
some of his remote ancestors and take them to his ship, which he has
called Nothing, toward another part, another different future.) {.espacio-arriba1}

</section>
<section epub:type="chapter" role="doc-chapter">

# Fósiles cretácicos, imágenes de una extinción

# 31. Fósiles cretácicos, imágenes de una extinción @ignore

## [Amílcar Amaya](997a-semblanzas.xhtml#a03) {.centrado}

Hace casi doscientos años, Joseph Nicéphore Niépce capturaría las
primeras imágenes que podríamos llamar fotografías, fueron tomadas desde
la ventana de su casa y las llamó “Puntos de vista”. En la actualidad es
algo complicado ver lo que hay impreso en esas antiguas placas, y eso
que nada más han pasado doscientos años.

Hay otra clase de fotografías, pero son muy difíciles de interpretar, y
son las que llamamos fósiles. Pueden llegar a ser muy duraderas ---vaya,
muchas se conservan en rocas---, pero si desconocemos el contexto en el
que se desarrollaba la cosa preservada, no tienen demasiada utilidad.
Son inherentemente importantes porque nos permiten recrear el mundo como
era hace miles o millones de años, aunque siempre es mejor cuando puedes
unir esas piezas con las que van arriba, abajo y a los lados. Muchas
veces, ese contexto global es algo de lo que carecemos al hablar del
registro fósil.

Robert A. DePalma, investigador del Departamento de Geología de la
Universidad de Kansas, publicó hace unos meses (abril de 2019) un primer
artículo donde exponía algo que, de corroborarse, sería uno de los
grandes descubrimientos de la paleontología moderna.

Es difícil encontrar en la comunidad paleontológica alguien que no
reconozca la fortaleza de la teoría de un impacto meteórico en el límite
K/Pg (Cretácico/Paleógeno). Se calcula que entre 70 y 80% de las
especies de seres vivos fueron afectadas o desaparecieron durante aquel
suceso. Lo que se pone en duda con frecuencia es el alcance del efecto
que tuvo ese evento sobre la extinción de los dinosaurios, una clase
carismática que DePalma usa para presentar su descubrimiento y el de su
equipo al mundo.

Otras evidencias indican que la Tierra estaba atravesando por un momento
muy convulso, con la separación de los continentes que formaban Pangea,
una creciente actividad volcánica… en resumen, es posible que el
meteorito _per se_ no fuera la causa directa de la extinción, sino, tal
vez, la enorme gota que derramó el vaso.

Aunque hay mucha evidencia del impacto de un bólido espacial, como el
cráter de Chicxulub, para empezar, y capas de sedimento con trazas de
iridio (elemento raro en la Tierra pero común en meteoritos y
asteroides) alrededor del mundo, siempre ha existido el interés por
encontrar algo más… sustancial. Algún resto que nos hablara si no del
instante mismo del impacto, sí de las horas subsecuentes, y poder ver
las primeras consecuencias de un fenómeno tan catastrófico. Se ha
supuesto que el choque habría generado olas de tsunami que habrían
podido alcanzar un kilómetro de altura cerca de la zona de impacto.
Suena increíble, pero en ese momento lo increíble tenía todo el sentido
del mundo.

Debemos recordar que hace sesenta y cinco millones de años, un gran mar
interior cubría el centro de lo que hoy es Estados Unidos, y una ola
gigante habría podido cubrir esa distancia sin problemas.

Lo que DePalma ha puesto sobre la mesa de los investigadores en su
artículo es que, en una localidad de Dakota del Norte llamada Tanis, las
ondas sísmicas, que viajan mucho más rápido y a mayor distancia que las
acuáticas, transformaron el ambiente de la región con violentas
inundaciones, apenas horas antes de que llegara el propio tsunami.

Incluso para los geólogos y paleontólogos el tiempo geológico es difícil
de asimilar. Los efectos climáticos o ecológicos de eventos
transformadores tales como una extinción masiva, solo son apreciables
como un conjunto de cambios que suceden a lo largo de millones de años.
Es como si tomáramos la fotografía de un paisaje con una cámara de muy
baja resolución: podemos adivinar la forma de las montañas al fondo, el
color del pasto y tal vez si había una persona caminando por ese campo
al momento de tomar la foto, pero nada más fino que eso.

Siguiendo esa analogía, DePalma y su equipo de colaboradores habrían
tomado una pequeña parte de esas secciones borrosas para convertirla en
una imagen de alta definición. Describen un ambiente muy energético:
aguas muy agitadas, mezcla de plantas y animales provenientes de una
amplia diversidad de ambientes. La suposición es que los terremotos
provocados por el impacto en Chicxulub, a más de tres mil kilómetros de
distancia, causaron importantes levantamientos de tierra que,
consecuentemente, empujarían grandes masas de agua hacia arriba,
inundando extensas áreas costeras, llenándolas con sedimentos y
desechos. La gran cantidad de sedimento suspendido cubriría con gran
rapidez los restos, facilitando su conservación.

Pero, como decía Carl Sagan: afirmaciones extraordinarias requieren
evidencias extraordinarias.

Si bien el trabajo presentado por DePalma de entrada parece robusto, más
de una ceja se ha levantado por las circunstancias en las que se dio el
anuncio. El equipo encargado del descubrimiento es consciente de que, si
hay algo que llame la atención sobre el periodo cretáceo, ese algo tiene
que ver con dinosaurios. Y el artículo, de por sí con información muy
importante, apenas los menciona. Ese no es el problema, el pasado de la
vida en la Tierra es mucho más que dinosaurios, pero DePalma anunció en
The New Yorker, una publicación no muy relacionada con la prensa
científica, el descubrimiento de múltiples fósiles de dinosaurios,
cuando en un documento adjunto, ni siquiera en el artículo principal,
apenas menciona uno.

El trabajo de DePalma tiene el potencial de ser de las primeras
fotografías finas de un período lejano en la historia de la vida,
temporalmente muy acotada. Un trabajo paciente y detallado arrojaría
muchísimos datos interesantes sobre alteraciones geológicas y ecológicas
en tan particulares condiciones. Pero la búsqueda del sensacionalismo,
aún con las mejores intenciones, no acarrea ninguna ventaja, solo
escepticismo y dudas. Ahora, no es que la ciencia no funcione con
escepticismo, lo bueno que tiene el método científico es que cuando
propones algo que cambiará algún paradigma, siempre habrá alguien que
pondrá a prueba tu idea, ya sea para refutarla o, en el mejor de los
casos, corroborarla. Y a festejar.

## Referencias:

* [A seismically induced onshore surge deposit at the KPg boundary, North Dakota](https://www.pnas.org/content/116/17/8190)

* [Astonishment, skepticism greet fossils claimed to record dinosaur-killing asteroid impact](https://www.sciencemag.org/news/2019/04/astonishment-skepticism-greet-fossils-claimed-record-dinosaur-killing-asteroid-impact) {.sin-sangria espacio-arriba1}

* [The Day the Dinosaurs Died](https://www.newyorker.com/magazine/2019/04/08/the-day-the-dinosaurs-died) {.sin-sangria espacio-arriba1}

</section>
<section epub:type="chapter" role="doc-chapter">

# Cretaceous fossils, images of an extinction

# 31. Cretaceous fossils, images of an extinction @ignore

## By [Amílcar Amaya](997b-semblances.xhtml#a03) \
Translated by [Valeria Becerril](997b-semblances.xhtml#a54) {.centrado}

Almost two hundred years ago, Joseph Nicéphore Niépce captured the first
images of what we may call photographs, he took them from his house
windows and called them “Point of view”. Currently is a little
complicated to see what’s printed in those old plaques, even though it
has only been two hundred years.

There is another kind of photographs, but they are more difficult to
interpret, and they’re what we call fossils. They can be very lasting, I
mean, several are conserved as rocks, but if we don’t know the context
in which the preserved was developing, they are not that useful. They
are inherently important because they allow us to recreate the world as
it was thousands or millions of years ago, although it is always better
when you can assemble those pieces with what goes up, down or to the
sides. Regularly, that global context is something that we lack when
talking of the fossil registry.

Some months ago (April 2019), Robert A. DePalma, researcher on the
Geology Department of Kansas University, published a first article where
he presents something that, if corroborated, would be one of the
greatest discoveries in modern paleontology.

Is difficult to find in the paleontology community someone that does not
recognizes the strength of the meteorite impact theory in the limit K/Pg
(Cretaceous-Paleogene). It is estimated that between 70 and 80% of the
species of living beings were affected or disappeared during that event.
What is constantly challenged is the reach of the event’s effect over
the extinction of the dinosaurs, a charismatic class used by DePalma to
present his and his team’s discovery to the world.

Other evidence points to the Earth going through a highly turbulent
time, with the continent that made up Pangea separating, an[]{#anchor}d
increased volcanic activity… in summary, it is possible that the
meteor _per se_ was not the direct cause of extinction, but, maybe, the
straw that broke the camel’s back.

Although there’s plenty of evidence of the impact of a comet, like the
crater in Chicxulub and sediment layers with iridium traces (rare
elements in the Earth but common in meteorites and asteroids) around the
world, there has always been interest in finding something more…
substantial; some clue that would explain to us what happened if not at
the moment of impact, at least during the subsequent hours, and be able
to see the first consequences of such catastrophic phenomenon. It has
been thought that the impact would have generated kilometer high tsunami
waves near the impact zone. It sounds incredible, but in that moment
incredible made all the sense in the world.

We must remember that seventy-five million years ago, a great inland sea
covered the center of what today is the United States, and a giant wave
could have easily covered that distance.

What DePalma has placed on the researcher’s table in his article is that
in a North Dakota town called Tanis, the seismic waves, that travel much
faster and to a greater distance than the aquatics, transformed the
environment of the region with violent floods, barely hours before the
tsunami landed.

Even for the geologists and paleontologists the geological period is
hard to assimilate. The climatic or ecologic effect of transforming
events such as a massive extinction, are only visible as a set of
changes throughout millions of years. Is like if we took a photograph of
a landscape with a very low resolution camera: we can guess the shape of
the mountains at the background, the grass color and maybe if there was
a person walking by that field at the moment the photo was taken, but
nothing more detailed than that.

Following that analogy, DePalma and his team of collaborators would have
taken a little piece of those blurry sections to convert it into a high
definition image. They describe a very energetic environment: choppy
waters, a mix of plants and animals stemming of a vast variety of
environments. The assumption is that the earthquakes caused by the
impact in Chicxulub, more than three thousand kilometers long, caused
important earth risings that, consequently, would push big masses of
water up, flooding large coastal areas, filling them with sediment and
waste. The great quantity of the suspended sediment would cover quickly
the remains, allowing their preservation.

But, like Carl Sagan used to say: extraordinary affirmations require
extraordinary evidence.

While the work presented by DePalma at first seemed robust, it has
caused some doubts due to the circumstances of where the announcement
was made. The team in charge of the discovery is aware that, if there is
something that draws attention about the cretaceous period is the
dinosaurs. And the article, that contains very important information,
barely mentions it. That is no problem, the past of life on Earth is so
much more than just dinosaurs, but DePalma announced in the New Yorker,
a publication not very related with the scientific press, the discovery
of multiple dinosaur’s fossils, meanwhile only one dinosaur discovery
it’s barely mentioned in the attachment of the main scientific article.

DePalma’s work has the potential of prividing the first detailed
photographs of a distant period in the history of life, a very
abbreviated time. A patient and detailed work would throw many
interesting data about geological and ecological alterations in such
particularly conditions. But the search of sensationalism, even with the
best of intentions, carries no advantage, just skepticism and doubts.
It’s not that the science doesn’t work with skepticism, the advantage of
the science method is that when you propose something that would change
some paradigm, there will always be someone there to test your idea, be
it to refute it or, in the best case scenario, corroborate it. And all
that’s left is to celebrate.

## References:

* [A seismically induced onshore surge deposit at the KPg boundary, North Dakota](https://www.pnas.org/content/116/17/8190)

* [Astonishment, skepticism greet fossils claimed to record dinosaur-killing asteroid impact](https://www.sciencemag.org/news/2019/04/astonishment-skepticism-greet-fossils-claimed-record-dinosaur-killing-asteroid-impact) {.sin-sangria espacio-arriba1}

* [The Day the Dinosaurs Died](https://www.newyorker.com/magazine/2019/04/08/the-day-the-dinosaurs-died) {.sin-sangria espacio-arriba1}

</section>
<section epub:type="chapter" role="doc-chapter">

![](../img/lauren-snow_madestring-hechoscuerdas.jpg)

# _Estamos hechos de cuerdas_ de [Lauren Raye Snow](997a-semblanzas.xhtml#a49) {.centrado}

Luego de ver varias representaciones sobre la teoría de cuerdas, me empezaron a fascinar las visualizaciones artísticas de las diminutas cuerdas que constituyen la parte más pequeña de todas las cosas. Imaginé un ser cósmico, flotando en el espacio, tanto inmenso como minúsculo, con pequeñas cuerdas enlazadas reverberando por todo su cuerpo, que es a la vez una cuerda y el vacío del espacio.

# [Lauren Raye Snow](997b-semblances.xhtml#a49), _We Are Made of String_ {.centrado}

# 32. Estamos hechos de cuerdas / We Are Made of String @ignore

As I watched several presentations about string theory, I was fascinated by artists’ visualizations of the tiny strings that make up the smallest part of all things. I imagined a cosmic being, floating through space, perhaps both immense and minuscule, and tiny looped strings reverberating through her body, which is both string and the void of space.

</section>
<section epub:type="chapter" role="doc-chapter">

# Luz de estrella

# 33. Luz de estrella @ignore

## [Angela Lujan](997a-semblanzas.xhtml#a07) \
Traducido por [Andrea Chapela](997a-semblanzas.xhtml#a05) {.centrado}

Abigail no se mueve mientras observa el cielo nocturno por la ventana.
Coloca una mano sobre la superficie fría del cristal. Ojalá pudiera ver
más que ese puñado de estrellas, cuya luz apenas es suficiente para
atravesar la contaminación lumínica de la ciudad.

Esa ciudad se está alejando cada vez más. Su hogar se está alejando cada
vez más. El bamboleo del elevador la obliga a apoyarse en la ventana.
Por un momento, teme que el cristal se rompa y ella caiga hacia la
Tierra. Pero se mantiene, sólido, frío bajo sus dedos. Cuando se vuelve
a sentir segura, camina de regreso a su asiento.

El elevador es grande. Hay otras personas sentadas sin orden, pero
apiñadas junto a las ventanas. Como Abigail, la mayoría viajan solas.
Ella está segura de que extrañarán la Tierra, pero nadie los extrañará a
ellos.

Abigail siempre había buscado silencio y soledad. Pero ya comienza a
echar de menos la comodidad del hogar. Quería alejarse del sonido, del
caos y las luces falsas de la ciudad, encontrar una estabilidad apacible
y contemplar, por fin, la luz real de las estrellas.

En soledad, el silencio le atrae menos. Anhela el ruido y distracción
urbanos. Le servían de contraste. La exponían. Aquí, en este silencio,
se siente una extensión de su entorno. Sólo más silencio. Observa la
oscuridad entre las estrellas. Sabe que alguna vez estuvieron más cerca,
pero que una fuerza en el universo las está alejando. Lo aleja todo cada
vez más. Imagina una mano que empuja las galaxias como si fueran migas
de pan. La imagina acunando el elevador, llevándola lejos de la ciudad,
de la Tierra, hacia otro planeta y otro universo.

Se estremece y aparta ese pensamiento. Mira de nuevo por la ventana.
Allá abajo, la ciudad es solo un punto de luz. Desde aquí las estrellas
se ven más brillantes. Más abundantes. Las mira moverse por el
firmamento. Sonríe. Ha encontrado por fin luz de estrella. Su viaje
apenas comienza.

</section>
<section epub:type="chapter" role="doc-chapter">

# Starlight

# 33. Starlight @ignore

## By [Angela Lujan](997b-semblances.xhtml#a07) {.centrado}

Abigail stands very still as she gazes out the window. She places a hand
on the cold glass as she peers into the night sky. She wishes could see
more than the handful of stars whose light is strong enough to pierce
the light pollution of the nearby city.

The city is moving farther away. Her home is moving farther away. She
sways with the movements of the elevator and keeps her balance by
leaning more weight against the window. For a moment she fears the glass
will crack and she will tumble back down to Earth. It holds strong, cool
against her hand. When she is more confident in her balance, she walks
back to her seat.

The elevator is spacious. Others are seated sporadically, clustered near
the windows. Most of them are alone, like Abigail. Abigail is sure that
they too will miss Earth, but not be missed.

The sterility and the silence up here were what she had craved. But
she’s already begun to need the comforts of her home. She’d wanted to
move away from the noise, the chaos, the false light of the city. To
find a calm steadiness, and finally see the real light of the stars.

Alone, the silence is less appealing. She craves the noise, the
distraction of the city. It juxtaposed her. Revealed her. Here, in
silence, she feels she is just more silence. An extension of her
surroundings. She looks into the blankness between the stars. She knows
those stars used to be closer together, that a force in this universe
pushes all things further and further apart. She imagines a great hand,
swiping galaxies aside like breadcrumbs. She imagines it cradling the
elevator, pulling her away from the city, away from Earth, to another
planet, another universe.

She shudders, and lets the thought go. She glances again toward the
window. Far below, the city is a pinpoint of light. The stars are
already brighter here. The stars are more plentiful here. She sees them
sweeping across the sky. She smiles. She’s found starlight, and her
journey has just begun.

</section>
<section epub:type="chapter" role="doc-chapter">

# Lemniscata

# 34. Lemniscata @ignore

## [Nelly Geraldine García-Rosas](997a-semblanzas.xhtml#a33) {.centrado}

E L I S A. Escribo cada letra con el mismo cuidado con que limpio los
espejos del interferómetro. ELISA. Repaso cada línea y curva para
asegurarme de que puedo escribir tu nombre incluso sin una pluma,
incluso con los ojos cerrados. E-L-I-S-A. Memoria muscular, ondas
imperceptibles que se propagan desde mi cuerpo a la velocidad de la luz,
trazos sin tinta que escriben en la urdimbre del espacio-tiempo.

Hace mil millones de años chocaron dos agujeros negros de masa estelar.
Habían estado orbitándose por un tiempo, bailando, acercándose cada vez
más rápido, hasta que se fundieron de forma tan violenta que todo el
universo pudo sentirlo. “Por un momento se vieron como una lemniscata”,
me dijiste, “como si quisieran representar la infinitud por sí mismos.
Pero, en un instante, se volvieron un círculo gigante, un cero, nada”.
Me reí porque estabas pensando en dos dimensiones. En tu mente, esos
agujeros negros eran figuras planas en una hoja de papel. Pero no se
hicieron “nada”, ahora son un objeto más grande. No como nosotras. Me
pregunto si alguien, alguna vez, medirá cómo distorsionamos el
espacio-tiempo cuando chocamos a nuestro modo particular, Elisa, aquella
vez que fuimos infinitas por un instante.

A veces, cuando escribo tu nombre, me pongo a trazar la S una y otra
vez. Parece una onda, como la que ha estado apareciendo en el
espectrograma los últimos días: un _glitch_ con un patrón de eco.
Calculé la frecuencia de sus repeticiones y estoy segura de que una onda
más grande viene en camino.

¿Cuál es la longitud de onda de tu nombre, Elisa? ¿Por qué te fuiste?
Tengo la necesidad de decir tantas cosas, de gritar, de que alguien se
entere cuán sola estoy en esta estación. Por favor, escúchame. Quien
sea.

Me llevó algo de tiempo codificar estos datos en forma de una onda
no-métrica que pueda montarse en la gran ola que viene. Miro este enorme
océano y, de todas formas, voy a tirar una botellita con un mensaje que
viajará como parte del universo mismo. Ahora tu nombre es infinito,
ELI8A.

</section>
<section epub:type="chapter" role="doc-chapter">

# Lemniscate

# 34. Lemniscate @ignore

## By [Nelly Geraldine García-Rosas](997b-semblances.xhtml#a33) {.centrado}

E L I S A. I write every letter with the same care I clean the
interferometer’s mirrors. ELISA. I retrace every line and curve just to
prove myself I could write your name even without a pen, even with my
eyes closed. E-L-I-S-A. Muscle memory, imperceptible waves that
propagate away from me at the speed of light, inkless hand movements
that write on the very fabric of space-time.

A billion years ago, two stellar-mass black holes collided. They had
been orbiting each other for a while, dancing, getting closer and
closer, faster and faster, until they merged so violently that the whole
universe sensed it. “For a moment they looked like a lemniscate curve,”
you told me, “as if they wanted to represent infinity by themselves.
But, in an instant, they became a big circle, a zero, nothing.” I
laughed because you were thinking in two dimensions. In your head, those
black holes were plane figures on a paper sheet. But they didn’t become
“nothing,” they’re something bigger now. Unlike us. I wonder if someone,
somewhere, will ever measure the way we distorted space-time when we
crashed together in our own way, Elisa, that time we were infinite for
an instant.

Sometimes, when I write your name I find myself tracing the letter S
over and over. It resembles a wave, like the one that has been appearing
in the spectrogram for the past days: a glitch with an echo pattern.
I’ve calculated the frequency of its repetitions and I’m sure there is a
big one on the way.

What is the wavelength of your name, Elisa? Why did you leave? I have an
urge to say so many things, to shout, to let you know how alone I am in
this station. Please, listen to me. Anyone.

It took me some time to encode this data as a non-metric wave that can
be carried by the big ripple that is coming. I look at this vast ocean
and, nonetheless, I’m throwing a tiny bottle with a message that will
travel within the universe itself. Now your name is infinite, ELI8A.

</section>
<section epub:type="chapter" role="doc-chapter">

# Universo

# 35. Universo @ignore

## [Édgar Omar Avilés](997a-semblanzas.xhtml#a13) {.centrado}

## I {.centrado}

Cuando el primer astronauta asomó la cabeza fuera del universo, vio
asombrado el cadáver de la víctima con la que nos habíamos impactado.

## II {.centrado}

Cuando el primer astronauta asomó la cabeza fuera del universo, vio
asombrado cómo el universo se empezaba a desinflar.

## III {.centrado}

Cuando el primer astronauta asomó la cabeza fuera del universo, vio
asombrado la garganta de la que proveníamos como una risotada.

## IV {.centrado}

Cuando el primer astronauta asomó la cabeza fuera del universo, vio asombrado el asombro de los demás astronautas que asomaban la cabeza en los miles de universos paralelos.

</section>
<section epub:type="chapter" role="doc-chapter">

# Universe

# 35. Universe @ignore

## By [Édgar Omar Avilés](997b-semblances.xhtml#a13) \
Translated by [Toshiya Kamei](997b-semblances.xhtml#a53) {.centrado}

## I {.centrado}

When the first astronaut stuck his head out of the Universe, he saw,
with amazement, the dead victim we had crashed into.

## II {.centrado}

When the first astronaut stuck his head out of the Universe, he watched
in amazement as the Universe began to go flat.

## III {.centrado}

When the first astronaut stuck his head out of the Universe, he saw,
with amazement, the throat that spat us out like a cackle.

## IV {.centrado}

When the first astronaut stuck his head out of the Universe, he was
amazed at how astonished other astronauts were as they stuck their heads
into thousands of parallel Universes.

</section>
<section epub:type="chapter" role="doc-chapter">

![](../img/alejandra-espino_deshilada-frayed01.jpg)

![](../img/alejandra-espino_deshilada-frayed02.jpg)

![](../img/alejandra-espino_deshilada-frayed03.jpg)

# _Deshilada_ de [Alejandra Espino](997a-semblanzas.xhtml#a41) {.centrado}

Del proceso, creo que me inspiraron principalmente el texto de [_La Física pende de una cuerda_](http://www.comoves.unam.mx/numeros/articulo/108/la-fisica-pende-de-una-cuerda), y el [video](https://www.youtube.com/watch?v=kpJ51h7bi8g) en el que Brian Cox habla con Leonard Susskind sobre multiversos. La idea de dimensiones ocultas, y de que si de alguna manera pudiéramos curvar o alterar nuestra realidad se abren posibilidades infinitas (muchas que no pueden ni siquiera ser), esta idea de curvar y de tensiones la pensé siempre como una danza, y visualmente la uní no sé porqué mecanismos de mi cabeza con el Ballet Triádico de Oskar Schlemmer, de donde se inspira el vestuario de la personaja, ya sabes, me gusta moverme muy en los años veinte.

# [Alejandra Espino](997b-semblances.xhtml#a41), _Frayed_  {.centrado}

# 36. Deshilada / Frayed @ignore 

For the process, I think I was mainly inspired by a physics text [hanging by a string](http://www.comoves.unam.mx/numeros/articulo/108/la-fisica-pende-de-una-cuerda), and the [video](https://www.youtube.com/watch?v=kpJ51h7bi8g) in which Brian Cox talks with Leonard Susskind about multiverses. The idea being one of hidden dimensions, that if we could somehow curl or alter our reality, infinite possibilities open up (many that cannot even be.) I always imagined this curving and tension as a dance, and visually my head blended that via who-knows-what mechanisms with Oskar Schlemmer’s Triad Ballet, from which the costume of the character is inspired. As you may guess, I like very much to inhabit a roaring 20’s space.

</section>
<section epub:type="chapter" role="doc-chapter">

# Pude verla en un espejo

# 37. Pude verla en un espejo @ignore

## [Daniela Venegas](997a-semblanzas.xhtml#a11) {.centrado}

Le tomó unos instantes darse cuenta de que la voz provenía del anciano
que se reflejaba junto a ella.

---¡Señorita, yo pude verla en un espejo!

Anna echó un vistazo a los espejos que los rodeaban en la tienda y se
vio reflejada desde cientos de ángulos a la vez.

---Podemos vernos en todos, mire ---le respondió en tono dulce al dueño
de la tienda, dudando de su salud mental.

El anciano le ofreció un objeto que Anna recibió amablemente, antes de
casi dejarlo caer por la sorpresa. El espejo que sostenía entre sus
manos mostraba una imagen invertida de su propia habitación.

---Yo pude verla ayer en este espejo, parece que su yo del otro lado lo
compró.

---¿Cómo es posible?

---Lo que muestran los espejos no es un reflejo de este universo, sino
de universos paralelos casi idénticos a éste. Este espejo es una ventana
a uno donde sus acciones están desfasadas con respecto al nuestro. Tal
vez usted sea la única diferencia entre estas dos realidades en
particular.

---¿Cómo sabe usted todo esto?

---Hace cuarenta años vi a… alguien en un espejo, alguien que era
imposible que estuviera ahí. Desde entonces me he dedicado a buscar
otras diferencias. Necesitaba comprobar mi cordura. Déjeme regalárselo,
pero tenga mucho cuidado de no romperlo, los fragmentos reflejarían
realidades distintas al original y se perdería la ventana a ese curioso
universo alterno.

En cuanto Anna se retiró, el anciano cerró la tienda, suspiró y fue a
sentarse a su escritorio. {.espacio-arriba1 .sin-sangria}

Con movimientos lentos abrió un cajón y se vio reflejado en un espejo
quebrado del tamaño de un cuaderno. Cerró fuertemente los ojos por
varios segundos, al abrirlos una lágrima corrió hasta su barbilla. Tomó
un pequeño paquete del mismo cajón y lo abrió, revelando un último
fragmento que completaba el rectángulo. Con cuidado, lo colocó sobre la
muñeca y presionó el filo contra su delgada piel mientras sonreía.

No estaba loco, en verdad había podido verla en un espejo 40 años antes,
y ahora descansaría sabiendo que, en algún universo, su pequeña había
sobrevivido.

</section>
<section epub:type="chapter" role="doc-chapter">

# Reflected in a Mirror

# 37. Reflected in a mirror @ignore

## By [Daniela Venegas](997b-semblances.xhtml#a11) \
Translated by [Valeria Becerril](997b-semblances.xhtml#a54) {.centrado}

It took her a moment to figure out that the voice came from the old man
reflected beside her.

“Miss, I could see you in a mirror!”

Anna saw all the mirrors that filled the store and saw herself reflected
on hundreds of them at a time.

“We are in all of them, look,” she answered the store owner in a sweet
tone, doubting his sanity.

The old man offered her an object that Anna accepted kindly, before
almost dropping it in surprise. The mirror she was holding showed an
inverted image of her own room.

“I could see you in this mirror yesterday, it seems that your alter ego
from the other side bought it.”

“How is this possible?”

“What’s shown in mirrors is not a reflection of this universe, but of
the parallel universes almost identical to this one. This mirror is a
window to a place where there is a time-lag in relation to our universe.
Maybe you are the only difference between these two particular
realities.”

“How do you know all this?”

“Forty years ago I saw… someone in a mirror, someone who couldn’t have
been _there_. Since that day I’ve dedicate myself to look for other
variants. I needed to see I wasn’t crazy. Let me gift it to you, but be
very careful not to break it, the fragments would reflect alternate
realities to the original one and the window to this curious alternate
universe would be lost.”

The moment Anna left, the old man closed the store, sighed and went to
sit down at his desk.

With slow movements he opened the drawer and saw himself reflected in a
broken mirror the size of a notebook. He shut his eyes for several
seconds, when he opened them a tear ran down his cheek. He took a little
package from the same drawer and opened it, revealing one last fragment
that completed the rectangle. Carefully, he placed it over his wrist and
pressed the edge against his thin skin while he smiled.

He wasn’t crazy, he could actually see her reflected in the mirror forty
years ago, and now he would rest knowing that, in some universe, his
little girl had survived.

</section>
<section epub:type="chapter" role="doc-chapter">

# La teoría de Pablo

# 38. La teoría de Pablo @ignore

## [Agustín Fest](997a-semblanzas.xhtml#a01) {.centrado}

Perseguimos a la sospechosa, tiene biología aumentada. Soy una novata,
me apellido Pablo. Sigo a la teniente. La teniente dispara su escopeta,
el monstruo cae. Según las clases de sensibilización, no deberíamos
llamarle monstruo, pero nadie tiene que saber lo que ocurre en mi
cabeza. Todavía no. Cuando ascienda a oficial tendré más cuidado. Quiero
acercarme a ver, pero mi superior alza el brazo y lo impide. “No
entiendes ---dice la teniente---, al parecer no somos gente, somos otra
cosa”. Habla incoherencias, quizás debería reportarla. La sospechosa
suelta un alarido. “Nos está comiendo, Pablo. Está en los sonidos. Está
consumiendo el ruido de nuestras cabezas”. ¿Cómo nos está comiendo,
Pablo?, quiero preguntarle, pero se desata un terremoto, las luces
parpadean, las paredes se rompen.

La sospechosa abre la boca y escupe sangre. Se ha modificado tanto que
es una deformidad, humanidad negada y abandonada. Suelta una última
mirada, sonríe herida y enigmática. Soy la teniente Pablo, suelto el
arma, me duele el hombro por el empujón del disparo. La novata recoge el
arma porque teme el regaño de nuestros superiores, teme que lean su
cabeza así como definitivamente leerán la mía por ser una oficial. “Qué
necia eres, no se trata de eso”. Nada de esto está bien, la novata no ha
entendido que compartimos una historia, que la sospechosa tiene algo
extraño. Me arrodillo, de pronto me siento muy cansada. ¿Es una
simulación de la academia? ¿Un ejercicio?

El dolor se extiende. Me desangro. ¿Cuándo regresó el disparo? La novata
se acerca porque mi grito la preocupa pero yo quiero reír. No puedo,
pero quiero hacerlo. No veo a la teniente, quizá ya se ha ido. Creo que
ya estoy comprendiendo: somos un _jingle_, el eterno retorno. Mis
piernas están deshechas. Pablo alza la escopeta, porque tiene un poco de
piedad, y yo no puedo explicarle que rechace las enseñanzas académicas,
porque eso podría sacarnos de la repetición. Me falta boca, solo tengo
ojos, unos cuantos dedos y algunas extremidades inútiles. Miro el cañón
de la escopeta a unos centímetros de mi cabeza. Soy Pablo, el monstruo.
Soy crimen.

</section>
<section epub:type="chapter" role="doc-chapter">

# Pablo’s Theory

# 38. Pablo’s Theory @ignore

## By [Agustín Fest](997b-semblances.xhtml#a01) {.centrado}

We’re in pursuit of the suspect, she has augmented biology. I’m a
rookie, my last name is Pablo. I follow the lieutenant. The lieutenant
shoots her shotgun, the monster falls. Sensitivity classes: we shouldn’t
call it a monster, but no one has to know what goes in my head. Not yet.
I’ll be more careful when I become an officer. I want to get near and
see, but my superior raise her arm and stops me. “You don’t understand”,
says the lieutenant, “it seems we aren’t people, we’re something else.”
She doesn’t make sense, I should report her. The suspect scream. “She’s
eating us, Pablo. She is in the sound. She’s using the noise in our
heads to feed herself.” How is she eating us, Pablo, I want to ask her
but there is a quake, the lights flicker, the walls break.

The suspect opens her mouth and spits blood. She’s so modified that she
is a deformity, abandoned and denied humanity. She looks at us one last
time, she has a hurt and mysterious smile. I’m Pablo, the lieutenant, I
drop my gun. My shoulder hurts, shotgun’s kick was too hard. The rookie
picks up the weapon because she’s afraid of our bosses, she is afraid
they’ll read her head like they’ll read mine because I’m an officer.
“You’re so stubborn, it’s not about this.” Everything is wrong, the
rookie doesn’t get it, we share a history, this suspect is weird. I
kneel, suddenly I’m very tired. Is this a simulation of the Academy? An
exercise?

Pain is spreading. I’m bleeding out. When did she return fire? The
rookie is getting closer to me because my scream worried her but I want
to laugh. I can’t, but I want to. I can’t see the lieutenant anymore,
maybe she’s gone. I think I’m getting the grasp of this: we’re a jingle,
an eternal return. My knees are broken. Pablo raises her shotgun because
she wants to give me mercy and I can’t explain her she should reject the
academic teachings because that could help getting us out of this loop.
I don’t have a mouth, I have eyes, some fingers and some useless limbs.
I look at the shotgun’s mouth a few inches apart from my head. I’m
Pablo, the monster. I’m crime.

</section>
<section epub:type="chapter" role="doc-chapter">

# En la orilla de la nada

# 39. En la orilla de la nada @ignore

## [Efraím Blanco](997a-semblanzas.xhtml#a14) {.centrado}

El científico miró otra vez la fórmula que lo resolvía todo y luego miró
otra vez hacia el infinito. Estaba, según sus cálculos, a la orilla de
la nada. Lo único que sostenía su humanidad era esa cuerda y la mesa que
había logrado equilibrar sobre ella. Encima de la mesa descansaba la
hoja de papel con la fórmula matemática que resolvía la teoría del
multiverso. Las simulaciones en el laboratorio eran bastantes claras: si
tensaba la cuerda, crearía más universos casi iguales al suyo; si
aflojaba un poco, algún universo casi idéntico sería destruido en otro
lado. Lo único que equilibraba la cuerda encima de esa nada era el
pequeñísimo dios que la sostenía del otro lado. Estaban tan lejos uno
del otro que apenas si alcanzaban a verse. Ni el dios ni el científico
estaban seguros de lo que ocurriría a continuación, pues si se fijaban
bien, aquella nada era una casa de espejos donde su imagen se reflejaba
sin fin en lo que parecía ser el ombligo de cada universo. Descansaban,
entonces, sobre la cuerda original que habría dado origen a todo.

Llevaban así un buen rato cuando un colibrí se paró encima de la cuerda.

Para otros no fue un ave, sino un dragón o una mosca, un delfín con
gafas o John Lennon con una guitarra desafinada. Todas las cuerdas, eso
sí, se sacudieron al mismo tiempo. Todos los dioses lloraron y el
científico entendió que había llegado el momento. Se acomodó la bata,
los lentes, miró otra vez la fórmula y luego asintió con un pequeño
movimiento de cabeza hacia todos los otros científicos encima de una
cuerda que asentían de regreso hacia él. Supo la verdad y miró por
última vez a los dioses, al tiempo que sacudía la cuerda con violencia,
como si quisiera librarse del colibrí que la tensaba, del dragón, de las
moscas, de todo lo que sobrara para terminar de crear en paz su propio
universo multiplicado por los tiempos de los tiempos.

El colibrí voló.

</section>
<section epub:type="chapter" role="doc-chapter">

# On the Edge of Nothing

# 39. On the Edge of Nothing @ignore

## By [Efraím Blanco](997b-semblances.xhtml#a14) \
Translated by [Toshiya Kamei](997b-semblances.xhtml#a53) {.centrado}

The scientist glanced again at the formula that solved everything and
then again toward infinity. He was, according to his calculations, on
the edge of nothing. The only thing that kept him alive was that rope
and the table he had managed to balance on it. Above the table rested
the sheet of paper with the mathematical formula that solved the
multiverse theory. Lab simulations were quite clear: if he tightened the
rope, he would create more universes almost equal to his own; if it he
loosed it a little, some almost identical universe would be destroyed
somewhere. The only thing that balanced the rope on top of that nothing
was a tiny god who held it on the other side. The scientist and the god
were so far away they could barely see each other. Neither of them was
sure of what would happen next, because if they looked carefully, that
nothing was an infinite house of mirrors where their images were
reflected endlessly in what seemed to be the navel of each universe.
They rested, then, on the original rope that would have originated
everything.

They had a good time when a hummingbird perched on top of the rope.

For others it wasn’t a bird, but a dragon or a fly, a bespectacled
dolphin or John Lennon with an out-of-tune guitar. All the ropes, yes,
shook at the same time. All the gods wept and the scientist understood
the time had come. The scientist fixed his coat, adjusted his glasses,
glanced at the formula again, and then gave a little nod to all the
other scientists on a rope who nodded back to him. He knew the truth and
looked for the last time at the gods, while shaking the rope violently,
as if he wanted to get rid of the hummingbird that tightened the rope,
the dragon, the flies, and all that is left to finish creating in peace
their own multiplied universe for all time.

The hummingbird flew away.

</section>
<section epub:type="chapter" role="doc-chapter">

![](../img/alejandra-gamez_ser-being01.jpg)

![](../img/alejandra-gamez_ser-being02.jpg)

# _El ser_ de [Alejandra Gámez](997a-semblanzas.xhtml#a42) {.centrado}

Este minicómic está en el estilo muy característico de Alejandra (_The mountain with teeth_) y básicamente demuestra algo que mucha gente ya sabe: los gatos son los verdaderos amos del universo.

![](../img/TheBeing01.jpg)

![](../img/theBeing02.jpg)

# [Alejandra Gámez](997b-semblances.xhtml#a42), _The Being_  {.centrado}

# 40. El ser / The Being @ignore

A brief two-pages comic-series, in the characteristic style of its author (+++AKA+++ “The mountain with teeth”), in which cats are pretty much the main beings of the Universe.

</section>
<section epub:type="chapter" role="doc-chapter">

# El objeto más extraño en todo el espaciotiempo

# 41. El objeto más extraño en todo el espaciotiempo @ignore

## [José Luis Ramírez](997a-semblanzas.xhtml#a22) {.centrado}

Yo. Los fotones orbitan mi horizonte, debatiéndose entre escapar
centrifugados hacia el vacío en todas direcciones o dejarse caer en
cascada hasta ser parte de mí. Más allá, el plasma rueda tan veloz como
es posible, arrastrando el marco del espaciotiempo en el mismo sentido
que yo todo observo; hay un campo magnético a mi alrededor, provocado
por los electrones que se precipitan. Eso soy yo. La masa de todas esas
partículas, la suma de sus cargas electrostáticas, su momento angular.
Nada más. En mi interior no tiene importancia alguna el número y los
diferentes tipos de materia, el balance entre _quarks_ y _antiquarks_…
Todo cuanto se vierte en mí se hace uno conmigo.

Aquí el tiempo fluye en ambos sentidos, sí, es el espacio el que avanza
inexorable hacia un único punto, hacia mí, la singularidad. Incubado en
y sobre la superficie del cosmos. Soy el haz del abismo y como tal, es
menester que me pronuncie, clamar mi nombre que es infinito en el tiempo
y único en el espacio, dejar constancia de cuanto ha acontecido conmigo,
gritar: Yo soy lo que soy y hago mía la única verdad del cosmos. La
belleza infinita de la oscuridad perdida en la traducción de una
antipoesía, jeringonza, nada sino ruido aleatorio. La radiación de
Hawking visible a más de un parsec del punto que la genera,
consumiéndose en luz, masa y energía desde su origen hasta
desintegrarle.

</section>
<section epub:type="chapter" role="doc-chapter">

# The Strangest Object in the Whole Spacetime

# 41. The Strangest Object in the Whole Spacetime @ignore

## By [José Luis Ramírez](997b-semblances.xhtml#a22) \ 
Edited by [Toshiya Kamei](997b-semblances.xhtml#a53) {.centrado}

Myself. Photons orbit my horizon, hesitating between fleeing toward the
void in every direction or cascading down until they become part of me.
Beyond, plasma rolls at full speed, frame-dragging spacetime as
everything I observe. There’s a magnetic field around me, caused by
falling electrons. This is me. The mass of all those particles, the sum
of their electrostatic charges, their angular momentum. Nothing else.
Here inside me, the quantity and type of particles and the balance
between quarks and antiquarks have no importance at all. Everything
poured into me becomes one with me.

Here, time flows in both directions. Space, otherwise, advances
inexorably toward a single point, toward me, the singularity. Incubated
in and on the surface of the cosmos. I’m the face of the abyss and, as
such, I must pronounce myself, claim my name that is infinite in time
and unique in space, record what has happened to me, and shout, “I’m who
I am and the only truth of the cosmos is mine!” The infinite beauty of
darkness is lost in translation, antipoetry, gibberish, nothing but
random noise. Hawking radiation is visible more than one parsec from the
point that generates it, consuming itself into light, mass, and energy
from its origin until it disintegrates.

</section>
<section epub:type="chapter" role="doc-chapter">

# Los mundos que imaginamos

# 42. Los mundos que imaginamos @ignore

## [Antígona Segura Peralta](997a-semblanzas.xhtml#a38) {.centrado}

---_Mamá ¿qué pedirías si se te concediera lo que sea?_\
---_Creo que una casa enorme con biblioteca, una alberca, una salita de
cine…_\
---_No mamá, no entiendes, te estoy diciendo lo-que-sea_.\
---_Ah, ya. Creo que me gustaría ver los paisajes de otros planetas pero
sin tener que hacer el viaje hasta allá. Me imagino los océanos de lava,
las atmósferas de colores, las nubes de sales, los amaneceres con dos
soles…_\
---_Ya ves mamá, yo sabía que tú podías_.\
(Conversación con mi hijo cuando tenía como 8 años) {.epigrafe} 

Hasta 1995, la existencia de planetas alrededor de otras estrellas era
algo que solo habíamos imaginado y que nuestras teorías predecían. Así
como en nuestro sistema solar, alrededor de otras estrellas existirían
cuerpos a los que llamamos exoplanetas. Y es que en el proceso de
formación de las estrellas se genera una estructura que puede dar origen
a planetas. Todo comienza en unas zonas en el espacio llamadas “nubes
moleculares”. Están hechas de gas y polvo que expulsan las estrellas en
sus últimas fases de evolución. El gas contiene mayoritariamente
moléculas de hidrógeno (H<sub>2</sub>), el resto es agua, monóxido de carbono y
compuestos orgánicos, es decir, moléculas de carbono e hidrógeno. El
polvo está hecho principalmente de silicatos, una combinación de
oxígeno, silicio y otros elementos como magnesio y hierro, que es el
mismo material que hace a las rocas terrestres y que, de hecho,
constituye casi 70% de la masa de nuestro planeta. En la superficie del
polvo, dependiendo de la temperatura, pueden formarse hielos de agua,
metano, amoniaco o bióxido de carbono.

Las nubes moleculares contienen grumos que se vuelven muy masivos de
forma que comienzan a colapsarse formando un disco, al que se le llama
disco circunestelar o protoplanetario. En el centro de este disco se
forma la estrella y del resto se formarán los planetas. Como el material
del disco proviene de la nube molecular, los planetas estarán formados
de esas mismas cosas: hidrógeno, hielos, silicatos o todas las
anteriores. En nuestro sistema solar tenemos tres “sabores” de planetas;
Mercurio, Venus, Tierra y Marte, con núcleos de hierro cubiertos por un
manto y corteza de silicatos; Júpiter y Saturno, hechos de hidrógeno y
helio y un poco de otros elementos como carbono, azufre y nitrógeno;
Urano y Neptuno formados con una mezcla de hielo de agua con algo de
hidrógeno, helio y silicatos.

Los primeros exoplanetas que detectamos tenían masas muy similares a
Júpiter solo que estaban mucho más cerca de su estrella. Júpiter está
cinco veces más lejos del Sol que la Tierra, o sea a cinco unidades
astronómicas (<span class="versalitas">UA</span>) de distancia. En cambio, los primeros exoplanetas
detectados estaban a distancias menores a una UA de su estrella. Aunque
estos planetas tendrían una composición muy similar a Júpiter o Saturno,
están a temperaturas mucho más altas debido a la cercanía con su
estrella y por ello decimos que son tipo “Júpiter caliente”. Mientras
que en Júpiter y Saturno hay nubes formadas de agua y amoniaco, en los
Júpiter calientes pueden formarse nubes de hierro, silicatos o sales
como el cloruro de potasio.

Conforme las técnicas de detección de exoplanetas mejoraron, logramos
detectar planetas cada vez más pequeños. Así descubrimos que no solo
existían los Júpiter calientes sino también las Tierras calientes. Estos
son planetas con núcleos de hierro y mantos de silicatos, pero sus
temperaturas superficiales exceden los 1200 °C, así que la superficie de
silicatos se encuentra fundida y se evapora, ese gas se eleva hasta que
la temperatura del espacio lo enfría, se vuelve sólido y cae hacia el
planeta como una lluvia de polvo y rocas.

Para saber de qué están hechos los exoplanetas usamos modelos teóricos
que predicen cuál será el tamaño de un objeto que está hecho de cierto
material. Por ejemplo, un planeta hecho de hidrógeno con treinta veces
la masa de la Tierra (30 M) tendrá un radio ocho veces mayor al de la
Tierra (8 R); en cambio, si estuviera hecho solo de silicatos, con
esa masa sería unas dos veces mayor que la Tierra. En el extremo, el
material más denso disponible para hacer un planeta es el hierro, un
mundo de este material con 30 M tendría 1.8 R. Entonces, para los
casos en los que podemos medir la masa y el radio de los exoplanetas,
podemos compararlos con nuestros modelos y saber de qué están hechos…
o casi. Para los ejemplos que puse antes, las masas y radios de los
exoplanetas son suficientemente grandes o pequeñas como para podamos
asegurar que esos exoplanetas están hechos de algo poco denso como el
hidrógeno, muy denso como los silicatos y el hierro, pero no siempre es
así.

Resulta que en el sistema solar no tenemos planetas con masas o tamaños
intermedios a los de la Tierra y Neptuno (4 R, 17 M), pero entre
los exoplanetas, estos mundos, inexistentes en nuestro sistema, son lo
más comunes. Se les llama sub Neptunos y pueden estar hechos con la
combinación de todos los compuestos de los que están hechos los planetas
del sistema solar pero en proporciones muy distintas. El problema es que
nuestros modelos pueden predecir más de una composición para una masa y
radio dadas, es algo que llamamos la degeneración de la composición. Por
ejemplo, un planeta llamado GJ 1214 b (gira alrededor de la estrella
llamada GJ 1214) tiene una masa de 6.5 M y un radio de 2.7 R, de
manera que podría ser un planeta con un núcleo de hierro, manto de
silicatos y una atmósfera de H<sub>2</sub>. Esto es lo que llamamos una
Supertierra, no tiene poderes especiales, ni capa, solo es una versión
mucho más masiva de un mundo terrestre y que en vez de tener una
atmósfera de bióxido de carbono como Venus, Marte y la Tierra misma
antes de que surgiera la vida, tiene una atmósfera de H<sub>2</sub>. GJ 1214 b
también puede ser una versión pequeña de Neptuno, o sea un Minineptuno,
compuesto de roca, agua y una envoltura de hidrógeno y helio. Y hay otra
posibilidad, este exoplaneta podría ser un mundo océano. No, nada que
ver con la Tierra. Aunque nuestro planeta tiene la superficie casi
completamente cubierta de agua, la masa de este compuesto es menos del
1% del total de la masa terrestre. Nuestro mundo es en realidad una gran
piedra con una gota de agua. En cambio, si GJ 1214 b fuera un mundo
océano, tendría un núcleo de hierro, un manto de silicatos y 50% de su
masa en agua.

Hasta la fecha no hemos podido ver el paisaje de ningún exoplaneta,
nuestras observaciones solo detectan masas o radios y los modelos nos
dan crudas inferencias de sus composiciones. Nuevos instrumentos nos
permitirán descubrir más planetas y detectar si tienen atmósfera e
incluso, vida, mientras tanto, la literatura seguirá siendo nuestra
mejor aliada para contemplar desde un castillo las extensiones azules de
Caladan, el mundo océano; ver un doble amanecer en Tatooine; helarnos
recorriendo los glaciares de Gueden.

</section>
<section epub:type="chapter" role="doc-chapter">

# The worlds we imagine

# 42. The worlds we imagine @ignore

## By [Antígona Segura Peralta](997b-semblances.xhtml#a38) {.centrado}

“Mom, if you could ask for anything, what would it be?”\
“A huge house with a library, a pool, a cinema room…”\
“No mom, you didn’t understand, I’m saying anything.”\
“Oh ok. I think I would like to see other planets landscapes, but
without having to travel there. I imagine the lava oceans, the colored
atmospheres, the salt clouds, sunsets with two suns…”\
“See mom, I knew you could do it.”\
(Conversation with my son when he was 8 years old) {.epigrafe}

Until 1995, the existence of other planets surrounding other stars was
something we had only imagined and that our theories predicted. Just
like in our solar system, there would be bodies surrounding other stars,
the ones we call exoplanets. And is in the star forming process where a
structure is generated that can originate planets. It all starts in
zones in the space called “molecular clouds”. They are made of gas and
dust expulsed by the stars in their last evolution phase. The gas
contains mainly hydrogen molecules (H<sub>2</sub>), the rest is water, carbon
monoxide and organic compounds, meaning, carbon and hydrogen molecules.
The dust is made mainly of silicates, a combination of oxygen, silicon
and other elements like magnesium and iron, that is the same material
that forms the rocks on Earth and that, in fact, constitutes almost 70%
of the mass of our planet. In the dust surface, depending on the
temperature, can form ice made of water, methanol, ammoniac or carbon
dioxide.

The molecular clouds contain clumps that become too massive so they
start to collapse and forming a disc, which is called circumstellar or
protoplanetary disc. In the center of this disc the star is formed and
the planets will be formed from the remains. Like the material of the
disc that comes from the molecular cloud, the planets are formed of
these same things: hydrogen, ice, silicates or all of the above. In our
solar system we have three “flavors” of planets; Mercury, Venus, Earth
and Mars, with iron core covered by a mantle and crust both composed of
silicates; Jupiter and Saturn, made of hydrogen and helium and some
other elements like carbon, sulfur and nitrogen; and Uranus and Neptune
formed with a mix of ice made of water with a little hydrogen, helium,
and silicates.

The first exoplanets that we detected had very similar masses to
Jupiter, but they were much closer to their star. Jupiter is five times
more remote from the Sun than Earth, thatis five astronomical units (<span class="versalitas">AU</span>)
away. Instead, the first exoplanets detected were at distances shorter
than one AU of their star. Although these planets would have a very
similar composition to Jupiter and Saturn, they are at much higher
temperatures due to the closeness to their star and that’s why we say
that they are “hot Jupiters”. While in Jupiter and Saturn there are
clouds made of water and ammoniac, in the hot Jupiters the formation of
clouds of iron, silicate or salt like the potassium chloride can occur.

As the detection techniques of exoplanets improved, we’ve managed to
detect planets increasingly smaller. That’s how we discovered that apart
from hot Jupiter there were also hot Earths. These are planets with iron
nucleus and silicate mantles, but their superficial temperatures exceed
1200 °C, so the surface of the silicates melts and evaporates, that gas
rises until the temperature of the space cools it, it becomes solid and
falls back to the planet like a shower of dust and rocks.

In order to know what the exoplanets are made of we use theoretical
models that predict the size of the subject when made of certain
material. For example, a planet made of hydrogen thirty times the mass
of the Earth (30 M) would have a radius eight times greater to the
one of the Earth (8 R); instead, if it was made of silicates, with
that mass it would be two times larger than Earth, In the extreme, the
highest density material available to make a planet is iron, a world
made of this material with 30 M would have 1.8 R. Then, for the
cases in which we can measure the mass and the radius of the exoplanets,
we can compare them with our models and know of what they are made of…
or almost know. For the examples that I mentioned above, the masses and
radiuses of the exoplanets are sufficiently big or small so that we can
guarantee that the exoplanets are made of something not very dense like
hydrogen, very dense like silicates and iron, but this is not always the
case.

It turns out that in our solar system we don’t have planets with masses
or intermediate sizes to those of Earth and Neptune (4R, 17 M.),
but among exoplanets, these worlds, inexistent in our system, are the
most common. They are called sub Neptunes and they can be made of the
combination of all the compounds of what the planets of the solar system
are composed of but in different proportions. The problem is that our
models can predict more than one composition for a given mass and
radius, is something we call the degeneracy of the composition. For
example, a planet called GJ 1214 b (orbits around the star called
GJ1214) and has a mass of 6.5 M and a radius of 2.7 R, so it could
be a planet with an iron core, silicate mantle and a H<sub>2</sub> atmosphere.
This is what we called a Super-Earth, it doesn’t have any special
powers, nor cape, it’s just a much more massive version of a terrestrial
world that instead of having a carbon dioxide atmosphere like Venus,
Mars and the Earth itself before life emerged, has an atmosphere of
H<sub>2</sub>. GJ 1214 b it can also be a smaller version of Neptune, a
Mini-Neptune, composed of rock, water and a hydrogen and helium
envelope. And there is another possibility, this exoplanet could be an
ocean world. No, nothing to do with the Earth, although our planet has a
surface almost completely covered by water, the mass of this compound is
less than 1% of the total of Earth’s mass. Our world is in fact one big
rock with a drop of water. Instead, if GJ 1214 B was an ocean world, it
would have an iron core, a silicate mantle and 50% of its mass would be
water.

Thus far we haven’t been able to see the landscape of any exoplanet, our
observations only detected masses or radius and the models give us
inferences of their compositions. New instruments would allow us to
discover more planets and detect if they have an atmosphere or even
life, meanwhile, literature remains our best ally to contemplate
Caladan’s blue extension; to be able to see a double sunset in Tatooine;
or freeze while we travel the glaciers of Gethen.

</section>
<section epub:type="chapter" role="doc-chapter">

![](../img/dianita_multiversos-multiverse.jpg)

# _Multiversos_ de [Dianita](997a-semblanzas.xhtml#a45) {.centrado}

Esto está fuera de mi zona de confort porque trabajo exclusivamente con fantasía, así que está siendo interesante observar estos temas y en lugar de hacer ilustraciones puramente científicas, darles un giro. (Me arrepiento un poco de no haber perseguido más la Astronomía como quería desde peque). Ya hice este proyecto algo superpersonal para celebrar que puedo juntar ambas áreas. _Multiversos_ acabó siendo de unas bailarinas jarochas con faldas de galaxias (simulando las posibles galaxias en otros universos con vida como la nuestra). 

# [Dianita](997b-semblances.xhtml#a45), _Multiverse_ {.centrado}

# 43. Multiversos / Multiverse @ignore

This lies outside my comfort zone because I work exclusively with fantasy; as a result, it’s interesting to consider these themes and, instead of making purely scientific illustrations, give them a spin (now I kind of regret not having pursued Astronomy like I wanted to as a kid.) So I made this project something superpersonal to celebrate that I can play in both fields. “Multiverse” ended up being jarocha dancers with galaxy skirts (simulating possible galaxies in other universes with life like ours.)

</section>
<section epub:type="chapter" role="doc-chapter">

# La Orden de los Pensadores de la Singularidad

# 44. La Orden de los Pensadores de la Singularidad @ignore

## [Gerardo Horacio Porcayo](997a-semblanzas.xhtml#a17) {.centrado}

El grupo nació hermético por contexto; no pretendíamos nada ni se
plantearon reglas, solo buscamos una vía para la convivencia. Doce
especies y grupos distintos en órbita a Cygnus x-1, con la bruma
sensorial derivada de las emisiones del agujero negro que nos obligó a
fabricar estos módulos (especie de batiscafos de inmersión profunda,
nunca más allá del disco de acreción, menos hasta el horizonte de
eventos) y a nuestras naves capitanas, a abandonar las inmediaciones
para evitar un mayor deterioro en sus instrumentos.

La nuestra es una investigación de campo en el filo; es como volver al
pasado, a las viejas misiones Mercury. Una cápsula monoplaza por cada
grupo, doce satélites con el instrumental fijo en el centro de esa dona
luminosa y cargada de radiaciones. Teorizamos una órbita similar y hemos
conseguido una eficiente transmisión entre nosotros. Nuestro primer
lenguaje fue la matemática y un extraño prurito nos hizo obviar, dejar
al margen cualquier referencia a raza o planeta de origen. Nos une un
objeto de estudio: es el centro de nuestras vidas. Paulatinamente hemos
confrontado teorías, postulados; incluso filosofías de las que
erradicamos cualquier antropocentrismo (o sus variantes).

Continuar este ciclo nos ha impuesto un itinerario común: debates
formales en el primer tercio del día, debates teológicos en el segundo
y, en el tercero, la simple especulación que ha fructificado en
fantasías científicas y aventureras…

Nadie está seguro de lo que pasa afuera. Postulamos una estación
espacial en torno al único planeta gaseoso que sobrevivía cuando
arribamos y, de tiempo en tiempo, un cohete robot nos proveía de víveres
y reemplazos de instrumental.

Hace tres jornadas, la cápsula cinco fue la primera en recibir planos y
material para la ruta de escape. Luego, las demás. Conjeturamos una
guerra entre nuestros congéneres.

No retrocederemos. Decidimos seguir aquí, perpetuar esta natural Orden
de los Pensadores de la Singularidad, regresarles estos mensajes, los
datos recopilados y las filosofías acuñadas en conjunto.

Confiamos en que nada frenará nuestros suministros. Sería preferible
morir aquí que sumarnos a otra guerra absurda.

</section>
<section epub:type="chapter" role="doc-chapter">

# The Order of The Singularity Thinkers

# 44. The Order of The Singularity Thinkers @ignore

## By [Gerardo Horacio Porcayo](997b-semblances.xhtml#a17) {.centrado}

The group was born hermetic by context. We never intended anything but a
way to communicate. Twelve species and expeditions in orbit on Cygnus
X-1, under a sensory blur as a byproduct of the black hole emissions
that pushed us to develop these modules (kind of a deep diving
bathyscaphe, but never beyond the crescent-shaped emission ring nor the
event horizon) and to our captain vessels to abandon the near area to
avoid a major damage in the instruments.

Ours was a field research on the edge, it was like going back to old
Mercury Missions: a single pilot capsule for each race; twelve
satellites with their instruments fixed on the core of the high
radiation light donut. We theorized a similar orbit in our capsules and
found an efficient way of transmissions between us. Math was our first
language and then a strange susceptibility made us skip, leave behind
any reference to race or planet of origin. We’re linked by our study
object: it is the center of our very lives. Gradually we’ve confronted
many theories, postulates, even philosophies out of any anthropomorphism
(or any variant of it).

As we kept on this cycle, we built a common schedule: formal discussions
on the first part of the day, theological debate on the second part, and
plain science fiction speculation or adventures on the last third.

Nobody’s sure about what’s happening outside of this ring. We imagined
the construction of an interplanetary space station around the only gas
planet that’s left.

From time to time a rocket robot used to deliver supplies and spare
parts.

Three days ago, capsule five was the first to receive blue prints and
materials for an escape route. After that, we all received something
similar. We’ve conjectured there’s a war between our races in the
vicinity.

We’re not gonna take a step back. We chose to stay here, to perpetuate
this natural Order of the Singularity Thinkers. We’re gonna return the
rockets with these messages, the reunited data and all the philosophy
papers we achieve.

We trust that nothing will stop our supplies. Anyway, it would be better
to die in here than to be added to another absurd war.

</section>
<section epub:type="chapter" role="doc-chapter">

# Crítica literaria especulativa

# 45. Crítica literaria especulativa @ignore

## [Isaí Moreno](997a-semblanzas.xhtml#a21) {.centrado}

Todo escrito narrativo proviene de una alteración en el espacio tiempo
que, necesariamente, detona acciones, una curva dramática con ascensos y
descensos, esto es, una onda cuya propagación se desplaza por el éter
del pensamiento. Einstein predijo la existencia de ondas que viajan a la
velocidad de la luz a partir de un evento crucial entre cuerpos
altamente masivos. Un hombre escribe con el bolígrafo. A continuación se
crea una herramienta de análisis milimétrico, ajustada para detectar la
forma de la onda dramática en la historia: la onda no aparece en las
lecturas de los sensores textuales: no hay curva dramática. El hombre
regresa al papel. Retoma el bolígrafo. Tachonea todo y vuelve a empezar
en una nueva hoja blanca. Escribe desde su emoción. Escribe, a sabiendas
de que está generando desorden. Y es el desorden lo que hace explotar su
narración. Oposiciones. Conflicto. Con una gráfica simple, en el escrito
se puede notar de pronto la aparición de la onda dramática: ondulando en
el espacio, rompiendo las barreras del tiempo. El desorden ha concretado
el milagro de una historia que funciona. El desorden ha sido como un
cuerpo masivo en colisión cuyas leyes gravitatorias conforman nodos,
puntos masivos de no retorno en un espacio narrativo insospechado: caos
digno de estudio para los teóricos literarios.

Pese a que en la exposición matemática de sus teorías también había un
germen narrativo, Einstein no sospechó la existencia de una onda
dramática en la escritura.

</section>
<section epub:type="chapter" role="doc-chapter">

# Speculative Literary Criticism

# 45. Speculative Literary Criticism @ignore

## By [Isaí Moreno](997b-semblances.xhtml#a21) \
Translated by [Toshiya Kamei](997b-semblances.xhtml#a53) {.centrado}

All narrative writing comes from an alteration in space-time that,
necessarily, detonates actions, a dramatic curve with ascents and
descents that is, a wave whose propagation is displaced by the ether
of thought. Einstein predicted the existence of waves that travel at the
speed of light from a crucial event between highly massive bodies. A man
writes with his pen. Next, a millimeter analysis tool is created,
adjusted to detect the shape of the dramatic wave in the story. The wave
doesn’t appear in the readings of the textual sensors: there’s no
dramatic curve. The man returns to the paper. He picks up the pen. He
scratches off everything and starts over on a new blank sheet. He writes
from his guts. He writes, fully aware he’s creating chaos. And it’s the
chaos that makes his narrative explode. Oppositions. Conflict. With a
simple graph, suddenly the dramatic wave appears in the writing,
undulating in space and breaking the barriers of time. Chaos has
materialized the miracle of a story that works. The chaos has been like
a massive body in collision whose gravitational laws conform nodes,
massive points of no return in an unsuspected narrative space, chaos
worthy of study for literary theorists.

Although in the mathematical exposition of his theories there
was also a narrative germ, Einstein didn’t suspect the existence of a
dramatic wave in writing.

</section>
<section epub:type="chapter" role="doc-chapter">

![](../img/beli-torre_cosmos-cosmos.jpg)

# _Cosmos_ de [Beli](997a-semblanzas.xhtml#a43) {.centrado}

Una imagen que hace pensar en agujeros negros y universos paralelos. ¿Quién sería nuestro espejo en otra dimensión?

# [Beli](997b-semblances.xhtml#a43), _Cosmos_  {.centrado}

# 46. Cosmos / Cosmos @ignore

An image that suggests black holes and parallel universes. Who would it be our mirror in another dimension?

</section>
<section epub:type="chapter" role="doc-chapter">

# ¿Eureka?

# 47. ¿Eureka? @ignore

## [Blanca Mart](997a-semblanzas.xhtml#a08) {.centrado}

Albert aprovechó un descuido del científico loco y entró en su máquina
del tiempo. Sabía que no debía hacer eso. No se entra en una máquina del
tiempo así como así y menos si es de un colega, y menos si es de un
colega loco. Pero él quería conocer a Arquímedes y decir “eureka”. Pulsó
un botón y apareció en una amplia casa en Siracusa.

Allí estaba el matemático, en pleno baño, pensando mientras se bañaba,
calibrando, calculando. De pronto gritó “¡Eureka!” y salió corriendo.
Albert estudió precipitadamente sus papeles y, pleno de ciencia, pulsó
el duplicador que se había traído de su propio tiempo. De nuevo acertó y
regresó al siglo +++XX+++.

Pero no sabía en qué fecha ni en qué lugar estaba. ¡Dichosa máquina!
¿Qué era aquello?, ¿una variable…? Parecían unos estudios de cine. Se
abrió una puerta y una hermosísima mujer vestida de negro apareció
frente a él, canturreaba “Put the Blame on Mame”, una seductora canción,
mientras movía levemente su larga y rizada cabellera y se quitaba
lentamente los largos guantes.

Le miró con curiosidad.

---¿Quién eres?

---Me llamo Einstein.

Ella se acercó mucho y le sonrió.

---Yo, Gilda, en ese filme, ¿sabes…?@note

Él susurró: "Eureka". Más tarde pulsó el mismo botón casi sin darse
cuenta y apareció en su propio laboratorio.

“¡Eureka!”, repitió, el cabello electrocutado, de punta.

¡Menudo viaje en el tiempo!

Por desdicha, al gran estudioso, se le olvidaron las matemáticas.

Son cosas que ocurren si uno entra en una máquina del tiempo. Porque no
solo es el tiempo, sino los versos que cantan entre las cuerdas. {.espacio-arriba1 .sin-sangria}

</section>
<section epub:type="chapter" role="doc-chapter">

# Eureka?

# 47. Eureka? @ignore

## By [Blanca Mart](997b-semblances.xhtml#a08) \
Translated by [Toshiya Kamei](997b-semblances.xhtml#a53) {.centrado}

When the mad scientist wasn’t looking, Albert sneaked into his time
machine. He knew he shouldn’t do it. You don’t sneak into a time machine
just like that, especially if it belongs to a colleague, and more so if
he’s crazy. But Albert wanted to meet Archimedes and shout, “Eureka!”
When he pressed a button, he was transported to a large house in
Syracuse.

There the mathematician was immersed in a tub, deep in thought while he
was taking a bath. He was calibrating and calculating. All of a sudden,
he shouted, “Eureka!” and went out running. Albert hastily studied his
papers, and with full access to science, he pressed the duplicator he
had brought from his own time. He hit the button again and returned to
the twentieth century.

However, he had no idea what date it was or where he was. Damned
machine! What was that? A variable… He stood before buildings that
looked like movie studios. A door slid open, and a drop-dead gorgeous
woman in a black dress appeared in front of him. She was humming a
seductive rendition of “Put the Blame on Mame” while occasionally
tossing her long curly hair and slowly removing her long gloves.

She stared at him wide-eyed.

“Who are you?”

“My name is Albert Einstein.”

She stepped closer and smiled at him.

“I’m Gilda from that movie, you know…”@note

He mumbled, “Eureka.” Later, he pressed the same button almost without
much thought and was transported to his own lab.

“Eureka!,” he repeated, his hair standing on end, as if he was
electrocuted.

What a trip!

Unfortunately, the great scientist forgot the mathematics.

This is what happens if you sneak into a time machine. It’s not only
time, but also verses that sing between strings. {.espacio-arriba1 .sin-sangria}

</section>
<section epub:type="chapter" role="doc-chapter">

# La satisfacción lo revivió

# 48. La satisfacción lo revivió @ignore

## [Felecia Caton Garcia](997a-semblanzas.xhtml#a15) \
Traducido por [Andrea Chapela](997a-semblanzas.xhtml#a05) {.centrado}

Sebastian se levantó de su escritorio y lanzó el fajo de papeles al otro
lado de la habitación.

---¡Ya está! ---gritó en dirección del otro ocupante de la habitación:
una gata de pelaje negro y brillante que dormía sobre la impresora---.
¡Ya estoy harto!

Mientras gritaba, se jalaba el cabello y pateaba la mesa. La gata abrió
uno de sus ojos amarillos y bostezó, estaba acostumbrada a los arrebatos
de Sebastián.

Los últimos seis años, Sebastián había estado trabajando en la primera
computadora cuántica del mundo. Una de verdad, no como esas mediocres
que aparecían y luego se ahogaban con tanta información. Hasta había
pensado el nombre: _La Spukhafte 64_.

---Planck---dijo Sebastián dirigiéndose a la gata con voz más
calmada---. Planck, si logro entender esto, nunca volverás a comer Meow
Mix. Te daré pescado y solo de nivel sashimi.

Planck abrió el otro ojo y se levantó, estirándose y lanzando algunas
bolas de pelo dentro del mecanismo de la impresora (Sebastián tenía que
comprar una cada nueve meses). Entonces abrió su hocico:

---Muy bien--- dijo---, te ayudaré.

Sebastián la miró fijamente. Planck movió la cola.

Finalmente, él habló:

---Ya enloquecí, ¿verdad Planck? Supongo que los dos sabíamos que esto
sucedería, pero nunca pensé que tendría esta alucinación en particular.

---Esta no es una alucinación, Sebastián ---Planck se lamió la pata
derecha y luego se rascó detrás de la oreja---. Nunca me pareció que
valiera la pena hablar, pero estoy cansada de comer la porquería esa que
pones en mi plato que, por cierto, nunca lavas. Aunque no tengo pulgares
oponibles, este es un problema que puedo solucionar.

---¿Sabes cómo resolver la computación cuántica?

---No solo yo. Fluffy, Morris y hasta cualquier vil gato de callejón
podría resolverlo.

---Pero, ¿cómo? ---A Sebastián le daba igual si se había vuelto loco.
Estaba dispuesto a enloquecer a cambio de respuestas.

---¿Eso de nuestras nueve vidas?, ¡es una tontería! Como tú, solo
tenemos una vida. La diferencia es que tenemos acceso a nueve universos.
A veces estamos debajo de la rueda de un coche o cayendo de un tejado,
pero, al mismo tiempo, no lo estamos. Confía en mí. Te enseñaré. Mira,
acércate. Pon tu mano en mi cabeza. Así. Ahora, ráscame detrás de las
orejas. Sí, así, muy bien. Ahora, agárrate fuerte.

</section>
<section epub:type="chapter" role="doc-chapter">

# Satisfaction Brought It Back

# 48. Satisfaction Brought It Back @ignore

## By [Felecia Caton Garcia](997b-semblances.xhtml#a15) {.centrado}

Sebastian stood up from his desk and threw the stack of papers across
the room. “That’s it,” he screamed at the room’s only other occupant: a
sleek, black cat curled on top of a printer. “I’ve had it!” he yelled,
tugging on his too long hair and kicking his desk. The cat opened one
yellow eye and yawned, familiar with Sebastian’s outbursts.

For six years, Sebastian had been working on bringing the world’s first
true quantum computer online, none of those half measures that kept
sputtering to life before choking on information. He’d even thought of a
name for it: _The Spukhafte 64_. “Planck,” he said, addressing the cat
in a calmer voice, “Planck, if I can figure this out, you’ll never eat
Meow Mix again. It’s nothing but sashimi-grade albacore from here on
out.”

Planck opened her other eye and stood, stretching, tufts of fur floating
into the delicate mechanism of the printer (Sebastian bought a new one
every nine months). She opened her mouth, “Okay,” she said, “I’ll help
you.” Sebastian stared. Planck’s tail twitched.

Finally, Sebastian spoke, “I’ve finally gone mad, eh, Planck? I guess we
both knew this would happen, but I didn’t anticipate this particular
hallucination.”

“No hallucination, Sebastian.” Planck licked a front paw and rubbed her
ear. “It never seemed worthwhile before, but I’m tired of eating that
crap you put in the bowl you never wash, and, while I’ve got no
opposable thumbs, this is a problem I can solve.”

“You can solve quantum computing?”

“Me, Fluffy, Morris, the goddamn cat in the alley can solve quantum
computing.”

“But how?” Sebastian didn’t even care if he was mad. He’d trade sanity
for answers.

“Nine lives? What a joke. We have one life, just like you. Difference
is, we have nine universes. Sometimes we’re under the wheels of a car or
falling off a roof, but at the same time, we’re not. Trust me. I’ll show
you. Just come over her and put your hand on my head. That’s it. Scratch
a bit there just behind the ears. Yeah, that’s good. Now hold on.”

</section>
<section epub:type="chapter" role="doc-chapter">

# Una manzana en el Cosmos

# 49. Una manzana en el Cosmos @ignore

## [David Venegas](997a-semblanzas.xhtml#a40) {.centrado}

Una de las frutas más famosas de la historia es la manzana que, según se
cuenta, golpeó a Isaac Newton en la cabeza mientras descansaba a la
sombra de un árbol. Esta extraña musa supuestamente inspiró a Newton
para tratar de explicar la atracción que experimentan los objetos unos
por otros, misma que mantiene en órbita a los planetas, y que hace que
estas frutas caigan e interrumpan las siestas bajo los manzanos.

Con su Ley de la gravitación universal, Newton explicó la caída de los
objetos y el movimiento de los planetas que se conocían a finales del
siglo +++XVII+++, así que pasó a la posteridad como el científico que logró
darle sentido al funcionamiento del universo conocido de su tiempo, con
una teoría casi completa. Sin embargo, la parte que no logró comprender
fue la naturaleza y el origen de esa atracción que experimentaban los
cuerpos entre sí. Cómo es que los objetos producían lo que denominó
Fuerza de Gravedad fue para para él un misterio inexplicable hasta el
día de su muerte, y se mantuvo sin respuesta durante dos siglos.

A principios del siglo +++XX+++, Albert Einstein analizó el problema desde
otro punto de vista. A pesar de que los cálculos de Newton eran
correctos y describían algo conocido, el avance de la tecnología
permitió conocer el movimiento del cielo de manera más precisa y las
observaciones ya no coincidían con lo que describía a Ley de gravitación
universal. Ante este problema, Einstein se dio cuenta de que, para
entender la naturaleza de la atracción entre los cuerpos, había que
dejar de pensar que se producía por una fuerza que emanaba de una
manzana, de la Tierra o de cualquier otro objeto. En 1915, postuló la
Teoría de la relatividad general para sustituir la teoría de Newton; en
en esta nueva teoría, Einstein propone que el universo entero es como un
tejido en el que se hilan y entrelazan el espacio y el tiempo; y que la
gravedad no es una característica propia de los cuerpos, sino una
consecuencia de la interacción de estos con el tejido del universo.

Para simplificar la relatividad general, podemos pensar que el universo
es como una tela estirada muy tensa. Imaginemos luego que una bola de
boliche colocada en esta sábana es el Sol. La depresión que la bola
produce en la tela representa la distorsión que el Sol crea en el tejido
del espacio-tiempo. Ahora, si hacemos rodar una manzana por la misma
sábana, esta dará vueltas alrededor de la bola, atrapada en su
distorsión, hasta que pierda velocidad y caiga hacia el centro de la
misma. Lo mismo ocurre con los planetas alrededor del Sol, pero estos
mantienen su velocidad porque no están rodando sobre una sábana, sino
desplazándose en el vacío; así que se mantienen orbitando alrededor del
sol, atrapados en su distorsión.

Todos los objetos generan distorsiones en el tejido del Cosmos, a las
cuales se les llama campos gravitatorios. Entre más masivo sea un
objeto, más intenso será su campo y más objetos podrán quedar atrapados
en este.

Ya ha pasado más de un siglo desde la publicación de la teoría de la
relatividad y, como es de esperarse, la tecnología que nos permite
observar el universo también ha avanzado. De hecho, el desarrollo de un
tipo muy particular de detector permitió que en 2016 se corroborara una
de las predicciones más ambiciosas de Einstein. Según la relatividad
general, el tejido espacio-tiempo se distorsiona en presencia de la masa
de los cuerpos, y es además el medio por el que se pueden propagar ondas
gravitacionales cuando ocurren explosiones o colisiones, como si fueran
ondas en la superficie de un estanque. El detector LIGO se construyó
específicamente para detectar estas ondas y con él logramos observar en
2016, por primera vez, las perturbaciones en la estructura del universo
que produjeron dos agujeros negros al chocar entre sí.

Gracias a descubrimientos como éste sabemos que algunos aspectos del
universo funcionan como Einstein los predijo, aunque también hemos
observado fenómenos que nos sugieren que la teoría de la relatividad
podría tener algunos problemas.

Una de estas dificultades surgió cuando pudimos saber el valor de la
masa en conjunto de todas las estrellas de una galaxia, y determinar la
intensidad de su campo gravitatorio. De acuerdo con estos cálculos,
fundamentados en la relatividad general, las galaxias no podrían
mantener su forma solo con el campo gravitatorio de sus estrellas, pues
giran tan rápido que deberían salir disparadas y dispersarse por el
Espacio. Sin embargo, las estrellas se mantienen dando vueltas alrededor
del centro de las galaxias, formando conjuntos bien definidos. Si
queremos encontrar una explicación siguiendo al pie de la letra la
teoría de Einstein, debemos suponer que existe una gran cantidad de
materia además de las estrellas, que está aportando la masa necesaria
para que el campo gravitatorio de la galaxia sea suficientemente intenso
para mantener a las estrellas en su sitio. El problema es que esta
materia es indetectable por cualquiera de los medios que conocemos y,
por lo tanto, no tenemos manera de saber su naturaleza o identidad, así
que se le ha denominado materia oscura.

Otro problema es que, según los cálculos de la relatividad general, el
universo se debería estar expandiendo a una velocidad constante, pero
sucede que dicha expansión ocurre a un ritmo cada vez más acelerado. De
acuerdo con el trabajo de Einstein, la única manera de que esto ocurra
es que exista una fuerza de repulsión que acelera continuamente esa
expansión. Nuevamente, esta fuerza es indetectable, pues no hemos
logrado observarla y mucho menos estudiarla, así que se le da el nombre
de energía oscura.

Y así, casi por casualidad, nos encontramos de nuevo lidiando con un
problema parecido al que se enfrentó Newton hace más de 300 años.

Debido a la magnitud de su influencia, se calcula que la materia y la
energía oscuras representan en conjunto el 95% del universo; esto
equivale a decir que, si el Cosmos fuera una enciclopedia de 20
volúmenes, solo somos capaces de leer el primero, mientras que los otros
19 contienen el origen y la naturaleza de formas de materia y energía
que actualmente son un enigma para nosotros. También es posible que las
cuentas no nos cuadren porque necesitamos hacer otro cambio radical en
nuestra manera de entender el espacio-tiempo, así que hay científicos
trabajando en explicaciones alternativas como la teoría de cuerdas o la
posibilidad de los universos paralelos.

Mientras tanto, el Cosmos se sigue expandiendo, las galaxias y los
planetas siguen orbitando, y las manzanas siguen cayendo sin que sepamos
exactamente por qué.

</section>
<section epub:type="chapter" role="doc-chapter">

# An Apple in the Cosmos

# 49. An Apple in the Cosmos @ignore

## By [David Venegas](997b-semblances.xhtml#a40) \
Translated by [Valeria Becerril](997b-semblances.xhtml#a54) {.centrado}

One of the most famous fruits throughout history is the apple that,
legend has it, struck Isaac Newton in the head while he rested under a
tree. This strange muse allegedly inspired Newton to try to explain the
attraction experimented between objects, the same that keeps the planets
in orbit, and that makes these fruits fall and interrupt naps under
apple trees.

Newton explained the effects of falling objects and the movement of the
planets that were known at the end of the +++XVII+++ century, through his law
of universal gravitation, so he is to be remembered as the scientist
that managed to make sense of the workings of the known Universe at that
time, with an almost complete theory. However, the part that he wasn’t
able to understand was the nature and the origin of this attraction
between the bodies. The way in which the objects produced what he called
as gravitational force was for him an inexplicable mystery till the day
of his death, and remained without an answer during two centuries.

At the beginning of the +++XX+++ century, Albert Einstein analyzed the problem
from another point of view. Despite the fact that Newton’s calculations
were correct and described the Universe as he knew it, the technological
advances provided the knowledge of the movement of the sky in a precise
manner and the observations no longer matched with what was described by
the universal gravitational law. Facing this problem, Einstein realized
that in order to understand the nature of the attraction between the
bodies, it was necessary to stop thinking that it was produced by a
force that emanated from the apple, from the earth or from any other
object. In 1915, he postulated the general theory of relativity to
substitute Newton’s theory; in this new theory, Einstein proposes that
the entire universe is like a tapestry in which the space and time weave
and intertwine; and that gravity is not a particular feature, but a
consequence of the interaction of these with the tapestry of the
Universe.

To simplify the general relativity, we can think that the Universe is
like a tensely stretch fabric. Let’s imagine that a bowling ball placed
in the sheet is the Sun. The dip produced by the ball on the fabric
represents the distortion that the Sun creates in the tapestry of space
and time. Now, if we let an apple roll through the same sheet, it would
circle the ball, trapped in its distortion, until velocity was lost and
it fell towards the center of the same. The same occurs with the planets
surrounding the Sun, but they keep their velocity because they are not
rolling on a sheet, instead they are moving through space; so they keep
orbiting around the Sun, trapped in its distortion.

All the objects generate distortions in the tapestry of the Cosmos,
which are called gravitational fields. The more massive an object is the
more intense its gravitational field, and the more objects that can be
trapped on it.

More than a century has passed since the publication of the theory of
relativity and, as expected, the technology that allows us to observe
the universe has also advanced. In fact, the development of a very
particular detector allowed one of Einstein’s most ambitious predictions
to be corroborated in 2016. According to general relativity, the
tapestry of space and time is distorted in the presence of the masses of
the bodies, and is also the medium by which gravitational waves are able
to propagate when explosions or collisions occur, as if they were
ripples on the surface of a pond. The LIGO detector was built
specifically to detect these waves and with it we succeeded in observing
in 2016, for the first time, the disturbances in the structure of the
Universe that were made by two black holes colliding with each other.

Thanks to discoveries like this we know that some aspects of the
Universe work like Einstein predicted, although we have also watched
phenomena that suggests that the relative theory could have some
problems.

One of this difficulties emerged when we were able to know the value of
the mass in concert with all the stars in the galaxy, and determine the
intensity of its gravitational field. According with these calculations,
based on the general relativity, the galaxies shouldn’t be able to keep
their form only with the gravitation field of their stars, since they
are spinning so quickly that they should spin shoot out and disperse
through space. However, the stars keep spinning around the center of the
galaxies, creating well defined sets. If we wish to find an explanation
that follows Einstein’s theory of relativity to the letter, we must
assume that a great quantity of matter exists besides that of the stars,
which is supplying the necessary mass so the gravitational field of the
galaxy is sufficiently intense to keep the stars in their place. The
problem is that this matter is undetectable by any known means, thus, we
have no way of knowing their nature or identity, so it has been
denominated as dark matter.

Another problem is that, according to calculations of general
relativity, the Universe should be expanding at a constant velocity, but
so it happens the such expansion occurs at a rhythm progressively
faster. According to Einstein’s work, the only way that this may happen
is the existence of a repulsion force that progressively accelerates
this expansion. Again, this force is undetectable, since we haven’t been
able to observe it and much less study it, so it is given the name of
dark energy.

And thus, and almost by chance, we are again dealing with a similar
problem to the one Newton faced almost 300 years ago.

Due to the magnitude of their influence, it is calculated that the
matter and the dark energies represent jointly the 95% of the universe;
this is equivalent to saying that, if the Cosmos was a 20 volume
encyclopedia, we are only capable of reading the first volume, while the
other 19 contain the origin and the nature of the shapes of the matter
and energy that currently are an enigma to us. It is also possible that
the figures do not add up because we need another radical change in our
own way of understanding the space-time, so there are scientists working
on alternative explanations like string theory or the possibility of
parallel universes.

Meanwhile, the cosmos is still expanding, the galaxies and the planets
are still orbiting, and the apples keep falling without us truly knowing
why.

</section>
<section epub:type="chapter" role="doc-chapter">

![](../img/sara-felix_movement-movimientoespacio.jpg)

# _Movimiento a través del espacio_ de [Sara Felix](997a-semblanzas.xhtml#a51) {.centrado}

Inspirada por imágenes de ondas gravitacionales, me imaginé los colores de fondo como las ondas más grandes en el espacio, con las ondas individuales bailando en la superficie. 

# [Sara Felix](997b-semblances.xhtml#a51), _Movement through Space_ {.centrado}

# 50. Movimiento a través del espacio / Movement through Space @ignore

Inspired by images of gravitational waves I thought of the larger ripples in space as the background colors and the individual waves dancing on the surface.

</section>
