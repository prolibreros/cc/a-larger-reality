# Universo

# 35. Universo @ignore

## [Édgar Omar Avilés](997a-semblanzas.xhtml#a13) {.centrado}

## I {.centrado}

Cuando el primer astronauta asomó la cabeza fuera del universo, vio
asombrado el cadáver de la víctima con la que nos habíamos impactado.

## II {.centrado}

Cuando el primer astronauta asomó la cabeza fuera del universo, vio
asombrado cómo el universo se empezaba a desinflar.

## III {.centrado}

Cuando el primer astronauta asomó la cabeza fuera del universo, vio
asombrado la garganta de la que proveníamos como una risotada.

## IV {.centrado}

Cuando el primer astronauta asomó la cabeza fuera del universo, vio asombrado el asombro de los demás astronautas que asomaban la cabeza en los miles de universos paralelos.
