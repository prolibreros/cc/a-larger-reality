# Room 33

# 3. Room 33 @ignore

## By [Karen Chacek](997b-semblances.xhtml#a25) \
Translated by [Toshiya Kamei](997b-semblances.xhtml#a53) {.centrado}

I tell the toothless old man glued to the reception desk, “Under normal
circumstances, I’d never stay with four bamboo palms and two ivy trees,”
in a windowless room like the one I urged him to assign me. I’ve always
found those rooms depressing---the ones with jacquard curtains
simulating windows covering brick walls or hiding old heaters _the
splendor_ didn’t recognize when it left. But today isn’t a normal 
day---whoever looks toward the street will become ill by pure suggestion.
The TV announcement had advised locking yourself in a room with
at least six air-purification plants and closing the windows
to “become invisible as the danger passes by.” I only came for a
symposium on extinct languages. Now I’m stranded at the core of a
contingency of incalculable magnitude, bathed in yellow sweat from head
to toe.

The newscaster’s voice makes me nauseous. He translates the statement
someone else reads in staccato English. He repeats that the potential
benefits for public health have always been greater than the risks, that
having lifted the ban to create lethal viruses in labs in 2017 was a
responsible act. “Nature is the biggest bioterrorist, and we have to do
everything possible to stay one step ahead.” A full statement from the
president of the National Science Advisory Board for Biosecurity in the
same country that built a trinity of dazzling laboratories in this small
Latin American city forgotten by the god of satellite systems, with an
average temperature of thirty-seven degrees Celsius and a variety of
disused air-conditioning devices scattered throughout its old buildings.

The old man at the reception smiles as if he doesn’t care. He hands me
the key to room 33.

I move the plants using the moldy luggage trolley. At the end of the
corridor, my hand trembles in front of the door. “Quiet, honey,” I tell
myself. “When you were six, you were able to disappear from the world by
simply winding yourself up in a curtain. Why wouldn’t that work now?”
