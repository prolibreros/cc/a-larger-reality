# KIC 5520878 con variaciones y fuga

# 18. KIC 5520878 con variaciones y fuga @ignore

## [Martha Riva Palacio Obón](997a-semblanzas.xhtml#a29) [intercambiable por autora 1] {.centrado}

El movimiento de un péndulo [intercambiable por memoria de X] tiende a
seguir un patrón que incluye el conjunto de todos sus comportamientos
posibles. Los desplazamientos ---bucles infinitos--- de un sistema
caótico a través de sus atractores se conocen como fractales. No podemos
predecir cómo es que el aleteo de una mariposa [intercambiable por
recuerdo 1] se transformará en huracán [intercambiable por recuerdo
2], porque desconocemos las condiciones iniciales. Dentro del pulso de
una estrella puede ocultarse una segunda variación de origen
desconocido. Puede ser que el radio de dos frecuencias sea de 1.618 y
puede ser que en el momento preciso en el que me rompo los brazos, un
físico en Hawaii [intercambiable por John Learned] conecte el ritmo de
KIC 5520878 con la proporción áurea. Crecimiento Laplaciano, fractura
radial bilateral: las condiciones iniciales que desembocan en la
construcción de la historia de mi vida, comienzan mucho antes de que yo
nazca [intercambiable por variable extraña]. Proporción áurea,
anatomía cósmica. Me gusta más el San Juan Bautista de Leonardo que la
Mona Lisa [ambos cuadros intercambiables por las chanclas azules que
traía puestas el día que los vi en el Louvre]. Andar en bicicleta, la
espiral de un nautilo [intercambiable por amonita]. Mis fracturas son
también la fractura de muñeca de mi mamá cuando yo era niña y pasaba el
día [intercambiable por todos los veranos de mi infancia] en la
alberca. El calcio que vuelve al mar y la cadera de mi abuela
[intercambiable por (vacío)]. Dentro del _Contrapunto No. 17b:
Inverso_ de Bach, están contenidas todas las fugas que compondrá la
última versión del algoritmo que ahora me toma dictado [intercambiable
por autora 2]. Mis brazos rotos se mueven distinto, tienen un desfase
con el resto de mi cuerpo. Balanceo pendular de extremidades superiores,
retorno a un pasado homínido [intercambiable por proyección a un futuro
post-humano]. El movimiento de un péndulo [intercambiable por memoria
inversa de X] tiende a seguir un patrón que incluye el conjunto de
todos sus comportamientos posibles.
