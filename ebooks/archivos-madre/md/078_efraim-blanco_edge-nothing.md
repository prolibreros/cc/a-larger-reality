# On the Edge of Nothing

# 39. On the Edge of Nothing @ignore

## By [Efraím Blanco](997b-semblances.xhtml#a14) \
Translated by [Toshiya Kamei](997b-semblances.xhtml#a53) {.centrado}

The scientist glanced again at the formula that solved everything and
then again toward infinity. He was, according to his calculations, on
the edge of nothing. The only thing that kept him alive was that rope
and the table he had managed to balance on it. Above the table rested
the sheet of paper with the mathematical formula that solved the
multiverse theory. Lab simulations were quite clear: if he tightened the
rope, he would create more universes almost equal to his own; if it he
loosed it a little, some almost identical universe would be destroyed
somewhere. The only thing that balanced the rope on top of that nothing
was a tiny god who held it on the other side. The scientist and the god
were so far away they could barely see each other. Neither of them was
sure of what would happen next, because if they looked carefully, that
nothing was an infinite house of mirrors where their images were
reflected endlessly in what seemed to be the navel of each universe.
They rested, then, on the original rope that would have originated
everything.

They had a good time when a hummingbird perched on top of the rope.

For others it wasn’t a bird, but a dragon or a fly, a bespectacled
dolphin or John Lennon with an out-of-tune guitar. All the ropes, yes,
shook at the same time. All the gods wept and the scientist understood
the time had come. The scientist fixed his coat, adjusted his glasses,
glanced at the formula again, and then gave a little nod to all the
other scientists on a rope who nodded back to him. He knew the truth and
looked for the last time at the gods, while shaking the rope violently,
as if he wanted to get rid of the hummingbird that tightened the rope,
the dragon, the flies, and all that is left to finish creating in peace
their own multiplied universe for all time.

The hummingbird flew away.
