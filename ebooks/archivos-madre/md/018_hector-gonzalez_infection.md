# Infection

# 9. Infection @ignore

## By [Héctor Octavio González Hernández](997b-semblances.xhtml#a19) {.centrado}

“We have to destroy the planet.”

“Wow,” &amp;∗∗&amp; said, cautious to not anger |}}|, his teacher and
mentor. “Destroying the whole planet just because it got infected?”

“This isn’t a simple infection.” |}}| said, while approaching the
student “They have developed the exchange of services or goods by using
_currency_. It’s a potent viral infection that can lead to devastating
effects. Eight out of the ten races we have planted and analyzed ended
destroying themselves! I’m just cleaning up before it becomes a bigger
mess!”

“But…” timidly interjected &amp;∗∗&amp;

“WHAT?!” replied |}}|, visibly angry.

“This is the first time this virus has manifested with this genus. Yes,
the _currency_ has devastated other beings. However, this is a unique
opportunity to see if planet !=52 can resist this virus. Maybe we can
analyze them and learn how they can subsist.”

“Your optimism is quite amusing.” scoffed |}}|. “If I remember
correctly, planet !=52 is quite isolated, correct?”

“It’s a third planet on a separate sector, quite young, and can be
easily fenced with an asteroid belt if things go sour.”

“&amp;∗∗&amp;, although the idea is ridiculous, your track record is
admirable. We’ll let this infection go for 5 cycles and after that,
reevaluate.”

“Thank you!” said &amp;∗∗&amp;, “I’m quite optimistic about it.”

“I recommend you to be cautiously optimistic. Hope is easy to plant but
hard to harvest.”

∗∗∗ {.centrado .espacio-arriba1}

“Can you give me an update on planet !=52?” A slight sense of
satisfaction crossed |}}|’s mind. {.espacio-arriba1 .sin-sangria}

“Well. The situation is not optimal. The dominant species managed to
resist the _currency_ virus effects and thrived under it for about 2.47
cycles. However, it mutated and has led to a downward spiral for the
whole planet. I truly had high hopes for this cold blooded species.”

“Proceed with cleansing the system using Asteroid 10 event. That should
be enough to destroy it.”

The mentor left the pupil to finish the task. However, &amp;∗∗&amp; changed
the asteroid level to 7. Not enough to obliterate planet !=52 but with
enough power to give it a second chance. &amp;∗∗&amp; had a lot of hope for a
late harvest.
