# Artificial Extinction

# 20. Artificial Extinction @ignore

## By [Smok](997b-semblances.xhtml#a36) {.centrado}

<script type="text/javascript" src="../js/poetry.js"></script>

Don’t blame me for this, {.poesia}

in your black waters I have washed my hands. {.poesia}

It’s true, I hung them while they were sleeping, {.poesia}

I left them without heaven, without sustenance, {.poesia}

I allowed the fire to embrace them. {.poesia}

But I also pretended not to see them when I passed by, {.poesia}

when they were hiding from my ashes, {.poesia}

from my eternal kiss. {.poesia}

And only the strong ones created their own path. {.poesia}

Slowly, changing faces, {.poesia}

feathers and scales. {.poesia}

Then you opened your eyes, {.poesia}

you invented the stone, {.poesia}

you tested the blood. {.poesia}

You stole my sickle {.poesia}

and you broke the clock. {.poesia}

You were faster than evolution, {.poesia}

you were hungrier than the devourer. {.poesia}

You advanced the day of judgment for the innocent, {.poesia}

for those who had no voice to defend themselves. {.poesia}

So they entered the list of the lost ones, {.poesia}

and you prepared their pedestal with pride in the museums. {.poesia}

But don’t be sorry when you no longer see your {.poesia}

beauty reflected in the waters. {.poesia}

Use those tears for the earth to flourish. {.poesia}

You can be the judge, {.poesia}

but not the victim. {.poesia}

If you want to play God, {.poesia}

You have to learn to give life. {.poesia}
