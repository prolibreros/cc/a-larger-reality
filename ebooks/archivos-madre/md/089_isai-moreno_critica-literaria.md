# Crítica literaria especulativa

# 45. Crítica literaria especulativa @ignore

## [Isaí Moreno](997a-semblanzas.xhtml#a21) {.centrado}

Todo escrito narrativo proviene de una alteración en el espacio tiempo
que, necesariamente, detona acciones, una curva dramática con ascensos y
descensos, esto es, una onda cuya propagación se desplaza por el éter
del pensamiento. Einstein predijo la existencia de ondas que viajan a la
velocidad de la luz a partir de un evento crucial entre cuerpos
altamente masivos. Un hombre escribe con el bolígrafo. A continuación se
crea una herramienta de análisis milimétrico, ajustada para detectar la
forma de la onda dramática en la historia: la onda no aparece en las
lecturas de los sensores textuales: no hay curva dramática. El hombre
regresa al papel. Retoma el bolígrafo. Tachonea todo y vuelve a empezar
en una nueva hoja blanca. Escribe desde su emoción. Escribe, a sabiendas
de que está generando desorden. Y es el desorden lo que hace explotar su
narración. Oposiciones. Conflicto. Con una gráfica simple, en el escrito
se puede notar de pronto la aparición de la onda dramática: ondulando en
el espacio, rompiendo las barreras del tiempo. El desorden ha concretado
el milagro de una historia que funciona. El desorden ha sido como un
cuerpo masivo en colisión cuyas leyes gravitatorias conforman nodos,
puntos masivos de no retorno en un espacio narrativo insospechado: caos
digno de estudio para los teóricos literarios.

Pese a que en la exposición matemática de sus teorías también había un
germen narrativo, Einstein no sospechó la existencia de una onda
dramática en la escritura.
