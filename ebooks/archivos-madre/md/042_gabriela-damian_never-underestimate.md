# Never Underestimate a Sleeping Woman

# 21. Never Underestimate a Sleeping Woman @ignore

## By [Gabriela Damián Miravete](997b-semblances.xhtml#a16) \
Translated by [Toshiya Kamei](997b-semblances.xhtml#a53) {.centrado}

The village was ready for the eruption of the Popocatépetl volcano, the
warrior, which had been adorned for more than one hundred years with its
steam plume, exhaling its bad humor under its breath; it made a
statement with occasional pyrotechnics, juggling in the air a few molten
rocks and rather thin spurts of lava, expelled with the pleasant purpose
of frightening. And it worked: it scared people from near and far.

Preparations took decades: the houses along the slopes of the volcano
became empty fruit peels, and the towns moved to create a new periphery,
like beads on a necklace. The national park made a contingency plan for
the fauna; they studied where owls, hummingbirds, woodpeckers and
vultures would fly, which routes cacomistles, opossums, raccoons,
lynxes, coyotes, armadillos, and fawns would flee to, what branches or
stems painted lady butterflies would cling to, where lizards would
climb, and where serpents and rattlesnakes would hide. We couldn’t
afford to lose them. Here and there sophisticated burrows,
case-basements, small refuges, and high iridium posts were built so that
they could observe, like everyone else, the passage of fire, and the
fearsome breath of lahars.

We were ready. After shaking the ground, belching volcanic gas, and
spitting magma, the volcano held its breath, and people took shelter in
their new anti-seismic little sheds and waited, watching from their
screens and rooftops.

But no one had thought Iztaccíhuatl volcano, the Sleeping Woman watched
by the warrior, could wake up. Its chimneys had been filling of magma
little by little. So much was expected of the warrior that she has been
forgotten. Iztaccíhuatl got up early: the stars were still shining when
the snow was cast aside as if, with a kick, the sheets were thrown to
reveal her feet. The explosions gleamed on the purple sky, and the
rivers of viscous red-orange lava crawled down the slopes like dragons
of pure fire. Volcanic ash clouds wove spidery cobwebs of lightning and
thunder.

For some reason, our fear changed. We saw the eruption not as hell, but
as a future miracle. Iztaccíhuatl didn’t want to scare anyone. She
simply needed to create new land. And like all women who wake up, she
transformed the landscape.
