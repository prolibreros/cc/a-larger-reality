# Chicle bomba

# 8. Chicle bomba @ignore

## [Gerardo Sifuentes](997a-semblanzas.xhtml#a18) {.centrado}

Un beso para recordar todo. Dos, para asegurarse de que saliera bien.
Eso le dijo. En el primero le pasó el chicle con la lengua y, al
mascarlo, ocurrió lo que creía imposible. La carga se le subió a la
cabeza en apenas segundos. Al principio creyó que todo seguía igual,
pero observó la pantalla de la _tablet_, se puso a contestar el examen y
entonces se dio cuenta. Las fórmulas adquirieron lógica, las ecuaciones
dejaron de ser jeroglíficos. Empezó a comprender aquel lenguaje que le
estaba prohibido. Por primera vez en el año escolar sintió que había
valido la pena aprender todo aquello. Analizó cada pregunta, fue hasta
el último de los rincones de su cerebro y echó a andar la maquinaria
hasta dar con las respuestas. Siempre supo cómo hacerlo, solo faltaba
quien le trazara la ruta. Los virus modificados y en hibernación dentro
del acetato de polivinilo fueron chispas en el polvorín de sus neuronas.
Mascó hasta que la goma perdió su sabor. En la tarde el efecto fue
desvaneciéndose y vino la fiebre. Pero en cuestión de horas y tras una
siesta su organismo quedó como nuevo. Después se enteró de que los besos
no eran necesarios para suministrar la dosis.

