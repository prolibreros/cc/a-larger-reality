# Satisfaction Brought It Back

# 48. Satisfaction Brought It Back @ignore

## By [Felecia Caton Garcia](997b-semblances.xhtml#a15) {.centrado}

Sebastian stood up from his desk and threw the stack of papers across
the room. “That’s it,” he screamed at the room’s only other occupant: a
sleek, black cat curled on top of a printer. “I’ve had it!” he yelled,
tugging on his too long hair and kicking his desk. The cat opened one
yellow eye and yawned, familiar with Sebastian’s outbursts.

For six years, Sebastian had been working on bringing the world’s first
true quantum computer online, none of those half measures that kept
sputtering to life before choking on information. He’d even thought of a
name for it: _The Spukhafte 64_. “Planck,” he said, addressing the cat
in a calmer voice, “Planck, if I can figure this out, you’ll never eat
Meow Mix again. It’s nothing but sashimi-grade albacore from here on
out.”

Planck opened her other eye and stood, stretching, tufts of fur floating
into the delicate mechanism of the printer (Sebastian bought a new one
every nine months). She opened her mouth, “Okay,” she said, “I’ll help
you.” Sebastian stared. Planck’s tail twitched.

Finally, Sebastian spoke, “I’ve finally gone mad, eh, Planck? I guess we
both knew this would happen, but I didn’t anticipate this particular
hallucination.”

“No hallucination, Sebastian.” Planck licked a front paw and rubbed her
ear. “It never seemed worthwhile before, but I’m tired of eating that
crap you put in the bowl you never wash, and, while I’ve got no
opposable thumbs, this is a problem I can solve.”

“You can solve quantum computing?”

“Me, Fluffy, Morris, the goddamn cat in the alley can solve quantum
computing.”

“But how?” Sebastian didn’t even care if he was mad. He’d trade sanity
for answers.

“Nine lives? What a joke. We have one life, just like you. Difference
is, we have nine universes. Sometimes we’re under the wheels of a car or
falling off a roof, but at the same time, we’re not. Trust me. I’ll show
you. Just come over her and put your hand on my head. That’s it. Scratch
a bit there just behind the ears. Yeah, that’s good. Now hold on.”
