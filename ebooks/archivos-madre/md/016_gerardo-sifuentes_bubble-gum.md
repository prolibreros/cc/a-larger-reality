# Bubble Gum

# 8. Bubble Gum @ignore

## By [Gerardo Sifuentes](997b-semblances.xhtml#a18) \
Translated by [Toshiya Kamei](997b-semblances.xhtml#a53) {.centrado}

A kiss to remember everything. Two, to make sure it goes well. You told
me that. With the first kiss, you passed me the gum with your tongue.
When I chewed it, what I thought was impossible happened. The load went
to my head in just seconds. At first, I thought everything was the same,
but I looked at the tablet screen, began to answer the exam, and then
realized. As the formulas made sense, the equations stopped being
hieroglyphic. I began to understand that language off-limits to me. For
the first time in the school year, I felt it was worth learning all
that. I analyzed each question, went to the last corner of my brain, and
turned on the machinery until I found answers. I always knew how to do
it, just missing who would trace the route. The modified and hibernating
viruses within polyvinyl acetate sparked the powder keg of my neurons. I
chewed until the gum lost its flavor. In the afternoon, the effect was
fading and the fever came on. But in a matter of hours and after a nap,
I was like new. Later I learned the kisses weren’t necessary to deliver
the dose.
