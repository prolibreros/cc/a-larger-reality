# La teoría de Pablo

# 38. La teoría de Pablo @ignore

## [Agustín Fest](997a-semblanzas.xhtml#a01) {.centrado}

Perseguimos a la sospechosa, tiene biología aumentada. Soy una novata,
me apellido Pablo. Sigo a la teniente. La teniente dispara su escopeta,
el monstruo cae. Según las clases de sensibilización, no deberíamos
llamarle monstruo, pero nadie tiene que saber lo que ocurre en mi
cabeza. Todavía no. Cuando ascienda a oficial tendré más cuidado. Quiero
acercarme a ver, pero mi superior alza el brazo y lo impide. “No
entiendes ---dice la teniente---, al parecer no somos gente, somos otra
cosa”. Habla incoherencias, quizás debería reportarla. La sospechosa
suelta un alarido. “Nos está comiendo, Pablo. Está en los sonidos. Está
consumiendo el ruido de nuestras cabezas”. ¿Cómo nos está comiendo,
Pablo?, quiero preguntarle, pero se desata un terremoto, las luces
parpadean, las paredes se rompen.

La sospechosa abre la boca y escupe sangre. Se ha modificado tanto que
es una deformidad, humanidad negada y abandonada. Suelta una última
mirada, sonríe herida y enigmática. Soy la teniente Pablo, suelto el
arma, me duele el hombro por el empujón del disparo. La novata recoge el
arma porque teme el regaño de nuestros superiores, teme que lean su
cabeza así como definitivamente leerán la mía por ser una oficial. “Qué
necia eres, no se trata de eso”. Nada de esto está bien, la novata no ha
entendido que compartimos una historia, que la sospechosa tiene algo
extraño. Me arrodillo, de pronto me siento muy cansada. ¿Es una
simulación de la academia? ¿Un ejercicio?

El dolor se extiende. Me desangro. ¿Cuándo regresó el disparo? La novata
se acerca porque mi grito la preocupa pero yo quiero reír. No puedo,
pero quiero hacerlo. No veo a la teniente, quizá ya se ha ido. Creo que
ya estoy comprendiendo: somos un _jingle_, el eterno retorno. Mis
piernas están deshechas. Pablo alza la escopeta, porque tiene un poco de
piedad, y yo no puedo explicarle que rechace las enseñanzas académicas,
porque eso podría sacarnos de la repetición. Me falta boca, solo tengo
ojos, unos cuantos dedos y algunas extremidades inútiles. Miro el cañón
de la escopeta a unos centímetros de mi cabeza. Soy Pablo, el monstruo.
Soy crimen.
