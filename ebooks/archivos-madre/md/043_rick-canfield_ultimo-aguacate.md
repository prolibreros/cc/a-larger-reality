# Quitzacuia Ahuácatl (El último aguacate)

# 22. Quitzacuia Ahuácatl (El último aguacate) @ignore

## [Rick Canfield](997a-semblanzas.xhtml#a35) {.centrado}

Trillones de estrellas inundan la atmósfera, nacen a miles de millones
de años de distancia por los restos de una supernova explosiva que
alcanza su cenit cósmico. La tormenta cuántica se acerca.

Sin saberlo, envejecido por su antimateria, Jaime Rodríguez se sienta
entre las sombras fantasmales de las torres que se proyectan en Tribeca
sobre una cafetería que mira hacia el río Hudson, que se acerca.

Alguna vez el paisaje sonoro de dos millones de historias y emociones
sucediendo a la vez, Manhattan es ahora un lugar solemne.

Mientras la lluvia golpea la fachada, un camarero prepara su última
comida, mientras los pensamientos de Jaime están en otra parte, añorando
productos frescos.

A pesar de su rústico aspecto español, bebe sorbos de chocolate caliente
importado como una oda a los reales ancestros nahuas, ya que sus padres
son agricultores migrantes; el padre de México y la madre de Filipinas,
que se reunieron en el calor de los campos de California.

El concreto en mal estado separa el alma vieja de las capas de suelo
empañado, cedido hace medio siglo a la Compañía de las Indias
Occidentales, la primera de muchas compañías que traicionaron acuerdos
de paz mediante la masacre de personas de la tribu Lenape.

Surgió Nueva Ámsterdam, separando a los habitantes de Manhattan
originales con una palizada ahora conocida como Wall Street,
convirtiéndose en el principal mercado de la trata de esclavos humanos
africanos y amerindios.

De manera similar, sus colegas acumularon vastas fortunas ---del trabajo
de otros--- en bóvedas digitales, ahora inútilmente selladas sin
electricidad ni internet. Sus inversiones fueron ecológicas.

Un viejo amor, una bella genetista brasileña llamada Carolina, se une a
él como un bálsamo; trae su propio brillo al café iluminado por la luz
de las velas.

Ella quería casarse. Si combinaran su herencia genética, serían la
culminación de mil tribus de la Tierra, muchas ya olvidadas como
estrellas moribundas.

Se escuchan las olas rompiendo sobre Broadway. Él contempla ciertos
errores que frenan la marea, pero nada más importa, excepto este
momento.

Los socios dividen los gastos por su inestimable reliquia tomada por
corsarios, mercenarios contratados por antiguos rivales.

Finalmente, el momento llega, mirándola a los ojos con tristeza, Jaime
entrega las llaves de su reino junto con él mismo.

Un reflejo de los agujeros negros en colisión y su danza eterna, lo
llaman _Quitzacuia Ahuácatl_, el hijo y heredero de su colonia de
estrellas regresa.
