![](../img/alejandra-espino_deshilada-frayed01.jpg)

![](../img/alejandra-espino_deshilada-frayed02.jpg)

![](../img/alejandra-espino_deshilada-frayed03.jpg)

# _Deshilada_ de [Alejandra Espino](997a-semblanzas.xhtml#a41) {.centrado}

Del proceso, creo que me inspiraron principalmente el texto de [_La Física pende de una cuerda_](http://www.comoves.unam.mx/numeros/articulo/108/la-fisica-pende-de-una-cuerda), y el [video](https://www.youtube.com/watch?v=kpJ51h7bi8g) en el que Brian Cox habla con Leonard Susskind sobre multiversos. La idea de dimensiones ocultas, y de que si de alguna manera pudiéramos curvar o alterar nuestra realidad se abren posibilidades infinitas (muchas que no pueden ni siquiera ser), esta idea de curvar y de tensiones la pensé siempre como una danza, y visualmente la uní no sé porqué mecanismos de mi cabeza con el Ballet Triádico de Oskar Schlemmer, de donde se inspira el vestuario de la personaja, ya sabes, me gusta moverme muy en los años veinte.

# [Alejandra Espino](997b-semblances.xhtml#a41), _Frayed_  {.centrado}

# 36. Deshilada / Frayed @ignore 

For the process, I think I was mainly inspired by a physics text [hanging by a string](http://www.comoves.unam.mx/numeros/articulo/108/la-fisica-pende-de-una-cuerda), and the [video](https://www.youtube.com/watch?v=kpJ51h7bi8g) in which Brian Cox talks with Leonard Susskind about multiverses. The idea being one of hidden dimensions, that if we could somehow curl or alter our reality, infinite possibilities open up (many that cannot even be.) I always imagined this curving and tension as a dance, and visually my head blended that via who-knows-what mechanisms with Oskar Schlemmer’s Triad Ballet, from which the costume of the character is inspired. As you may guess, I like very much to inhabit a roaring 20’s space.
