# Varias hipótesis para explicar la ausencia de restos de dinosaurios inmediatamente debajo de la capa de residuos geológicos conocida como Límite K/Pg, que se tiene como evidencia del impacto en la Tierra del asteroide de Chicxulub y la extinción masiva de formas de vida terrestre al final del periodo Cretácico, pero plantea aún este curioso enigma, que podría expresarse como la ausencia aparente de dinosaurios durante años antes del momento fatal, y entonces uno dice ¿dónde estaban los dinosaurios?, ¿qué estaban haciendo los dinosaurios?, ¿tenían algo mejor que hacer que esperar su fin entre llamas y sufrimientos espantosos?, porque yo no los culparía si lo hubiesen tenido, claro

# 30. Varias hipótesis para explicar la ausencia de restos de dinosaurios inmediatamente debajo de la capa de residuos geológicos conocida como Límite K/Pg, que se tiene como evidencia del impacto en la Tierra del asteroide de Chicxulub y la extinción masiva de formas de vida terrestre al final del periodo Cretácico, pero plantea aún este curioso enigma, que podría expresarse como la ausencia aparente de dinosaurios durante años antes del momento fatal, y entonces uno dice ¿dónde estaban los dinosaurios?, ¿qué estaban haciendo los dinosaurios?, ¿tenían algo mejor que hacer que esperar su fin entre llamas y sufrimientos espantosos?, porque yo no los culparía si lo hubiesen tenido, claro @ignore

## [Alberto Chimal](997a-semblanzas.xhtml#a02) {.centrado}

1. El impacto del asteroide ocultó la evidencia de otra extinción: la
que provocaron las misteriosas bacterias carnívoras a las que los
científicos del siglo +++XXVIII+++, si es que no nos extinguimos antes,
darán el nombre popular de _espirilos salvajes_. Siglos antes de que
llegara el asteroide, los espirilos eran ya una amenaza: no solo
mataban de fiebre a las criaturas de células eucariotas (en especial
a los dinosaurios), sino que se comían sus cuerpos, de los que no
quedaban ni los huesos. El asteroide mató a aquellos asesinos
invisibles, que se quemaron en las olas de fuego, y a casi la
totalidad de las criaturas que aún no se convertían en sus víctimas.
Después, mucho tiempo después, llegamos nosotros a excavar en la
tierra.
2. El impacto del asteroide ocultó la evidencia de otra extinción: la
que provocaron las criaturas diamantinas que ni en el siglo +++XXVIII+++
tendrán nombre (y si nos extinguimos antes, menos todavía). Anidaban
en el fondo de las más oscuras cavernas y de los abismos marinos,
pues preferían la oscuridad y las altas presiones. Vastas,
brillantes, de piel durísima y de fuerza descomunal, no más
inteligentes que los tiburones, salían volando hasta grandes
alturas, se dejaban caer y arrebataban por el aire a los
dinosaurios, pobres lagartos atribulados, para luego comérselos en
sus guaridas remotas. (Cuando cayó el asteroide en Chicxulub, el
estallido mató a casi todas las criaturas diamantinas, pero algunas
salieron expulsadas de la atmósfera terrestre ---sus cuerpos eran
enormemente, resistentes y además podían entrar en estado de
latencia, envolverse en un capullo hermético y resistir años sin
aire ni alimento--- y fueron a dar, tras un largo viaje, al interior
de la atmósfera de Júpiter, aún más oscuro y oprimente que su
antiguo hogar, donde son felices hasta hoy). {.espacio-arriba1}
3. El impacto del asteroide fue previsto por los videntes de la cultura
dinosáurica, de grandes cerebros y colmillos, y brazos pequeñitos
porque su ámbito no era el del mundo material (hoy los llamamos
tiranosaurios). Lo vieron venir y, como aquella comunidad de
especies inteligentes tenía mucho de precognición pero muy poco de
telequinesis ---o de herramientas de cualquier tipo para desviar
asteroides---, decidieron cometer suicidio colectivo. Y lo hicieron
en el que, para ellos, fue el día Flor de las Humildades del
Lenguaje del año 8 576 234 del Calendario Faminófeno, y las colinas
y ciudades de su mundo se quedaron en silencio por siglos, hasta que
les llegó la hora de la ruina y del fuego. {.espacio-arriba1}
4. El impacto del asteroide fue previsto por un científico iguanodonte,
Ssssumorssr del Cailano Yarepsíl, que de inmediato avisó al Consejo
General de Ancianos, el cuerpo rector de los más esclarecidos
dinosaurios. Con la sensatez que era habitual entre ellos,
discutieron las posibilidades al alcance de aquella civilización
racional, tecnológica e inmensamente antigua; concluyeron que el
asteroide no podía ser detenido, pero sí era posible, en cambio,
protegerse de las peores consecuencias de la catástrofe. Por lo
tanto, en el día Plenitud Imaginada de las Criaturas del año 82dce9
del Calendario Crivistorno, comenzó el magno proyecto: la excavación
de los grandes túneles y la construcción de las ciudades
subterráneas, en las que en apenas otro año pudieron refugiarse de
la ruina y el fuego todos los dinosaurios inteligentes, y donde son
felices hasta hoy porque ustedes no se pueden imaginar la belleza de
los jardines, las paredes pintadas, los soles artificiales que los
alumbran, ni el refinamiento y bondad de sus máquinas, que los
mantienen vivos e iguales sin arrebatarles la dignidad ni uncirlos a
labor injusta. {.espacio-arriba1}
5. El impacto del asteroide no fue previsto por nadie. No había quien
pudiera preverlo porque los dinosaurios no eran inteligentes. ¡Qué
tontería hablar de especies de dinosaurios inteligentes! En cambio,
siglos antes, hubo un resplandor en Chicxulub, en un llano que hoy
está sumergido: un círculo de luz que hubiera recordado los
“portales” que se ven las películas de Hollywood a cualquiera que
hubiese visto una, lo cual significa que no inquietó a ninguna
criatura. Por el portal (sí era un portal) entraron poco a poco los
camiones no tripulados, los robots y los drones voladores de la
empresa de cárnicos, todos con las armas en ristre, todos
obedientes. Y comenzó la masacre centenaria, violenta de todas las
criaturas que tuvieron a su alcance, y cuyos cuerpos muertos eran
cortados allí mismo, convertidos en paquetes, puestos en
refrigeración para ser llevados al siglo +++XXVIII+++ (¡de hecho era una
máquina del tiempo!), en el que ya es historia antigua la
destrucción permitida o propiciada por demagogos y oligarcas, pero
éstos ya se aburrieron de carne de esclavo, y por fin han terminado
de exterminar al resto de la raza humana, y quieren platillos
nuevos. {.espacio-arriba1}
6. El impacto del asteroide no fue previsto por nadie. En cambio,
siglos antes, hubo un resplandor en Chicxulub, en un llano que hoy
está sumergido: un círculo de luz que hubiera recordado los
“portales” que se ven las películas de Hollywood a cualquiera que
hubiese visto una, y por el cual entraron los pastores entrenados,
los cazadores y entrenadores que se llevaron a millones de
criaturas. Solo querían grabarlas en video para la escena culminante
de _Parque Jurásico 143: la Venganza del Regreso - Conclusión -
Segunda Vuelta_, que debía ser más impresionante que la de _Nuevos
Nuevos Vengadores - Fin de Partida 4 - Capítulo Por Fin Final_,
porque ya se había acabado el presupuesto asignado a la animación
digital… Pero una vez en el presente humano, los dinosaurios
resultaron ser notoriamente difíciles de controlar, se apoderaron de
las instalaciones que mantenían funcionando la máquina del tiempo y
se dedicaron, a partir de entonces, a cazar y comerse a aquellas
criaturas pequeñas y frágiles. {.espacio-arriba1}
7. El impacto del asteroide fue impedido, de hecho, por las enormes
máquinas de guerra espacial de los dinosaurios, que ya habían
vencido a las arañas de Marte y a las bestias diamantinas del
corazón de Júpiter. Así, las culturas de los dinosaurios perduraron
hasta el día de hoy, en el que sus demagogos y oligarcas las dominan
mediante el engaño y el odio, los dsungarípteros cometen genocidio
contra los pteranodontes, la catástrofe ecológica compite con la
nuclear a ver cuál ocurre primero y todos se están volviendo locos,
despacio, mientras sus comunidades se disuelven en odios ínfimos y
discusiones inútiles. Una científica aragosauria, llamada
Rrrromusrrs de la Clíspera Yonalia, ha inventado una máquina del
tiempo: una nave espaciotemporal que puede atravesar sin esfuerzo
las cuatro dimensiones, y tiene un plan desesperado: viajar al
tiempo previo al impacto e _impedir_ que las máquinas de guerra
espacial hagan su labor. Liberará una nube de espirilos salvajes en
la atmósfera de cada una. No la detendrán esas muertes. Ni las
otras. Que choque el asteroide. Que todo se acabe, porque la muerte
y el olvido son mejores que esto. (Tal vez, cumplida su labor, pueda
rescatar a algunos cuantos de sus remotos antepasados y llevárselos
en su nave, a la que ha llamado _Nada_, hacia otra parte, otro
futuro distinto). {.espacio-arriba1}
