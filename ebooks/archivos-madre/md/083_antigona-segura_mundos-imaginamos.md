# Los mundos que imaginamos

# 42. Los mundos que imaginamos @ignore

## [Antígona Segura Peralta](997a-semblanzas.xhtml#a38) {.centrado}

---_Mamá ¿qué pedirías si se te concediera lo que sea?_\
---_Creo que una casa enorme con biblioteca, una alberca, una salita de
cine…_\
---_No mamá, no entiendes, te estoy diciendo lo-que-sea_.\
---_Ah, ya. Creo que me gustaría ver los paisajes de otros planetas pero
sin tener que hacer el viaje hasta allá. Me imagino los océanos de lava,
las atmósferas de colores, las nubes de sales, los amaneceres con dos
soles…_\
---_Ya ves mamá, yo sabía que tú podías_.\
(Conversación con mi hijo cuando tenía como 8 años) {.epigrafe} 

Hasta 1995, la existencia de planetas alrededor de otras estrellas era
algo que solo habíamos imaginado y que nuestras teorías predecían. Así
como en nuestro sistema solar, alrededor de otras estrellas existirían
cuerpos a los que llamamos exoplanetas. Y es que en el proceso de
formación de las estrellas se genera una estructura que puede dar origen
a planetas. Todo comienza en unas zonas en el espacio llamadas “nubes
moleculares”. Están hechas de gas y polvo que expulsan las estrellas en
sus últimas fases de evolución. El gas contiene mayoritariamente
moléculas de hidrógeno (H<sub>2</sub>), el resto es agua, monóxido de carbono y
compuestos orgánicos, es decir, moléculas de carbono e hidrógeno. El
polvo está hecho principalmente de silicatos, una combinación de
oxígeno, silicio y otros elementos como magnesio y hierro, que es el
mismo material que hace a las rocas terrestres y que, de hecho,
constituye casi 70% de la masa de nuestro planeta. En la superficie del
polvo, dependiendo de la temperatura, pueden formarse hielos de agua,
metano, amoniaco o bióxido de carbono.

Las nubes moleculares contienen grumos que se vuelven muy masivos de
forma que comienzan a colapsarse formando un disco, al que se le llama
disco circunestelar o protoplanetario. En el centro de este disco se
forma la estrella y del resto se formarán los planetas. Como el material
del disco proviene de la nube molecular, los planetas estarán formados
de esas mismas cosas: hidrógeno, hielos, silicatos o todas las
anteriores. En nuestro sistema solar tenemos tres “sabores” de planetas;
Mercurio, Venus, Tierra y Marte, con núcleos de hierro cubiertos por un
manto y corteza de silicatos; Júpiter y Saturno, hechos de hidrógeno y
helio y un poco de otros elementos como carbono, azufre y nitrógeno;
Urano y Neptuno formados con una mezcla de hielo de agua con algo de
hidrógeno, helio y silicatos.

Los primeros exoplanetas que detectamos tenían masas muy similares a
Júpiter solo que estaban mucho más cerca de su estrella. Júpiter está
cinco veces más lejos del Sol que la Tierra, o sea a cinco unidades
astronómicas (<span class="versalitas">UA</span>) de distancia. En cambio, los primeros exoplanetas
detectados estaban a distancias menores a una UA de su estrella. Aunque
estos planetas tendrían una composición muy similar a Júpiter o Saturno,
están a temperaturas mucho más altas debido a la cercanía con su
estrella y por ello decimos que son tipo “Júpiter caliente”. Mientras
que en Júpiter y Saturno hay nubes formadas de agua y amoniaco, en los
Júpiter calientes pueden formarse nubes de hierro, silicatos o sales
como el cloruro de potasio.

Conforme las técnicas de detección de exoplanetas mejoraron, logramos
detectar planetas cada vez más pequeños. Así descubrimos que no solo
existían los Júpiter calientes sino también las Tierras calientes. Estos
son planetas con núcleos de hierro y mantos de silicatos, pero sus
temperaturas superficiales exceden los 1200 °C, así que la superficie de
silicatos se encuentra fundida y se evapora, ese gas se eleva hasta que
la temperatura del espacio lo enfría, se vuelve sólido y cae hacia el
planeta como una lluvia de polvo y rocas.

Para saber de qué están hechos los exoplanetas usamos modelos teóricos
que predicen cuál será el tamaño de un objeto que está hecho de cierto
material. Por ejemplo, un planeta hecho de hidrógeno con treinta veces
la masa de la Tierra (30 M) tendrá un radio ocho veces mayor al de la
Tierra (8 R); en cambio, si estuviera hecho solo de silicatos, con
esa masa sería unas dos veces mayor que la Tierra. En el extremo, el
material más denso disponible para hacer un planeta es el hierro, un
mundo de este material con 30 M tendría 1.8 R. Entonces, para los
casos en los que podemos medir la masa y el radio de los exoplanetas,
podemos compararlos con nuestros modelos y saber de qué están hechos…
o casi. Para los ejemplos que puse antes, las masas y radios de los
exoplanetas son suficientemente grandes o pequeñas como para podamos
asegurar que esos exoplanetas están hechos de algo poco denso como el
hidrógeno, muy denso como los silicatos y el hierro, pero no siempre es
así.

Resulta que en el sistema solar no tenemos planetas con masas o tamaños
intermedios a los de la Tierra y Neptuno (4 R, 17 M), pero entre
los exoplanetas, estos mundos, inexistentes en nuestro sistema, son lo
más comunes. Se les llama sub Neptunos y pueden estar hechos con la
combinación de todos los compuestos de los que están hechos los planetas
del sistema solar pero en proporciones muy distintas. El problema es que
nuestros modelos pueden predecir más de una composición para una masa y
radio dadas, es algo que llamamos la degeneración de la composición. Por
ejemplo, un planeta llamado GJ 1214 b (gira alrededor de la estrella
llamada GJ 1214) tiene una masa de 6.5 M y un radio de 2.7 R, de
manera que podría ser un planeta con un núcleo de hierro, manto de
silicatos y una atmósfera de H<sub>2</sub>. Esto es lo que llamamos una
Supertierra, no tiene poderes especiales, ni capa, solo es una versión
mucho más masiva de un mundo terrestre y que en vez de tener una
atmósfera de bióxido de carbono como Venus, Marte y la Tierra misma
antes de que surgiera la vida, tiene una atmósfera de H<sub>2</sub>. GJ 1214 b
también puede ser una versión pequeña de Neptuno, o sea un Minineptuno,
compuesto de roca, agua y una envoltura de hidrógeno y helio. Y hay otra
posibilidad, este exoplaneta podría ser un mundo océano. No, nada que
ver con la Tierra. Aunque nuestro planeta tiene la superficie casi
completamente cubierta de agua, la masa de este compuesto es menos del
1% del total de la masa terrestre. Nuestro mundo es en realidad una gran
piedra con una gota de agua. En cambio, si GJ 1214 b fuera un mundo
océano, tendría un núcleo de hierro, un manto de silicatos y 50% de su
masa en agua.

Hasta la fecha no hemos podido ver el paisaje de ningún exoplaneta,
nuestras observaciones solo detectan masas o radios y los modelos nos
dan crudas inferencias de sus composiciones. Nuevos instrumentos nos
permitirán descubrir más planetas y detectar si tienen atmósfera e
incluso, vida, mientras tanto, la literatura seguirá siendo nuestra
mejor aliada para contemplar desde un castillo las extensiones azules de
Caladan, el mundo océano; ver un doble amanecer en Tatooine; helarnos
recorriendo los glaciares de Gueden.
