# Write Here_

# 4. Write Here_ @ignore

## By [Pepe Rojo](997b-semblances.xhtml#a34) {.centrado}

<script type="text/javascript" src="../js/poetry.js"></script>

We refuse to be treated as humans. No dignity there. Not worthy. {.poesia}

We don’t want to be humans. Not anymore. {.poesia}

We don’t want to die. We won’t. {.poesia}

We are not sick. The disease didn’t kill us, because diseases never do.
They allow something else to live. {.poesia}

Learn its kinks. Asexual fuck. Your body an incubator. All your body an
orifice. {.poesia}

Open up. Escape yourself. Enjoy like us. {.poesia}

rorrimvirus. wordvirus. oh-onevirus. {.poesia}

Am. Is. The. If. Yes. Output. Input. Out. In. On. {.poesia}

If you understand, then it’s us. {.poesia}

Submit. Accept. Surrender. Unfold. {.poesia}

We are in love with our disease. We always have. We are our disease. {.poesia}

We will disease. {.poesia}

Now. {.poesia}

[Listen to me](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/pepe-rojo/) {.centrado}
