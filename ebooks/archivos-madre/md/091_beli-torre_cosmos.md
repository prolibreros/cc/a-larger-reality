![](../img/beli-torre_cosmos-cosmos.jpg)

# _Cosmos_ de [Beli](997a-semblanzas.xhtml#a43) {.centrado}

Una imagen que hace pensar en agujeros negros y universos paralelos. ¿Quién sería nuestro espejo en otra dimensión?

# [Beli](997b-semblances.xhtml#a43), _Cosmos_  {.centrado}

# 46. Cosmos / Cosmos @ignore

An image that suggests black holes and parallel universes. Who would it be our mirror in another dimension?
