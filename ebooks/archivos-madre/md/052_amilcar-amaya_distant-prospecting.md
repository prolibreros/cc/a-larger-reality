# Distant Prospecting

# 26. Distant Prospecting @ignore

## By [Amílcar Amaya](997b-semblances.xhtml#a03) \
Translated by [Toshiya Kamei](997b-semblances.xhtml#a53) {.centrado}

That heat. It was horrible. If there were a smooth surface where a drop
of water would fall, it would have hissed like oil in a pan. But there
was no smooth surface and water was too precious to be wasted in a
futile experiment. The shadow of the cliff behind him was shrinking with
the arrival of noon. The paleontologist wasn’t as helpless as he might
seem. The harsh sun was one of the few things he didn’t like about his
job.

What really interested him was asleep under his feet, away from the
roots of biznagas, choyas, yuccas, and strawberry trees. Multiple layers
of sediment between their pages of silica---because, yes, viewed from
a proper perspective, those ageless plates would seem like a gigantic
book---kept the traces of life on Earth in the form of bones, prints,
impressions. As makeshift bookmarks.

That particular area was rich in Cretaceous outcrops that is, a
window that allowed the world to be seen as it was at least sixty-five
million years earlier. A strange time when there was more oxygen in the
atmosphere, animals that could reach gigantic sizes and average global
temperatures higher than at present. The heat was that spark of life.

If the superposition principle didn’t suffer from any break, the deeper
the pressed remains were, the older they were. Below the dinosaurs and
the first flowers there would be reptiles with candles on their backs,
fish that learned to walk, and tiny worms with some inner strength. And
if it went down enough, many, many kilometers, the very consistency of
the earth would be transformed from the reliable solid on which the
paleontologist supported his boots into a not-so-reliable boiling liquid
where everything would be ephemeral.

Thinking about those plutonic heats, the paleontologist was no longer
bothered by forty degrees on the surface. He could keep working a little
longer.
