# La Orden de los Pensadores de la Singularidad

# 44. La Orden de los Pensadores de la Singularidad @ignore

## [Gerardo Horacio Porcayo](997a-semblanzas.xhtml#a17) {.centrado}

El grupo nació hermético por contexto; no pretendíamos nada ni se
plantearon reglas, solo buscamos una vía para la convivencia. Doce
especies y grupos distintos en órbita a Cygnus x-1, con la bruma
sensorial derivada de las emisiones del agujero negro que nos obligó a
fabricar estos módulos (especie de batiscafos de inmersión profunda,
nunca más allá del disco de acreción, menos hasta el horizonte de
eventos) y a nuestras naves capitanas, a abandonar las inmediaciones
para evitar un mayor deterioro en sus instrumentos.

La nuestra es una investigación de campo en el filo; es como volver al
pasado, a las viejas misiones Mercury. Una cápsula monoplaza por cada
grupo, doce satélites con el instrumental fijo en el centro de esa dona
luminosa y cargada de radiaciones. Teorizamos una órbita similar y hemos
conseguido una eficiente transmisión entre nosotros. Nuestro primer
lenguaje fue la matemática y un extraño prurito nos hizo obviar, dejar
al margen cualquier referencia a raza o planeta de origen. Nos une un
objeto de estudio: es el centro de nuestras vidas. Paulatinamente hemos
confrontado teorías, postulados; incluso filosofías de las que
erradicamos cualquier antropocentrismo (o sus variantes).

Continuar este ciclo nos ha impuesto un itinerario común: debates
formales en el primer tercio del día, debates teológicos en el segundo
y, en el tercero, la simple especulación que ha fructificado en
fantasías científicas y aventureras…

Nadie está seguro de lo que pasa afuera. Postulamos una estación
espacial en torno al único planeta gaseoso que sobrevivía cuando
arribamos y, de tiempo en tiempo, un cohete robot nos proveía de víveres
y reemplazos de instrumental.

Hace tres jornadas, la cápsula cinco fue la primera en recibir planos y
material para la ruta de escape. Luego, las demás. Conjeturamos una
guerra entre nuestros congéneres.

No retrocederemos. Decidimos seguir aquí, perpetuar esta natural Orden
de los Pensadores de la Singularidad, regresarles estos mensajes, los
datos recopilados y las filosofías acuñadas en conjunto.

Confiamos en que nada frenará nuestros suministros. Sería preferible
morir aquí que sumarnos a otra guerra absurda.
