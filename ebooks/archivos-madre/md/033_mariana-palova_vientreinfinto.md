![](../img/mariana-palova_vientreinfin-infinitewomb.jpg)

# _Vientre infinito_ de [Mariana Palova](997a-semblanzas.xhtml#a27) {.centrado}

Como es la primera vez que trato con temas de esta índole, fue muy complicado para mí inclinarme hacia lo científico, a pesar de ser el tema central, por lo que tanto mi texto como mi obra terminaron dándole una personificación a algo tan intangible como la adaptación al medio natural; algo que, a pesar de su complejidad científica, siempre parece tener mucho misticismo. El bestiario que conforma la propia naturaleza parece infinito y fantástico, aún siendo real y tangible, lo que ha despertado mi imaginación desde que tengo memoria. Encontrar este lazo entre mi obra y algo totalmente desconocido en el ámbito artístico para mí: la creación de ideas a partir de hechos científicos, fue tan sorpresivo, que la creación final del texto (de la cual había descartado montones de ideas antes) nació de forma espontánea y prácticamente en los últimos minutos de entrega.

# [Mariana Palova](997b-semblances.xhtml#a27), _Infinite womb_ {.centrado}

# 17. Vientre infinito / Infinite womb @ignore

As it was the first time I’ve dealt with science fiction in my art, it was very complicated for me to get my bearings and work with the central theme. Consequently, both my text and my art ended up personifying something as intangible as natural adaptation, a process that, despite its scientific complexity, always seems filled with mysticism. The living bestiary of nature itself seems infinite and fantastic, even though it is real and tangible, awakening my imagination since I can remember. Finding this link between my work and something totally unknown in the artistic field for me---the creation of ideas from scientific facts--- was so surprising that the final creation of the text (from which I had discarded many ideas) was born spontaneously and practically in the last minutes before the deadline.
